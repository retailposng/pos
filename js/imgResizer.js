function dragObj(evt){
	var obj=getElementsByClassName(document,"div","draggable")[0];
	e=evtype(evt);
	mouseover=true;
	pleft=parseInt(obj.style.left);
	ptop=parseInt(obj.style.top);
	  xcoor=e.clientX;
	  ycoor=e.clientY;
	 document.onmousemove=function(e){
		 var x=e.clientX,y=e.clientY;
		  if((pleft+x-xcoor) >= 0 && ((pleft+x-xcoor+obj.offsetWidth) <= window.innerWidth)){// && window.offsetWidth
			  obj.style.left=pleft+x-xcoor+"px";
			  }
			  
			  if((ptop+y-ycoor) >= 0 && ((ptop+y-ycoor+obj.offsetHeight) <= window.innerHeight)){
			  obj.style.top=ptop+y-ycoor+"px";
			  }
			  getCoord();
		return false;
	 	};
	 return false;
	}
	document.onmouseup=function(e){document.onmousemove=null;};

function getCoord(){
	 if(!isDefined(getObj('srcimg'))) return false;
	 var coor,obj=getObj('draggable'),padd,scro;
	 coor=getPosition(obj);
	 padd=getPosition(getObj('srcimg'));
	 scro=scrollPosition();
	 return (((coor[0]-padd[0])<0)?(0):(coor[0]-padd[0]+scro[0]))+","+(((getObj('srcimg').offsetWidth-obj.offsetWidth-coor[0]-padd[0])<0)?(0):(getObj('srcimg').offsetWidth-obj.offsetWidth-coor[0]-padd[0]-scro[0]))+","+(((coor[1]-padd[1])<0)?(0):(coor[1]-padd[1]))+","+(((getObj('srcimg').offsetHeight-obj.offsetHeight-coor[1]-padd[1])<0)?(0):((getObj('srcimg').offsetHeight-obj.offsetHeight-coor[1]-padd[1])));
	 }	

function newWidth(form){
	var obj=getObj('draggable');
    if(form['ratio'].checked){
		form['h'].value=Math.round((((form['w'].value*100)/form['default_w'].value)*form['default_h'].value)/100);
}
    obj.style.width=Math.round(form['w'].value)+"px";
    obj.style.height=Math.round(form['h'].value)+"px";
}

function newHeight(form){
	var obj=getObj('draggable');
     if(form['ratio'].checked){
		form['w'].value=Math.round((((form['h'].value*100)/form['default_h'].value)*form['default_w'].value)/100);
}
    obj.style.width=Math.round(form['w'].value)+"px";
    obj.style.height=Math.round(form['h'].value)+"px";
}

function navW(o){
	var obj=getObj('draggable'),t=null;
	o.onmousedown=function(){
		t=setInterval(function(){
		if((getPosition(obj)[0]+obj.offsetWidth+2)>window.innerWidth) return false;
		obj.style.left=(obj.offsetLeft+1)+"px";
		getCoord();
		},10);
		};
	o.onmouseup=function(){
	clearInterval(t);
	};
	o.onmouseout=function(){
	clearInterval(t);
	};
	}
	
function navE(o){
	var obj=getObj('draggable'),t=null;
	o.onmousedown=function(){
		t=setInterval(function(){
		if((obj.offsetLeft-1)<=0) return false;
		obj.style.left=(obj.offsetLeft-1)+"px";
		getCoord();
		},10);
		};
	o.onmouseup=function(){
	clearInterval(t);
	};
	
	}
	
	function navN(o){
	var obj=getObj('draggable'),t=null;
	o.onmousedown=function(){
		t=setInterval(function(){
		if((obj.offsetTop-1)<=0) return false;
		obj.style.top=(obj.offsetTop-1)+"px";
		getCoord();
		},10);
		};
	o.onmouseup=function(){
	clearInterval(t);
	};
	
	}
	
	function navS(o){
	var obj=getObj('draggable'),t=null;
	o.onmousedown=function(){
		t=setInterval(function(){
		if((getPosition(obj)[1]+obj.offsetHeight)>window.innerHeight) return false;
		obj.style.top=(obj.offsetTop+1)+"px";
		getCoord();
		},10);
		};
	o.onmouseup=function(){
	clearInterval(t);
	};
	}

function navC(){
	$('#draggable').center();
	getCoord();
	}
