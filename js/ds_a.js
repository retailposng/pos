 function is(type, obj) {
	    var clas=Object.prototype.toString.call(obj).slice(8,-1);
	    return obj !== undefined && obj !== null && clas === type;
	}
 
Array.prototype.array_offset=function(n) {
	var arr=[];
	for (var i=0;i<this.length;i++) {
	if(i<=n) continue;
	arr.push(this[i]);
}
return arr;
};
 
 
Array.prototype.in_array=function(v){
		 var bool=false;
		for(var i=0;i<this.length;i++){
			if(this[i]==v){
				 bool=true;
				 }
		}
		return (bool==true)?(true):(false);
	 };

	 Array.prototype.array_hasType=function(dataType){
		 var bool=false;
		for(var i=0;i<this.length;i++){
			if(typeof this[i]==dataType){
				 bool=true;
				 }
		}
		return (bool==true)?(true):(false);
	 };
	 
	 Array.prototype.array_countType=function(dataType){
		 var c=0;
		for(var i=0;i<this.length;i++){
			if(typeof this[i]==dataType){
				 c++;
				 }
		}
		return c;
	 };

var array_diff=function(arr1,arr2){
	 var arr=[],c=0;
	 for(var i=0;i<arr1.length;i++){
	 if(!arr2.in_array(arr1[i])){
	 arr[c++]=arr1[i];
	 }
	 }
	 return arr;
	 };

	 // array common
var array_com=function(arr1,arr2){
	 var arr=[],c=0;
	 for(var i=0;i<arr1.length;i++){
	 if(arr2.in_array(arr1[i])){
	 arr[c++]=arr1[i];
	 }
	 }
	 return arr;
	 };
 
	
	 Array.prototype.array_slen=function(){
		var arr=[],c=0;
			for (var i=1;i<this.length;i++){
				if(this[0][1].trim().length != this[i][1].trim().length){
					arr[c++]=this[i][0];
				}
			}
			return arr;
		};
		
		
		Array.prototype.ajax_params=function(){
			var arr=[];
		for(var i=0;i<this.length;i++){
			arr[i]="params["+i+"]="+this[i];
		}
		return arr.join("&");
		};
		
		Array.prototype.is_unique=function(){
			var bool=true;
			for(var i=0;i<this.length;i++){
				var c=0;
				for(var j=0;j<this.length;j++){
					if(this[i]==this[j]) c++;
				}
				if(c>1) bool=false;
			}
			return bool;
		};
		
		 Array.prototype.array_count=function(obj){
				var arr=this.array_trim(),c=0;
				for (var i=0;i<arr.length;i++){
					if(arr[i]==obj){
						c++;
					}
					
				}
					return c;
			};
	
		Array.prototype.array_find=function(v){
			 var c=-1;
			 for(var i=0;i<this.length;i++){
				 if(this[i]==v){
					c=i;
					break;
				 }
			 }
			 return c;
			 };
			
	Array.prototype.array_freq=function(){
		var arr=[],arr2=[],c=0;
		for (var i=0;i<this.length;i++){
			var k=0,arr3=[];
			for (var j=0;j<this.length;j++){
				if(this[i]==this[j]){
					arr3[k++]=j;
				}
	
			}
			if(!arr2.in_array(this[i])){
			arr2[c]=this[i];
			arr[c]=[this[i],k,arr3];
			c++;
			}
		}
		return arr.array_trim();
	};
	
	Array.prototype.array_sibling=function(data){
		var arr=[];
		if(this.length != data.length) return this;
		for (var i=0;i<this.length;i++){
			arr[i]=(is("Array",this[i]))?(this[i].array_extend(data[i])):([this[i],data[i]]);
		}
		return arr;
	};
	
	Array.prototype.array_grp_sum=function(){
		var arr=[],arr2=[],c=0;
		for (var i=0;i<this.length;i++){
			var k=0,arr3=[];
			for (var j=0;j<this.length;j++){
				if(this[i][0]==this[j][0]){
					arr3[k++]=this[j][1];
				}
	
			}
			if(!arr2.in_array(this[i][0])){
			arr2[c]=this[i][0];
			arr[c]=[this[i][0],arr3.array_sum()];
			c++;
			}
		}
		return arr.array_trim();
	};
	
	Array.prototype.array_trim=function(){
		var arr=[],c=0;
		for(var i=0;i<this.length;i++){
			if(isDefined(this[i]) && !new String(this[i]).isEmpty()){
			arr[c++]=this[i];
			}
		}
		return arr;
	};


	Array.prototype.array_remove=function(n){
		var arr=[],c=0;
		for(var i=0;i<this.length;i++){
			if(i==n){continue;}
			arr[c++]=this[i];
		}
		return arr;
	};
	
	Array.prototype.array_pad=function(len,data){
		if(this.length > len) return this;
		return this.array_extend(array_repeat(len-this.length,data));
	};

	Array.prototype.array_extend=function(obj,l){
		var j=this.length+obj.length,arr=[],m=0,n=l || this.length;
		for (var i=0;i<j;i++){
			
		if (i==n){
			m=i;
			var c=0;
			i+=obj.length;
			for (var k=n;k<i;k++){
				arr[k]=obj[c];
				c++;
			}
			}
			if(i<j)
			  arr[i]=this[m];
		m++;
		}
		return arr;
	};
	
	Array.prototype.array_sum=function(){
		var sum=0;
		for (var i=0;i<this.length;i++){
			sum += Number(this[i]);
		}
		return sum;
	};
	
	array_repeat=function(n,obj){
		var arr=[];
		for(var i=0;i<n;i++){
			arr[i]=obj;
		}
		return arr;
	};

	
	    
Array.prototype.array_lower=function(){
var arr=[];
for(var i=0;i<this.length;i++){
	if(isDefined(this[i])){
arr[i]=(typeof this[i]=="string")?(this[i].toLowerCase()):(this[i]);
}
}
return arr;
};

Array.prototype.array_map_keys=function(arr){
var keys=[],k=0,arr2=this.array_lower();
for(var i=0;i<arr2.length;i++){
	for(var j=0;j<arr.length;j++){
		if(arr[i]==arr2[j]) {
			if(!keys.in_array(j)){
			keys[k]=j;
			k++;
			}
			}
		}
}
return keys;
	};
	
	Array.prototype.array_sortby=function(arr){
		
	}
	
Array.prototype.array_sort=function(order){
var tmp,arr=this.array_lower();
	for(var j=0;j<arr.length;j++){
		for(var i=0;i<arr.length;i++){
			if((order=="asc")?(arr[i] > arr[i+1]):(arr[i] < arr[i+1])){ 
				tmp=arr[i];
				arr[i]=arr[i+1];
				arr[i+1]=tmp;
}
}
}
return arr;
};
	
Array.prototype.array_transpose=function(){
	var arr=[],len=this[0].length;
	for(var i=0;i<len;i++){
		var arr1=[];
		for(var j=0;j<this.length;j++){
			arr1[j]=this[j][i];
			}
			arr[i]=arr1;
		}
		return arr;
	};

Array.prototype.array_copy=function(){
	var arr=[];
	for(var i=0;i<this.length;i++){
		arr[i]=this[i];
		}
		return arr;
	};

	

Array.prototype.array_msort=function(col,order){
	var arr=this.array_transpose(),arr3=[];
	keys=arr[col].array_map_keys(arr[col].array_sort(order));
	for(var i=0;i<keys.length;i++){
		var arr4=[];
		for(var j=0;j<arr.length;j++){
			arr4[j]=arr[j][keys[i]];
			}
		arr3[i]=arr4;
}
	
	return arr3;
	};
	
	Array.prototype.array_inverse=function(){
		var arr=[],i=0;
		while(this.length>0){
			arr[i++]=this.pop();
			}
			return arr;
		};		
		
		function range(x,y,z){
			var arr=[],c=0;
			for(var i=x;i<y;i++,i +=z-1 || 0){
				arr[c++]=i;
			}
			return arr;
		}
		
		 function rand(min,max){
	    	  return Math.floor(min+(Math.random()*(max-min+1)));
	      }
	      
	      function xrange(lim,n){
	    	  var arr=[],i=n,v;
	    	  while(i>0){
	    		  v=rand(lim[0],lim[1]);
	    		  if(!arr.in_array(v)){
	    			  arr.push(v);
	    			  i--;
	    		  }
	    	} 
	    	  return arr;
	      }
		
		Array.prototype.array_shuffle=function(){
			var i=this.length,j,temp;
			if(i==0) return this;
			while(--i){
				j=Math.floor(Math.random()*(i+1));
				temp=this[i];
				this[i]=this[j];
				this[j]=temp;
			}
			return this;
			};
		
		Array.prototype.array_filter=function(kword,xKeyword){
			var arr=[],keyword=kword.trim().split(/[\s]+/g);
			for(var i=0;i<this.length;i++){
				var indx=[],z=0,doc;
				for(var j=0;j<keyword.length;j++){
					doc=this[i].join(" ").toLowerCase();
					n=doc.indexOf(keyword[j].toLowerCase());
					if(n >= 0 && (isDefined(xKeyword) && doc.indexOf(xKeyword.toLowerCase()) != -1 && doc.indexOf(kword.slice(xKeyword.length).toLowerCase()) != -1)) z++;
					indx.push(n);
					
				}
				indx.push(z);
				arr[i]=z;
			}
			arr1=arr.array_sort("desc");
			keys=arr.array_map_keys(arr1);
			 var arr3=[],m=0;
			 for(var k=0;k<keys.length;k++){
	 			  if(arr1[k] != 0){
	 				  arr3[m++]=keys[k];
	 		   			}
               }
			return arr3;
		};
			
		Array.prototype.array_search=function(keyword,optimize){
			var kword=keyword.trim().split(/[\s]+/g),arr1=[];
		 	for(var i=0;i<this.length;i++){
		 		var arr2=[],xyz=this[i].array_lower();
		 		for(var m=0;m<kword.length;m++){
		 			var q=0,abc;
		 			for(var n=0;n<xyz.length;n++){
		 				abc=xyz[n].replace(/[,]+/g,'');
		 				if(abc.split(' ').length > 1){
		 					var recur=abc.split(' ');
		 					for(var p=0;p<recur.length;p++){
		 						if(recur[p].indexOf(kword[m].toLowerCase().replace(/[,]+/g,''))==0 && recur[p].length > 1){
				 					q++;
				 				}
		 					}
		 				}
		 				else{
		 					if(abc.indexOf(kword[m].toLowerCase().replace(/[,]+/g,''))==0 && abc.length > 1){
			 					q++;
			 				}
		 				}
		 					
		 				arr2[m]=q;
			 		}
		 		}
		 			arr1[i]=arr2.array_sum();
		 		}
		 	
		       arr4=arr1.array_sort("desc");
	 		   keys=arr1.array_map_keys(arr4);
	 		   var arr3=[],m=0;
	 		  for(var k=0;k<keys.length;k++){
	 			  if(((isDefined(optimize) && !optimize) || arr4[k]==arr4[0]) && arr4[k] != 0){
	 				  arr3[m++]=keys[k];
	 		   			}
                }
	 		   return arr3;
		};
		
		function easing(n){
var j=1;i=1,arr=[],k=0,sum=n,arr2=[];
while(i<n/2){
	arr[j-1]=i=Math.pow(j,2)-1;
	if(j>1){
		arr2[k++]=sum;
		sum -=arr[j-1]-arr[j-2];
		}
	if (i>n) {
		arr[j-1]=n;
	break;
	}
	j++;
	}
	
	return arr2.array_extend(arr.array_inverse());	
	}

		function json2array(data){
			var arr=[];
			for(var x in data){
				if(data.hasOwnProperty(x) && typeof data[x] != 'function'){
					arr.push((typeof data[x]==typeof {})?(json2array(data[x])):(data[x]));
				}
			}
			return arr;
		}

		function array2json(data){
			var arr={};
			for(var i=0;i<data.length;i++){
				arr[i]=(typeof data[i]==typeof [])?(array2json(data[i])):(data[i]);
			}
			return arr;
		}