<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title>Web-based retail store point of sale</title>
<?php 
    include_once 'demo/meta.php';
     ?>
    <link rel="shortcut icon" href="img/favicon.png" />
    <link href="css/styles.css" rel="stylesheet"/>
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57857218-1', 'auto');
  ga('send', 'pageview');

</script>
</head>
<body>
<div class="row">
<div class="col-lg-12 banner text-center">
<img src="img/shopper.svg" class="infographic shopping-girl" style="width:250px;height:auto"/>
<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>

<div class="caption">
<h1><strong><img src="img/logo.png" style="position:absolute;top:20px;right:20px;width:100px;height:auto"/>Retail Point Of Sale</strong></h1>
<h3>A simple  &amp; flexible web-based point of sale for retail stores</h3>
<a href="/demo" class="btn btn-info btn-lg">Try demo version</a>
&nbsp;&nbsp;
<a href="/download" class="btn btn-warning btn-lg">Download</a>
</div>

<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>
</div>
</div>
<div class="row">
<div class="col-md-12">
<h2 class="pitch">
Our web based POS will manage your sales, stocks and real-time reporting of daily sales. Depending on your needs and the type of business you have, we have systems specifically designed for retails: electronics, phones, restaurant, bar, grocery, salon and more, a web based application. The system is made to provide the most efficient and productive POS experience for you and your customers.
</h2>
</div>
</div>

<div class="infographic">
<div class="row">
<div class="col-lg-4 text-center">
<h2 class=""><strong>Sell &amp; Manage stocks</strong></h2>
<div class="clearfix"></div>
<i class="icon-fresh7"></i>
<div class="clearfix">&nbsp;</div>
Easily add and sell any type of product. Track inventory in one or many stores.
</div>

<div class="col-lg-4 text-center">
<h2 class=""><strong>Sales report &amp; Analysis</strong></h2>
<div class="clearfix"></div>
<i class="icon-line31"></i>
<div class="clearfix">&nbsp;</div>
Pull daily sales report in text and chart representation
</div>

<div class="col-lg-4 text-center">
<h2 class=""><strong>Print &amp; Email Receipts</strong></h2>
<div class="clearfix"></div>
<i class="icon-invoice1"></i>
<div class="clearfix">&nbsp;</div>
Email or print receipts as mini/standard printer size
</div>
</div>

<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>

<div class="row">
<div class="col-lg-4 text-center">
<h2 class=""><strong>Discounts &amp; custom pricing</strong></h2>
<div class="clearfix"></div>
<i class="icon-discount2"></i>
<div class="clearfix">&nbsp;</div>
Make discounts on the fly as a percentage, a fixed amount
</div>

<div class="col-lg-4 text-center">
<h2 class=""><strong>Data backup &amp; Remote access</strong></h2>
<div class="clearfix"></div>
<i class="icon-cloud305"></i>
<div class="clearfix">&nbsp;</div>
See an instant snapshot of sales &amp; staff performance from anywhere, anytime.
</div>

<div class="col-lg-4 text-center">
<h2 class=""><strong>Built-in google image search</strong></h2>
<div class="clearfix"></div>
<i class="icon-photos9"></i>
<div class="clearfix">&nbsp;</div>
Assign image to products from image store
</div>

</div>

</div>

<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>

<div class="row">
<div class="col-lg-12 text-center">
<h1>Take a look at screen shots</h1>
<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>
<div class="row">
<div class="col-sm-8">
<img alt="retail pos dashboard" src="img/dashboard.png" class="img-responsive screenshot"/>
</div>
<div class="col-sm-4 text-left">
<h3 class="no-margin">Dashboard</h3>
<div class="clearfix">&nbsp;</div>
The interface is friendly and easy to use for navigation
</div>
</div>
<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>
<div class="row">
<div class="col-sm-4 text-left">
<h3 class="no-margin">Shopping Cart</h3>
<div class="clearfix">&nbsp;</div>
Select items to shopping cart, add customer &amp; order note, give fixed or percentage discount to products and checkout within a single page 
</div>
<div class="col-sm-8">
<img alt="retail pos dashboard" src="img/shopping-cart.png" class="img-responsive screenshot"/>
</div>
</div>

<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>
<div class="row">
<div class="col-sm-8">
<img alt="retail pos dashboard" src="img/sales-analysis.png" class="img-responsive screenshot"/>
</div>
<div class="col-sm-4 text-left">
<h3 class="no-margin">Sales Analysis</h3>
<div class="clearfix">&nbsp;</div>
Real-time and remote access to sales report and analysis
</div>
</div>

<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>
<div class="row">
<div class="col-sm-4 text-left">
<h3 class="no-margin">Image Store</h3>
<div class="clearfix">&nbsp;</div>
Assign image to product through google image search, or select from computer
</div>
<div class="col-sm-8">
<img alt="retail pos dashboard" src="img/image-store.png" class="img-responsive screenshot"/>
</div>
</div>

</div>
</div>
<div class="clearfix">&nbsp;</div>
<div class="clearfix">&nbsp;</div>
<div class="row link-box">
<div class="col-lg-12 text-center">
<small class="fs30">Enquiry: info@retail-pos.com.ng, +2348034309999</small>
</div>
</div>

<div class="row footer">
<div class="col-lg-12 text-center">A product designed by the team of <a href="http://www.attemptexams.com/team/" target="_blank">AttemptExams</a></div>
</div>

</body>
</html>
