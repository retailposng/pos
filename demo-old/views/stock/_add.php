<?php 
global $stock,$stockID,$supplier,$config;
ob_start();
?>

<div class="rel bg-color-white padding10" id="stock_box">
<?php 
if (isset($_GET["response"])) {
	?>
	<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
 <?=$_GET["response"];?>
</div>
	<?php 
}
?>
 <div id="err" class="alert alert-danger hide2">error</div>
<!-- <a href="stock.php?p=multiple" class="btn btn-xs btn-default absTR bluBT">Add Multiple</a> -->
 
 <form action="barcode.php" method="get" onsubmit="Barcode.reader(this)">
  <button class="btn btn-xs" onmouseover="this.form['data'].focus()" onclick="this.form['data'].focus()" name="activate" type="button">Barcode reader...</button>
 <input type="text" class="barcode-control" onfocus="Barcode.ready(this.form)" onblur="Barcode.notReady(this.form)" id="barcode_control" placeholder="Barcode" value="<?=@$stock["barcode"];?>" name="data" id="<?=uniqid();?>"   autofocus="autofocus" autocomplete="off"/>
 <iframe src="javascript:void(0)" class="hide" id="barcodeFrame" name="barcodeFrame"></iframe>
 </form>
 
 <form action="javascript:void(0)" method="post" enctype='multipart/form-data' class="stock-form">
 <input type="hidden" name="stockID" value="<?=$stockID;?>"/>
 <div class="col-md-6">
 <?=(isset($_GET["status"]) && !empty($_GET["status"]))?("<div class='alert alert-success padding5'>Successfully added</div>"):(NULL);?>
 
 <div class="control-group row" id="manu_box">
    <label>Barcode</label>
   <div class="col-xs-12 rel no-padding">
   <div class="input-group">
              		<input type="text" class="form-control" placeholder="Barcode" value="<?=@$stock["barcode"];?>" name="barcode" id="<?=uniqid();?>"   autocomplete="off" required="required"/>
	  <span class="input-group-btn">
 <button type="button" class="btn btn-info" onclick="Stock.barcode(this.form)">Generate</button>
 </span>
 </div>
	  </div>
  </div>

 

 <div class="clear">&nbsp;</div>
 
 <div class="control-group row" id="pro_box">
    <label>Product Name</label>
    
  <div class="col-xs-12 rel no-padding">
              		<input type="text" class="form-control" placeholder="Product" value="<?=@$stock["pname"];?>" name="product" id="<?=uniqid();?>" onchange="Stock.Icon.load(this.form)"  autocomplete="off" onkeyup="DList.search(event,this,getObj('product_dlist'))" required="required"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="product_dlist">
				      <ul class="navlist link"></ul>
				      </div>
    </div>
  </div>
  
  <div class="clear">&nbsp;</div>
   
    <div class="row" id="cat_box">
    <label>Category</label>
    
   <div class="col-xs-12 rel no-padding">
              		<input type="text" class="form-control" placeholder="Category" value="<?=@$stock["cname"];?>" name="category" id="<?=uniqid();?>" onchange="Stock.Icon.load(this.form)"  autocomplete="off" onkeyup="DList.search(event,this,getObj('category_dlist'))" required="required"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="category_dlist">
				      <ul class="navlist link"></ul>
				      </div>
    </div>
  </div>
  
<div class="clear">&nbsp;</div>
 
  <div class="control-group row" id="manu_box">
    <label>Manufacturer</label>
   <div class="col-xs-12 rel no-padding">
              		<input type="text" class="form-control" placeholder="Manufacturer" value="<?=@$stock["mname"];?>" name="manufacturer" id="<?=uniqid();?>" onchange="Stock.Icon.load(this.form)"  autocomplete="off" onkeyup="DList.search(event,this,getObj('manufacturer_dlist'))" required="required"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="manufacturer_dlist">
				      <ul class="navlist link"></ul>
				      </div>
    </div>
  </div>  
   <div class="clear">&nbsp;</div>
   <div class="row">
 <div class="col-xs-12 no-padding">
    <label onclick="toggle('desc2',function(){$('#_desc2').show();},function(){$('#_desc2').hide();})">Variant / Description <i class="caret"></i></label>
	 <div class="row hide2" id="_desc2">
	 <div class="col-xs-6 no-padding">
	 <select class="form-control" name="size">
	 <?php 
	 foreach (array(""=>"--Size--","none"=>"None","xxxs"=>"Smallest","xxs"=>"Extra extra small","xs"=>"Extra small","s"=>"Small","m"=>"Medium","l"=>"Large","xl"=>"Extra large","xxl"=>"Extra extra large","xxxl"=>"Largest") as $k=>$v){
	 	$sel=@$stock["size"]==$k?"selected='selected'":NULL;
	 	echo "<option {$sel} value='{$k}'>{$v}</option>";
	 }
	 ?>
	 </select>
	 </div>
	 <div class="col-xs-6 pr0">
	  <input type="text" class="form-control" placeholder="Color" value="<?=@$stock["color"];?>" name="color" id="<?=uniqid();?>" onchange="Stock.Icon.load(this.form)"  autocomplete="off" onkeyup="DList.search(event,this,getObj('color_dlist'))"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="color_dlist">
				      <ul class="navlist link"></ul>
				      </div>
	  
	 </div>
	 <div class="clear">&nbsp;</div>
	  <div class="col-xs-12 no-padding">
	   <input type="text" class="form-control" placeholder="Location" value="<?=@$stock["location"];?>" name="location" id="<?=uniqid();?>" onchange="Stock.Icon.load(this.form)"  autocomplete="off" onkeyup="DList.search(event,this,getObj('location_dlist'))"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="location_dlist">
				      <ul class="navlist link"></ul>
				      </div>
	  </div>
	  <div class="clear">&nbsp;</div>
	  <div class="col-xs-12 no-padding">
	 	 <textarea rows="3" class="form-control" name="description"   placeholder="Describe product or list the features"><?=@$stock["description"];?></textarea>
	 </div>
	 </div>
	 </div>
  </div>
  
  <div class="clear">&nbsp;</div>
  
  <?php 
  if($config["expiry"]==1){
  ?>
   <div class="control-group row">
 <div class="col-xs-6 no-padding">
    <label>Expiry Date</label>
	 <input type="text" class="form-control" name="expiry" onmouseover="$(this).datepicker({'viewMode':'years'})"/>
	 </div>
	 <div class="col-xs-6"></div>
  </div>
    <div class="clear">&nbsp;</div>
  <?php 
  }
  ?>
   
   <div class="control-group row">
    <div class="col-xs-7 no-padding">
    <label >Cost Price</label>
    <input type="text" class="form-control" name="cprice" value="<?=@$stock["cprice"];?>" />
    </div>
   
    
    <div class="col-xs-5 pr0">
   <label >Quantity</label>
    <input type="number" class="form-control" name="qty" min="1" value="<?=(!isset($stock["qty"]))?(1):($stock["qty"]);?>"  />
    </div>
    </div>
    
     <div class="clear">&nbsp;</div>
    <div class="control-group row">
    <div class="col-xs-6 no-padding">
    <label>Sale Price <span class="label label-info"><em>Retail</em></span></label>
    <input type="text" class="form-control" name="rsprice" value="<?=@$stock["rsprice"];?>" />
    </div>
    
     <div class="col-xs-6 pr0">
    <label >Sale Price <span class="label label-success"><em>Wholesale</em></span></label>
    <input type="text" class="form-control" name="wsprice" value="<?=@$stock["wsprice"];?>" />
    </div>
    </div>
    
     <div class="clear">&nbsp;</div>
 
 <?php 
 $vat=json_decode(@$stock["vat"],1);
 ?>
 
 
 <div class="control-group">
 <label>Vat</label>
  <div class="row" id="tax_box">
  <div class="col-xs-4 no-padding" title="Vat included in sale price">
  <a href="#inclusive" role="tab" data-toggle="tab" class="btn btn-default btn-block <?=$vat['type']=="#inclusive"?"active":NULL;?>" onclick="toggleBButton(this,$('#tax_box'))">Inclusive</a>
  </div>
  <div class="col-xs-4 no-padding" title="Vat will be added to sale price">
  <a href="#exclusive" id="exclusive_tab_type" role="tab" data-toggle="tab" class="btn btn-default btn-block <?=$vat['type']=="#exclusive"?"active":NULL;?>" onclick="toggleBButton(this,$('#tax_box'))">Exclusive</a>
  </div>
  <div class="col-xs-4 no-padding" title="No vat">
  <a href="#exempt" role="tab" data-toggle="tab" class="btn btn-default btn-block <?=in_array($vat['type'],array("#inclusive","#exclusive"))?NULL:"active";?>" onclick="toggleBButton(this,$('#tax_box'))">Exempt</a>
  </div>
<input type="hidden" name="vat[type]" value="<?=in_array($vat['type'],array("#inclusive","#exclusive"))?$vat['val']:"#exempt";?>"/>
</div>
</div>

<div class="clear">&nbsp;</div>
<div class="tab-content">
  <div role="tabpanel" class="tab-pane <?=$vat['type']=="#inclusive"?"active":NULL;?>" id="inclusive">
  <div class="input-group">
<input type="number" name="vat[#inclusive]" min="5"  value="<?=$vat['type']=="#inclusive"?$vat['val']:5;?>" placeholder="Enter Vat value" class="form-control"/>
<span class="input-group-addon">%</span>
</div>
</div>
<div role="tabpanel" class="tab-pane <?=$vat['type']=="#exclusive"?"active":NULL;?>" id="exclusive">
<div class="input-group">
<input type="number" name="vat[#exclusive]" min="5"  value="<?=$vat['type']=="#exclusive"?$vat['val']:5;?>" placeholder="Enter Vat value" class="form-control"/>
<span class="input-group-addon">%</span>
</div>
</div>
<div role="tabpanel" class="tab-pane <?=in_array($vat['type'],array("#inclusive","#exclusive"))?NULL:"active";?>" id="exempt">
<input type="hidden" name="vat[#exempt]" value=""/>
</div>
</div>

    <div class="clear">&nbsp;</div>
    <div class="control-group row">
    <div class="col-xs-4 no-padding">
    <label>Product Image</label>
   <a href="javascript:void(0)" onclick='ImageStore.show({"manufacturer":$("form.stock-form")[0]["manufacturer"].value,"product":$("form.stock-form")[0]["product"].value,"category":$("form.stock-form")[0]["category"].value})' class="thumbnail" title="Upload icon" style="width:100px">
   <img src="<?=(isset($_GET["edit"]))?("views/upload/thumbnail/{$stock["iname"]}"):("img/burger1.png");?>" class="img-responsive" id="product_img" alt="image-name"/>
 	<input type="hidden" name="icon" value="<?=(isset($_GET["edit"]))?("views/upload/thumbnail/{$stock["iname"]}"):("img/burger1.png");?>" id="icon_name_holder"/>
 	</a>
  </div>
    
    <div class="col-xs-8 rel">
    
  </div>
   
   </div>
 
 
 
 </div>
 <div class="col-md-6">
    <div class="col-xs-12">
	  <h3>Supplier</h3>
	  </div>
    <div class="control-group col-xs-12">
    <label class="control-label">Company Name</label>
    <div class="col-xs-12 rel no-padding">
              		<input type="text" class="form-control" placeholder="" value="<?=$supplier["name"];?>" name="supplier" id="<?=uniqid();?>"  autocomplete="off" onkeyup="DList.search(event,this,getObj('supplier_dlist'))" required="required"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="supplier_dlist">
				      <ul class="navlist link"></ul>
				      </div>
    </div>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group col-xs-12">
    <label class="control-label">Email Address</label>
    <input type="text" name="email" class="form-control" value="<?=$supplier["email"];?>" />
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group col-xs-12">
    <label class="control-label">Phone Number</label>
    <input type="text" name="phone" class="form-control" value="<?=$supplier["phone"];?>" />
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group col-xs-12">
    <label class="control-label">Office Address</label>
    <textarea class="form-control" rows="5" name="addr" >
    <?=$supplier["addr"];?>
    </textarea>
    </div>
    <div class="clear">&nbsp;</div>
   <div class="control-group row">
   <div class="col-xs-12">
    <label onclick="toggle('note',function(){$('#_note').show();},function(){$('#_note').hide();})">Order Note<i class="caret"></i></label>
	 <textarea rows="3" class="form-control hide2" name="note"  id="_note" placeholder="Add order note"></textarea>
	 </div>
  </div>
     <div class="clear">&nbsp;</div>
    <div class="col-xs-12">
    <div class="btn-group pull-right">
    <button type="submit" onclick="Stock.add(this.form)" class="btn btn-primary"><?=(isset($_GET["edit"]) && !empty($_GET["edit"]))?("Update"):("Save");?></button>
    </div>
    </div>
    <div class="clear">&nbsp;</div>
    <?php 
    if(isset($_GET["req"])){
    	echo "<div class='alert alert-success padding5'>Successfully added</div>";
    }
    ?>
    
 </div>
 </form>
 <div class="clear"></div>
 </div>
 
					                    <iframe src="javascript:void(0)" name="addStockFrame" class="hide"></iframe>
					              
					              <?php 
$contents=ob_get_clean();
include_once 'views/setting/_template.php';
					              include_once 'widget/imgstore/ImageStore.php';
					              include_once 'widget/imgcrop/dialog.php';
					              ?>