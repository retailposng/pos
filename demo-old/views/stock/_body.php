<div class="clear"></div>
<?php 
global $isDeleted;
if($isDeleted===true){
	?>
	<div class="padding10">
	<div class="alert alert-success">
	 <i class="glyphicon glyphicon-ok"></i>&nbsp;Successfully removed
	</div>
	</div>
	<?php
}
?>
<div class="col-md-12 mt10">
<table class="table table-striped table-bordered" id="tb_data">
<thead>
<tr><th>#</th><th></th><th>Product</th><th>Category</th><th>In stock</th><th>Qty. sold</th><th>Cost price</th><th>Sale price <span class="label label-info"><em>Retail</em></span></th><th>Sale price <span class="label label-success"><em>Wholesale</em></span></th><th>Date</th><th></th></tr>
</thead>
<tbody>
<?php
$i=1;
foreach ($this->pdo->query("select s.*,(from_unixtime(s.date,'%d-%m-%Y')) as sdate,m.name as mname,p.name as pname,c.name as cname,i.name as iname,(select sum(x.qty) from stock x where x.stockID=s.stockID) as instock,(select sum(x.qty) from sale x where x.stockID=s.stockID and x.status='sold') as sqty from (stock s join manufacturer m on m.manufacturerID join product p on p.productID join category c on c.categoryID join img i on i.imgID) where m.manufacturerID=p.manufacturerID and p.productID=s.productID and c.categoryID=p.categoryID and i.imgID=s.imgID and s.status='salable' order by s.date desc") as $fetch){
?>
<tr><td><?=$i;?></td>
<td class="mini-thumbnail">
<a href="javascript:void(0)" class="thumbnail no-margin mini-thumbnail">
<img src="views/upload/thumbnail/<?=$fetch["iname"];?>" />
</a>
</td>
<td><a href=""><?="{$fetch["mname"]} {$fetch["pname"]}";?></a><small class="pull-right"><?=$fetch["description"];?></small></td><td><?=$fetch["cname"];?></td><td><?=$fetch["instock"];?></td><td><?=intval($fetch["sqty"]);?></td><td><?=number_format($fetch["cprice"],2);?></td><td><?=number_format($fetch["rsprice"],2);?></td><td><?=(intval($fetch["wsprice"]) <= 0)?(""):(number_format($fetch["wsprice"],2));?></td><td><?=$fetch["sdate"];?></td>
<td style="width:50px">
<div class="btn-group">
<a href="stock.php?edit=<?=$fetch["stockID"];?>" class="btn btn-xs btn-info"><i class="glyphicon glyphicon-edit"></i></a>
<a href="javascript:void(0)" onclick="lock(true);$('#confirm_box').show();$('#del_stock').val('<?=$fetch["stockID"];?>');$('#confirm_box').center()" class="btn btn-xs btn-danger"><i class="glyphicon glyphicon-trash"></i></a>
</div>
<!--
<div class="btn-toolbar">
 <div class="btn-group">
<a href="#" class="btn btn-sm btn-danger" title="Move to Out-dated stocks">Out-dated</a>
</div> 
</div>-->
</td></tr>
<?php 
$i++;
}
?>
</tbody>
</table>
</div>

<div class="confirm-box padding10 hide2" id="confirm_box">
<form action="stock.php" method="post">
<div class="form-group">
<strong>Are you sure?</strong>
<hr class="no-margin">
<div class="clearfix">&nbsp;</div>
<div class="control-group">
<label>Action</label>
<select class="form-control" name="status">
<option value=""></option>
<option value="trash">Move to trash</option>
<option value="expired">Out-dated / Expired</option>
</select>
</div>
<div class="clearfix">&nbsp;</div>
<div class="control-group">
<label>Note</label>
<textarea rows="2" class="form-control" name="note" placeholder="Add Note"></textarea>
</div>
<div class="clearfix">&nbsp;</div>
<div class="control-group">
<div class="btn-group pull-right">
<input type="hidden" name="del" id="del_stock" value=""/>
<button type="submit" class="btn btn-default btn-xs">Yes</button>
<button type="button" onclick="lock();$('#confirm_box').hide()" class="btn btn-default btn-xs">No</button>
</div>
</div>
</div>
</form>
</div>