<ul class="nav nav-pills" role="tablist">
  <li>
    <a href="report.php#outofstock" class="fs15">
      <span class="absTR badge bg-color-danger"><?=$this->data["valuation"]["os"];?></span>
      Out of stock
    </a>
  </li>
  <li>
    <a href="report.php#reorder" class="fs15">
      <span class="absTR badge bg-color-warning"><?=$this->data["valuation"]["rol"];?></span>
     Re-order level
    </a>
  </li>
  <li>
    <a href="javascript:void(0)" class="fs15" onclick="Cart.Suspend.model()">
      <span class="absTR badge bg-color-info" id="suspend_count">0</span>
     Suspended
    </a>
  </li>
</ul>
