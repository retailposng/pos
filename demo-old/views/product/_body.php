 <div class="clear">&nbsp;</div>
<div class="col-sm-12">
<table class="table table-striped table-bordered" id="tb_data">
<thead>
<tr><th>#</th><th>Name</th><th>Category</th><th>In stock</th><th>Re-order level</th><th></th></tr>
</thead>
<tbody>
<?php
$i=1; 
foreach ($this->pdo->query("select p.*,concat(m.name,' ',p.name) as pname,c.name as cname,(select sum(s.qty) from stock s where p.productID=s.productID and s.status='salable') as instock from (product p join manufacturer m on m.manufacturerID join category c on c.categoryID) where c.categoryID=p.categoryID and m.manufacturerID=p.manufacturerID") as $fetch){
?>
<tr><td><?=$i;?></td><td><?=$fetch["pname"];?></td><td><?=$fetch["cname"];?></td><td><?=$fetch["instock"];?></td><td><?=$fetch["reorder"];?></td><td>
<a href="product.php?p=edit&name=<?=$fetch["name"];?>&desc=<?=$fetch["description"];?>&reorder=<?=$fetch["reorder"];?>&id=<?=$fetch["productID"];?>" class="btn btn-sm btn-info"><i class="glyphicon glyphicon-edit"></i></a>
</td></tr>
<?php 
$i++;
}
?>
</tbody>
</table>
</div>