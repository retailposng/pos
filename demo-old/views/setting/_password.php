<?php
global $isOk,$err;
?>
<div class="row">
<div class="col-md-4"></div>
<div class="col-md-4">
<h3>Reset Password</h3>
<?php
echo $err;
?>
<form action="password.php" method="post" role="form">
  <div class="form-group">
    <label for="exampleInputEmail1">Current Password</label>
    <input type="password" name="oldpwd" class="form-control" id="exampleInputEmail1" placeholder="****">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">New Password</label>
    <input type="password" name="pwd1" class="form-control" id="exampleInputPassword1" placeholder="****">
  </div>
  
  <div class="form-group">
    <label for="exampleInputPassword2">Retype New Password</label>
    <input type="password" name="pwd2" class="form-control" id="exampleInputPassword2" placeholder="****">
  </div>
  
  
  <button type="submit" name="reset" value="<?=uniqid();?>" class="btn btn-primary btn-block">Reset My password</button>
</form>
</div>
<div class="col-md-4"></div>
</div>