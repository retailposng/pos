<div class="clear"></div>
<?php 
if (isset($_GET["auth"])) {
	?>
	<div class="alert alert-danger alert-dismissible" role="alert" style="margin:15px 15px 0px 15px">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
  <strong>Permission!</strong> <?=$_GET["auth"];?>
</div>
	<?php 
}

list($todaySales,$productSold)=explode(":",empty($this->data["valuation"]["prosold"])?"0:0":$this->data["valuation"]["prosold"]);
?>
<div class="row mt10">
<div class="col-md-8 no-padding dashboard">
<div class="row">
<div class="col-sm-3">
<div class="bg-color-lightPrimary padding5 fg-color-white rel text-center">
<h3 class="text-center margin5"><a href="pos.php" class="fill">POS <br/><i class="icon-shopping145 fs70"></i></a></h3>
<div class="clear">&nbsp;</div>
<div class="absBL padding5 bg-color-primary w100">
&nbsp;
</div>
</div>
</div>

<div class="col-sm-3">
<div class="bg-color-lightPrimary padding5 fg-color-white rel text-center">
<h3 class="text-center margin5"><a href="sale.php" class="fill">Sales <br/><i class="icon-full22 fs70"></i></a></h3>
<div class="clear">&nbsp;</div>
<div class="absBL padding5 bg-color-primary w100">
<a href="javascript:void(0)" onclick="Synchronize.start(<?=date("Ymd");?>,'<?=json_encode($this->data);?>')" class="fill fs15">Synchronize&nbsp;<span class="glyphicon glyphicon-globe"></span></a>
</div>
</div>
</div>

<div class="col-sm-3">
<div class="bg-color-lightPrimary padding5 fg-color-white rel text-center">
<h3 class="text-center margin5"><a href="stock.php" class="fill"><span class="">Stocks</span> <br/><i class="icon-store5 fs70"></i></a></h3>
<div class="clear">&nbsp;</div>
<div class="absBL padding5 bg-color-primary w100">
<a href="stock.php?p=add" class="fill fs15">Add New Stock&nbsp;<span class="glyphicon glyphicon-plus-sign"></span></a>
</div>
</div>
</div>

<div class="col-sm-3">
<div class="bg-color-lightPrimary padding5 fg-color-white rel text-center">
<h3 class="text-center margin5"><a href="product.php" class="fill"><span class="">Products</span> <br/><i class="icon-fresh7 fs70"></i></a></h3>
<div class="clear">&nbsp;</div>
<div class="absBL padding5 bg-color-primary w100">
<a href="product.php?p=add" class="fill fs15">Add product&nbsp;<span class="glyphicon glyphicon-plus-sign"></span></a>
</div>
</div>
</div>



</div>
<div class="clear">&nbsp;</div>
<div class="row">
<div class="col-sm-3">
<div class="bg-color-lightPrimary padding5 fg-color-white rel text-center">
<h3 class="text-center margin5"><a href="customer.php" class="fill">Customers <br/><i class="icon-happy50 fs70"></i></a></h3>
<div class="clear">&nbsp;</div>
<div class="absBL padding5 bg-color-primary w100">
<a href="customer.php?p=add" class="fill fs15">Add customer&nbsp;<span class="glyphicon glyphicon-plus-sign"></span></a>
</div>
</div>
</div>

<div class="col-sm-3">
<div class="bg-color-lightPrimary padding5 fg-color-white rel text-center">
<h3 class="text-center margin5"><a href="user.php" class="fill"><span class="">Users</span> <br/><i class="icon-user fs70"></i></a></h3>
<div class="clear">&nbsp;</div>
<div class="absBL padding5 bg-color-primary w100">
<a href="user.php?p=add" class="fill fs15">Add user&nbsp;<span class="glyphicon glyphicon-plus-sign"></span></a>
</div>
</div>
</div>

<div class="col-sm-3">
<div class="bg-color-lightPrimary padding5 fg-color-white rel text-center">
<h3 class="text-center margin5"><a href="supplier.php" class="fill"><span class="">Suppliers</span> <br/><i class="icon-moving11 fs70"></i></a></h3>
<div class="clear">&nbsp;</div>
<div class="absBL padding5 bg-color-primary w100">
<a href="supplier.php?p=add" class="fill fs15">Add Supplier&nbsp;<span class="glyphicon glyphicon-plus-sign"></span></a>
</div>
</div>
</div>

<div class="col-sm-3">
<div class="bg-color-lightPrimary padding5 fg-color-white rel text-center">
<h3 class="text-center margin5"><a href="purchase.php" class="fill">Purchases <br/><i class="icon-packages3 fs70"></i></a></h3>
<div class="clear">&nbsp;</div>
<div class="absBL padding5 bg-color-primary w100">
<a href="stock.php?p=add" class="fill fs15">Add Purchase&nbsp;<span class="glyphicon glyphicon-plus-sign"></span></a>
</div>
</div>
</div>
</div>

<div class="clear">&nbsp;</div>
<div class="row">

<div class="col-sm-3">
<div class="bg-color-lightPrimary padding5 fg-color-white rel text-center">
<h3 class="text-center margin5"><a href="report.php" class="fill"><span class="">Reports</span> <br/><i class="icon-circular fs70"></i></a></h3>
<div class="clear">&nbsp;</div>
<div class="absBL padding5 bg-color-primary w100">
&nbsp;
</div>
</div>
</div>

<div class="col-sm-3">
<div class="bg-color-lightPrimary padding5 fg-color-white rel text-center">
<h3 class="text-center margin5"><a href="setting.php" class="fill">Settings <br/><i class="icon-cog fs70"></i></a></h3>
<div class="clear">&nbsp;</div>
<div class="absBL padding5 bg-color-primary w100">
&nbsp;
</div>
</div>
</div>

<div class="col-sm-6 no-padding">
<div class="row fg-color-white adj">
<div class="col-sm-6 padR0">
<div class="bg-color-green padding5">
<small>Today's Gross Sales</small>
<div class="clearfix"></div>
<span class="fs25 lead">₦<?=number_format($todaySales,2);?></span>
<div class="clearfix"></div>
</div>
</div>

<div class="col-sm-6">
<div class="bg-color-blue padding5">
<small>Products Sold</small>
<div class="clearfix"></div>
<span class="fs25 lead"><?=number_format($productSold);?></span>
<div class="clearfix"></div>
</div>
</div>
<div class="clearfix">&nbsp;</div>
<div class="col-sm-12">
<a href="bargen.php" class="btn btn-primary btn-block btn-large"><i class="glyphicon glyphicon-barcode" style="color:#fff !important"></i>&nbsp;Barcode Generator</a>
</div>

</div>

</div>

</div>

</div>
<div class="col-md-4">
<div class="panel panel-primary adj">
  <div class="panel-heading">
    <h3 class="panel-title">Gross Stock Valuation</h3>
    
  </div>
  <div class="panel-body">
  <h3>₦<?=number_format($this->data["valuation"]["tcp"],2);?><span class="label label-default pull-right">Cost price</span></h3>
  <h3>₦<?=number_format($this->data["valuation"]["tsp"],2);?><span class="label label-success pull-right">Sale price</span></h3>
  </div>
</div>

<div class="ae-well padding10">
<?php 
$grossMarginProfit=round((($this->data["valuation"]["tsp"]-$this->data["valuation"]["tcp"])/$this->data["valuation"]["tsp"])*100);
?>
<h3 class="lead mt0">Gross Profit Margin</h3>
<div id="profit_chart">
<canvas></canvas>
</div>
</div>


</div>
</div>
<div class="clearfix">&nbsp;</div>
<div class="row">
<div class="col-md-12">
<div class="row">
<div class="col-sm-3 bg-color-warning metric">
<a href="report.php#reorder" class="fill fs17 white"><strong><?=$this->data["valuation"]["rol"];?></strong><br/><span>Re-order level</span></a>
<div class="clear"></div>
</div>
<div class="col-sm-3 bg-color-red metric">
<a href="report.php#outofstock" class="fill fs17 white"><strong><?=$this->data["valuation"]["os"];?></strong><br/><span>Out of stock</span></a>
<div class="clear"></div>
</div>

<div class="col-sm-3 bg-color-blue metric">
<a href="report.php#topselling" class="fill fs17 white" title="Top selling products in a month ago"><strong>10</strong><br/><small>Top selling in 30days</small></a>
<div class="clear"></div>
</div>

<div class="col-sm-3 bg-color-green metric">
<a href="report.php#backorder" class="fill fs17 white"><strong><?=intval($this->data["valuation"]["bo"]);?></strong><br/><span>Back-order</span></a>
<div class="clear"></div>
</div>
</div>
</div>
</div>