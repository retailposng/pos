<?php 
include '_data.php';
global $isDeleted;
if($isDeleted===true){
	?>
	<div class="padding10">
	<div class="alert alert-success alert-dismissible" role="alert">
  <button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
   <i class="glyphicon glyphicon-ok"></i>&nbsp;Successfully removed
	</div>
	</div>
	<?php
}
?>
<div class="col-md-9">
<h3 class="lead">Net Sales <span class="label bg-color-green pull-right no-radius" id="_income">Net Income: ₦<?=number_format(array_sum($profits),2);?></span><span class="label label-warning no-radius pull-right" id="_revenue">Revenue: ₦<?=number_format(array_sum($sales),2);?></span></h3>
<div id="sales_chart">
<canvas height="200" width="600"></canvas>
</div>
</div>
<div class="col-md-3">
<h3 class="lead">Profit Margin</h3>
<div id="profit_chart">
<canvas></canvas>
</div>
</div>
<div class="clear">&nbsp;</div>
<div class="col-md-12" id="_analysis">
</div>
<div class="clear">&nbsp;</div>
<div class="col-md-4">
<h3 class="lead">Acquisitions</h3>
<div class="list-group" id="acqui_list">
<?php 
foreach ($acquisitions as $k=>$v){
	?>
	<a href="<?=$k;?>" class="list-group-item">
  <span class="badge"><?=$v[1];?></span>
    <p class="list-group-item-text"><?=$v[0];?></p>
  </a>
	<?php
}
?>
</div>
</div>
<div class="col-md-4">
<h3 class="lead">Analyze product</h3>
<form action="javascript:void(0)" method="post">
<input type="text" name="sku" class="form-control" placeholder="Enter SKU code"/>
<div class="clear">&nbsp;</div>
<button type="submit" class="btn btn-primary pull-right" onclick="analyzeProduct(this.form['sku'].value)">Analyze</button>
</form>
</div>
<div class="col-md-4">
<h3 class="lead">Gross Stock Valuation</h3>
<div class="panel panel-primary">
  <div class="panel-body">
  <h3>₦<?=number_format($tv["tcp"],2);?><span class="label label-default pull-right">Cost price</span></h3>
  <h3>₦<?=number_format($tv["tsp"],2);?><span class="label label-success pull-right">Sale price</span></h3>
  </div>
</div>
</div>
<div class="clear">&nbsp;</div>
<div class="col-md-5">
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">10 top selling products in the past 10 days</h3>
  </div>
  <div class="panel-body">
   <table class="table table-striped">
   <thead>
   <tr><th>#</th><th></th><th>Product</th><th>Qty.</th><th>In stock</th></tr>
   </thead>
    <tbody>
    <?php 
    $i=1;
    foreach ($pdo->query("select * from best_sell order by freq desc limit 10") as $fetch){
    ?>
    <tr><td><?=$i++;?></td>
    <td class="mini-thumbnail">
<a href="javascript:void(0)" class="thumbnail no-margin mini-thumbnail">
<img src="views/upload/thumbnail/<?=$fetch["iname"];?>"  />
</a>
</td>
    <td><a href="#analyze" onclick="analyzeProduct('<?=$fetch["SKU"];?>')"><?=$fetch["name"];?></a></td><td><?=$fetch["freq"];?></td><td><?=$fetch["instock"];?></td></tr>
 	 <?php 
 	 }
 	 ?>
    </tbody>
    </table>
  </div>
  </div>
<div class="clearfix">&nbsp;</div>
<a href="#" name="topselling"></a>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">10 top selling products in the past 1 month</h3>
  </div>
  <div class="panel-body">
   <table class="table table-striped">
   <thead>
   <tr><th>#</th><th></th><th>Product</th><th>Qty.</th><th>In stock</th></tr>
   </thead>
    <tbody>
    <?php 
    $i=1;
    foreach ($pdo->query("select * from best_sell_1m order by freq desc limit 10") as $fetch){
    ?>
    <tr><td><?=$i++;?></td>
    <td class="mini-thumbnail">
<a href="javascript:void(0)" class="thumbnail no-margin mini-thumbnail">
<img src="views/upload/thumbnail/<?=$fetch["iname"];?>"  />
</a>
</td>
    <td><a href="#analyze" onclick="analyzeProduct('<?=$fetch["SKU"];?>')"><?=$fetch["name"];?></a></td><td><?=$fetch["freq"];?></td><td><?=$fetch["instock"];?></td></tr>
 	 <?php 
 	 }
 	 ?>
    </tbody>
    </table>
  </div>
  </div>
<div class="clearfix">&nbsp;</div>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">10 Most sold products</h3>
  </div>
  <div class="panel-body">
    <table class="table table-striped">
     <thead>
   <tr><th>#</th><th></th><th>Product</th><th>Qty.</th></tr>
   </thead>
    <tbody>
    <?php 
    $i=1;
    foreach ($pdo->query("select * from most_sold order by freq desc limit 10") as $fetch){
    ?>
    <tr><td><?=$i++;?></td>
    <td class="mini-thumbnail">
<a href="javascript:void(0)" class="thumbnail no-margin mini-thumbnail">
<img src="views/upload/thumbnail/<?=$fetch["iname"];?>"  />
</a>
</td>
    <td><a href="#analyze" onclick="analyzeProduct('<?=$fetch["SKU"];?>')"><?=$fetch["name"];?></a></td><td><?=$fetch["freq"];?></td></tr>
 	 <?php 
 	 }
 	 ?>
    </tbody>
    </table>
  </div>
</div>
</div>
<div class="col-md-7">
<a href="#" name="backorder"></a>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Products in Back-Order</h3>
  </div>
  <div class="panel-body">
   <table class="table table-striped">
     <thead>
   <tr><th>#</th><th></th><th>Product</th><th>Qty.</th><th>Customer</th><th>Date</th><th></th></tr>
   </thead>
    <tbody>
    <?php 
    $i=1;
    foreach ($pdo->query("select * from backorder") as $fetch){
    ?>
    <tr><td><?=$i++;?></td>
    <td class="mini-thumbnail">
<a href="javascript:void(0)" class="thumbnail no-margin mini-thumbnail">
<img src="views/upload/thumbnail/<?=$fetch["iname"];?>"  />
</a>
</td>
    <td><a href="#analyze" onclick="analyzeProduct('<?=$fetch["SKU"];?>')"><?=$fetch["mname"]." ".$fetch["pname"];?></a></td><td><?=$fetch["qty"];?></td><td><?=$fetch["cname"];?></td><td><?=$time->format($fetch["sdate"]);?></td><td><a href="report.php?backorder=<?=$fetch["saleID"];?>" class="btn btn-danger btn-sm"><i class="glyphicon glyphicon-trash"></i></a></td></tr>
 	 <?php 
 	 }
 	 ?>
    </tbody>
    </table>
  </div>
</div>
<div class="clearfix">&nbsp;</div>
<a href="#" name="reorder"></a>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Products in Re-order level</h3>
  </div>
  <div class="panel-body">
   <table class="table table-striped">
     <thead>
   <tr><th>#</th><th></th><th>Product</th><th>Category</th><th>Qty</th><th>Cost price</th><th>Supplier</th></tr>
   </thead>
    <tbody>
    <?php 
    $i=1;
    foreach ($pdo->query("select s.* from (reorderlevel s)") as $fetch){
    ?>
    <tr><td><?=$i++;?></td>
    <td class="mini-thumbnail">
<a href="javascript:void(0)" class="thumbnail no-margin mini-thumbnail">
<img src="views/upload/thumbnail/<?=$fetch["iname"];?>"  />
</a>
</td>
    <td><a href="#analyze"><?=$fetch["pname"];?></a></td><td><?=$fetch["cname"];?></td><td><?=$fetch["qty"];?></td><td><?=$fetch["cprice"];?></td><td><a href=""><?=$fetch["supplier"];?></a></td></tr>
 	 <?php 
 	 }
 	 ?>
    </tbody>
    </table>
  </div>
  
</div>
<div class="clearfix">&nbsp;</div>
<a href="#" name="outofstock"></a>
<div class="panel panel-primary">
  <div class="panel-heading">
    <h3 class="panel-title">Out of stock items</h3>
  </div>
  <div class="panel-body">
   <table class="table table-striped">
     <thead>
   <tr><th>#</th><th></th><th>Product</th><th>Category</th><th>Back-order</th><th>Cost price</th><th>Supplier</th></tr>
   </thead>
    <tbody>
    <?php 
    $i=1;
    foreach ($pdo->query("select s.* from (outofstock s)") as $fetch){
    ?>
    <tr><td><?=$i++;?></td>
    <td class="mini-thumbnail">
<a href="javascript:void(0)" class="thumbnail no-margin mini-thumbnail">
<img src="views/upload/thumbnail/<?=$fetch["iname"];?>"  />
</a>
</td>
    <td><a href="#analyze"><?=$fetch["pname"];?></a></td><td><?=$fetch["cname"];?></td><td>0</td><td><?=$fetch["cprice"];?></td><td><a href=""><?=$fetch["supplier"];?></a></td></tr>
 	 <?php 
 	 }
 	 ?>
    </tbody>
    </table>
  </div>
  
</div>
</div>