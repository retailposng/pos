<div class="clear">&nbsp;</div>
<div class="col-sm-12">
<table class="table table-striped table-bordered" id="tb_data">
<thead>
<tr><th>#</th><th>Name</th><th>In stock</th><th>Product</th><th></th></tr>
</thead>
<tbody>
<?php
$i=1; 
foreach ($this->pdo->query("select c.*,(select sum(s.qty) from (stock s join product p on p.productID) where p.productID=s.productID and c.categoryID=p.categoryID) as instore,(select count(p.categoryID) from (stock s join product p on p.productID) where p.productID=s.productID and c.categoryID=p.categoryID) as pro from category c") as $fetch){
?>
<tr><td><?=$i;?></td><td><?=$fetch["name"];?></td><td><?=$fetch["instore"];?></td><td><?=$fetch["pro"];?></td><td><a href="category.php?p=edit&name=<?=$fetch["name"];?>&id=<?=$fetch["categoryID"];?>" class="btn btn-sm btn-default"><i class="glyphicon glyphicon-edit"></i></a></td></tr>
<?php 
$i++;
}
?>
</tbody>
</table>
</div>