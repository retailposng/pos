<div class="row">
<div class="col-md-2 no-padding"><h3 class="no-margin lh40">Users</h3></div>
<div class="col-md-6 no-padding adjMt10">
<div class="input-group">
                <div class="input-group-btn search-panel">
                    <a href="user.php?p=add" class="btn btn-primary">Add user&nbsp;<i class="glyphicon glyphicon-plus"></i></a>
             	</div>
                <input type="hidden" name="search_param" value="all" id="search_param">         
                <input type="text" class="form-control" name="x" placeholder="Filter" id="tb_data_search" onmouseover="this.focus()"/>
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
            </div>
            <div class="col-md-4 adjMt10">
            <?php 
            include_once 'views/stock/alert.php';
            ?>
            </div>
            </div>