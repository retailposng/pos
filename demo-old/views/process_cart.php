<form action="javascript:void(0)" method="post">
<div class="process row rel" id="process_box">
    <div class="col-md-3 borderR">
    <h3 class="bb-blue">Customer detail</h3>
    
    <div class="control-group">
    <label class="control-label">Full Name</label>
    <input type="text" name="fname" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Email Address</label>
    <input type="text" name="email" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Phone Number</label>
    <input type="text" name="phone" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Home/Office Address</label>
    <textarea class="form-control" rows="5" name="addr">
    </textarea>
    </div>
    
    
    </div>
    <div class="col-md-9 rel h">
    <h3 class="bb-blue">Customer Items  <a href='javascript:void(0)' onclick="ProcessCart.open(getObj('process_box'))" class="close" title="Close">&times;</a></h3>
   <div class="items-detail" id="items_detail"><h1>Please wait...</h1></div>
   <div class="clear"></div>
    </div>
    <div class="absTL w h padding10 hide" style="background:#fff"><h1>Please wait...</h1></div>
    </div>
</form>
<iframe src="javascript:void())" name="checkoutFrame" id="checkout_frame" class="hide"></iframe>

