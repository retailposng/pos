<?php
global $pdo;
include 'lib/options.php';
$date=getdate(time());
$arr_mon=array(1=>"January","February","March","April","May","June","July","August","September","October","November","December");
?>
<div class="row">
<div class="col-md-2 no-padding"><h3 class="no-margin lh40">Sales</h3></div>
<div class="col-md-6 no-padding adjMt10">
<div class="input-group" >


<span class="input-group-btn" id="dp_sale" data-date="<?=date("d-m-Y");?>" data-date-format="dd-mm-yyyy">
<button class="btn btn-default" type="button"><span class="glyphicon glyphicon-calendar"></span></button>
</span>


                <input type="text" class="form-control" id="tb_data_search" onmouseover="this.focus()" name="x" placeholder="Search" onkeyup="xSearchList.filter(this.value)">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
            
            
            <span id="_index"></span>
            </div>
            <div class="col-md-4 no-padding adjMt10">
            <?php 
            include_once 'views/stock/alert.php';
            ?>
            </div>
            </div>