<?php 
global $pdo;
?>
<div class="clear">&nbsp;</div>
<div class="col-md-12">
<table class="table table-striped table-bordered tb_data">
<thead>
<tr><th>#</th><th>Name</th><th>Email</th><th>Phone</th><th>Address</th><th>Purchased</th><th></th></tr>
</thead>
<tbody>
<?php
$i=1; 
foreach ($pdo->query("select c.*,(select sum(s.qty) from sale s where s.customerID=c.customerID) as qty from customer c order by qty desc,c.date asc") as $fetch){
?>
<tr><td><?=$i;?></td><td><?=$fetch["name"];?></td><td><?=$fetch["email"];?></td><td><?=$fetch["phone"];?></td><td><?=$fetch["addr"];?></td><td><a href="#"><?=$fetch["qty"];?></a></td><td><a href="customer.php?p=edit&name=<?=$fetch["name"];?>&email=<?=$fetch["email"];?>&phone=<?=$fetch["phone"];?>&addr=<?=$fetch["addr"];?>&id=<?=$fetch["customerID"];?>" class="btn btn-sm btn-default"><i class="glyphicon glyphicon-edit"></i></a></td></tr>
<?php 
$i++;
}
?>
</tbody>
</table>
</div>