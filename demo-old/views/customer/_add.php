<?php 
global $isOk,$post,$err;
if(isset($post)) $_POST=$post;
ob_start();
?>
<div class="row bg-color-white padding10">
<div class="col-md-7">
<?php
if ($isOk) {
	echo "<div class='alert alert-success padding5'>Successfully added</div>";
}

$err_arr=array(1=>"Enter customer name",23000=>"Duplicate data found either email/phone number already exist");
if(!empty($err)) echo "<div class='alert alert-danger padding5'>{$err_arr[$err]}</div>";	
?>
<form action="<?=$_SERVER["PHP_SELF"];?>?p=add" method="post">
<div class="control-group">
    <label class="control-label">Full Name</label>
    <input type="text" name="name" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Email Address</label>
    <input type="text" name="email" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Phone Number</label>
    <input type="text" name="phone" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Home/Office Address</label>
    <textarea class="form-control" rows="5" name="addr">
    </textarea>
    </div>
  <div class="clear">&nbsp;</div>
   <div class="control-group">
  <button type="submit" name="customer" class="btn btn-primary">Add</button>
    </div>
  </form>
</div>
<div class="col-md-5">

</div>
</div>
<?php 
$contents=ob_get_clean();
include_once 'views/setting/_body.php';
?>