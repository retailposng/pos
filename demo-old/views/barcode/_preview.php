<?php 
global $item;
$vat=$item["vat"];
$vat_str=NULL;
$vat_type=['#inclusive'=>'Inclusive','#exclusive'=>'Exclusive'];
if(in_array($vat["type"],["#inclusive","#exclusive"])) $vat_str="<small>+ {$vat['val']}% vat {$vat_type[$vat['type']]}</small>";
?>
<div class="row">
<div class="col-xs-12 text-center">
<h3 class="lead fg-color-dull"><?=$item["mname"]." ".$item["pname"];?></h3>
<div class="rel">
<img alt="item" src="views/upload/large/<?=$item["iname"];?>" class="img-responsive"/>
<span class="absBL label label-warning">Quantity: <?=$item["qty"];?></span>
</div>
<div class="clearfix">&nbsp;</div>
</div>
<div class="col-xs-12">
<table class="table table-striped">
<tbody>
<tr><td><strong>Manufacturer</strong></td><td><?=$item["mname"];?></td></tr>
<tr><td><strong>Product</strong></td><td><?=$item["pname"];?></td></tr>
<tr><td><strong>Category</strong></td><td><?=$item["cname"];?></td></tr>
<tr><td><strong>Sale price</strong></td><td>₦<?=number_format($item["rsprice"],2);?> <?=$vat_str;?></td></tr>
</tbody>
</table>
<div class="clearfix">&nbsp;</div>
</div>
<div class="col-xs-12 text-center">
<button type="button" class="btn btn-danger btn-block" onclick="window.close()">Close window</button>
<div class="clearfix">&nbsp;</div>
</div>
</div>
 

