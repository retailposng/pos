<?php 
include '../config.php';
ini_set("display_errors",1);
//print_r($_POST);
$hasCodes=($_POST["hasCodes"]=="true")?(true):(false);
?>
<table class="table table-striped">
    <thead>
    <?php 
    if($hasCodes==false){
    ?>
    <tr><th>#</th><th>Category</th><th>Product</th><th>Qty</th><th>Price</th><th>Line Total</th></tr>
    <?php 
    }
    else{
	?>
	 <tr><th>#</th><th>Category</th><th>Product</th><th>Product Code <a href='' class='pull-right'><i class='glyphicon glyphicon-barcode'></i></a></th><th>Qty</th><th>Price</th></tr>
	<?php
	}
    ?>
    </thead>
    <tbody>
    <?php 
    $i=1;
    $linetotal=array();
    $stock=$pdo->prepare("select c.name as cname,m.name as mname,p.name as pname,s.* from (product p join stock s on s.productID join manufacturer m on m.manufacturerID join category c on c.categoryID) where s.productID=p.productID and m.manufacturerID=p.manufacturerID and c.categoryID=p.categoryID and s.stockID=:stockID");
    foreach($_POST["params"] as $k=>$data){
	list($stockID,$qty)=explode(",",$data);
	$stock->execute(array("stockID"=>$stockID));
	$fetch=$stock->fetch(PDO::FETCH_ASSOC);
	if($hasCodes==false){
	$linetotal[]=$fetch['sprice']*$qty;
	?>
    <tr><td><?=$i++;?></td><td><input type='hidden' name='cname[]' value='<?=$fetch['cname'];?>'/><?=$fetch['cname'];?></td><td><input type='hidden' name='pname[]' value='<?=($fetch['mname']=="self")?("{$fetch['pname']}"):("{$fetch['mname']} - {$fetch['pname']}");?>'/><?=$fetch['mname'];?> - <?=$fetch['pname'];?></td><td><?=$qty;?></td><td><input type='hidden' name='sprice[]' value='<?=$fetch['sprice'];?>'/><?=number_format($fetch['sprice']*$qty);?></td><td><?=number_format($fetch['sprice']*$qty);?></td></tr>
    <?php 
	}
	else{
	$productCodes=array();
	for($j=0;$j<$qty;$j++){
	$linetotal[]=$fetch['sprice'];	
	if(!empty($fetch['codes'])){
		$productCodes=json_decode($fetch['codes'],1);
	}
	?>
    <tr><td><?=$i++;?></td><td><input type='hidden' name='cname[<?=$stockID;?>][]' value='<?=$fetch['cname'];?>'/><?=$fetch['cname'];?></td><td><input type='hidden' name='pname[<?=$stockID;?>][]' value='<?=($fetch['mname']=="self")?("{$fetch['pname']}"):("{$fetch['mname']} - {$fetch['pname']}");?>'/><?=$fetch['mname'];?> - <?=$fetch['pname'];?></td><td><input type="text" name="codes[<?=$stockID;?>][]" class="form-control" onmouseover="this.focus()" value="<?=@$productCodes[$k];?>"/></td><td>1</td><td><input type='hidden' name='sprice[<?=$stockID;?>][]' value='<?=$fetch['sprice'];?>'/><?=number_format($fetch['sprice']);?></td></tr>
    <?php 
    }
    }
    echo "<input type='hidden' name='stockID[]' value='{$stockID}'/><input type='hidden' name='qty[]' value='{$qty}'/>";
   
 	}
    ?>
    </tbody>
    </table>
    
    <div class="process-footer row">
    <div class="col-sm-3">Total &raquo; <small>₦<?=number_format(array_sum($linetotal),2);?></small> </div>
    <div class="col-sm-5" id="checkout_err"></div>
    <div class="col-sm-4 no-padding">
    <label><input type="checkbox" name="invoice"/>&nbsp;Print Invoice</label>
     <button type="submit" onclick="Checkout.process(this.form)" name="checkout" class="btn btn-default pull-right">Checkout</button>
    </div>
    <div class="clear"></div>
    </div>