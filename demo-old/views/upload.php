<?php
ini_set("display_errors",1);
session_start();
session_regenerate_id();
include_once '../config.php';
$usrID=intval($_SESSION['usrID']);
$instructorID=intval($_POST['instructorID']);
function __autoload($class_name){
	include_once "../lib/{$class_name}.php";
} 
include_once '../lib/smartResizeImage.php';

	$filename=@$_POST['isUpload'];
	if (!empty($filename) && file_exists("upload/$filename")){
		unlink("upload/$filename");
		}


try{
if(isset($_FILES)){
if(!empty($_FILES['file']['name'])){
	if($_FILES['file']['error'] > 0) Throw new PDOException(1);
	list($width,$height,$imageType)=getimagesize($_FILES['file']['tmp_name']);
	$imageType=image_type_to_mime_type($imageType);
	if(!in_array($imageType,array("image/gif","image/pjpeg","image/jpeg","image/jpg","image/png","image/x-png"))) Throw new PDOException(2);
	$rand=new RandNumGen;
	$filename=$rand->init(10).".png";
	if(is_uploaded_file($_FILES['file']['tmp_name'])){
		move_uploaded_file($_FILES['file']['tmp_name'],"upload/large/$filename");
		smart_resize_image("upload/large/$filename",64,64,true,"upload/thumbnail/$filename");
		//$pdo->exec("update instructor set logo='{$filename}' where instructorID=$instructorID");
		echo "<script>window.parent.ImageUpload.track('views/upload/thumbnail/{$filename}');</script>";
			}
			
}
}
}
catch (PDOException $e) {
	$err=$e->getMessage();
	echo "<script>window.parent.ImageUpload.track({$err});</script>";
}
?>