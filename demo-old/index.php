<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Retail store point of sale</title>    
    <link href="css/styles.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
    .jumbotron {
	text-align: center;
	width: 30rem;
	border-radius: 0.5rem;
	top: 0;
	bottom: 0;
	left: 0;
	right: 0;
	position: absolute;
	margin: 4rem auto;
	background-color: #fff;
	padding: 2rem;
}

.container .glyphicon-shopping-cart {
	font-size: 10rem;
	margin-top: 3rem;
	color: #428BCA;
}

input {
	width: 100%;
	margin-bottom: 1.4rem;
	padding: 1rem;
	background-color: #ecf2f4;
	border-radius: 0.2rem;
	border: none;
}
h2 {
	margin-bottom: 3rem;
	font-weight: bold;
	color: #ababab;
}
.btn {
	border-radius: 0.2rem;
}
.btn .glyphicon {
	font-size: 3rem;
	color: #fff;
}
.full-width {
	background-color: #8eb5e2;
	width: 100%;
	-webkit-border-top-right-radius: 0;
	-webkit-border-bottom-right-radius: 0;
	-moz-border-radius-topright: 0;
	-moz-border-radius-bottomright: 0;
	border-top-right-radius: 0;
	border-bottom-right-radius: 0;
}

.box {
	position: absolute;
	bottom: 0;
	left: 0;
	margin-bottom: 3rem;
	margin-left: 3rem;
	margin-right: 3rem;
}
    </style>
  </head>
  <body>
  
  <div class="clearfix">&nbsp;</div>
  <div class="clearfix">&nbsp;</div>
  <div class="clearfix">&nbsp;</div>
  <div class="clearfix">&nbsp;</div>
 
 
 
 <div class="jumbotron">

  
  <div class="container">
    <span class="glyphicon glyphicon-shopping-cart"></span>
    <h2>Retail-POS</h2>
    <div class="boxx">
     <?php 
            if(isset($_GET["auth"])){
            ?>
            <div class="alert alert-warning"><?=$_GET["auth"];?></div>
            <?php 
            }
            ?>
    <form action="models/__login.php" method="post" id="login" role="form">
        <input type="text" name="email" placeholder="Email Address">
	    <input type="password" name="pwd" placeholder="password" autocomplete="off">
	    <button type="submit" name="login" class="btn btn-default full-width"><span class="glyphicon glyphicon-ok"></span></button>
     </form>
    </div>
  </div>
</div>
 
 
  
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript">
   
    	    
    </script>
  </body>
</html>
