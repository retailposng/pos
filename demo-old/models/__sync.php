<?php 
include_once 'lib/smartResizeImage.php';
include_once 'lib/RandNumGen.php';
$rand=new RandNumGen;
$isOk=false;
$err=NULL;
try{
	if(isset($_FILES) && !empty($_FILES)){
		/* print("<pre>");
		print_r($_FILES);
		print("</pre>"); */
		for ($i=0,$j=1;$i<count($_FILES["file"]);$i++,$j++){
		if(!empty($_FILES['file']['name'][$i])){
			if($_FILES['file']['error'][$i] > 0) Throw new PDOException("Image $j might be too large");
			list($width,$height,$imageType)=getimagesize($_FILES['file']['tmp_name'][$i]);
			$imageType=image_type_to_mime_type($imageType);
			if(!in_array($imageType,array("image/gif","image/pjpeg","image/jpeg","image/jpg","image/png","image/x-png"))) Throw new PDOException("File format not allowed");
			$filename=$rand->init(10).".png";
			if(is_uploaded_file($_FILES['file']['tmp_name'][$i])){
				move_uploaded_file($_FILES['file']['tmp_name'][$i], "sync/large/$filename");
				//smart_resize_image($_FILES['file']['tmp_name'][$i],1000,1000,true,"synfolder/$filename");
			}
				
		} 
		}
	}
	$isOk=true;
}
catch (PDOException $ex) {
	$err=$ex->getMessage();
}
?>