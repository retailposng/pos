<?php
$isOk=false;
if (isset($_POST["profile"])) {
	try{
		$filename=NULL;
		if(isset($_FILES)){
			function __autoload($class_name){
				include_once "lib/{$class_name}.php";
			}
			include_once 'lib/smartResizeImage.php';
			if(!empty($_FILES['file']['name'])){
				if($_FILES['file']['error'] > 0) Throw new PDOException(1);
				list($width,$height,$imageType)=getimagesize($_FILES['file']['tmp_name']);
				$imageType=image_type_to_mime_type($imageType);
				if(!in_array($imageType,array("image/gif","image/pjpeg","image/jpeg","image/jpg","image/png","image/x-png"))) Throw new PDOException(2);
				if(is_uploaded_file($_FILES['file']['tmp_name'])){
					$filename="store.png";
					smart_resize_image($_FILES['file']['tmp_name'],100,100,true,"images/logo/{$filename}");
				}
					
			}
		}
		
		$profile=$pdo->prepare("replace profile set profileID=1,name=:name,storeID=:storeID,phone=:phone,email=:email,web=:web,addr=:addr,logo=:logo");
		$profile->execute(array("name"=>$_POST["name"],"storeID"=>$_POST["storeID"],"phone"=>$_POST["phone"],"email"=>$_POST["email"],"web"=>$_POST["web"],"addr"=>$_POST["addr"],"logo"=>(is_null($filename))?("pad_blue.png"):("store.png")));
		$isOk=true;
	}
	catch (PDOException $e) {
		echo $err=$e->getMessage();
		
	}
}

if (isset($_POST["sync"])){
	$config=$pdo->prepare("update sync set auto_sync=:auto_sync,syncKey=:syncKey where syncID=1");
	$config->execute(array("auto_sync"=>isset($_POST["auto_sync"])?1:0,"syncKey"=>$_POST["syncKey"]));
	$isOk=true;
}

if (isset($_POST["invoice"])){
	$config=$pdo->prepare("update config set invoice_note=:note,item_desc=:item_desc,invoice_printer=:printer where configID=1");
	$config->execute(array("note"=>$_POST["note"],"item_desc"=>@$_POST["item_desc"],"printer"=>$_POST["printer"]));
	$isOk=true;
}


if (isset($_POST["expiry"])){
	$config=$pdo->prepare("update config set expiry=:expiry where configID=1");
	$config->execute(array("expiry"=>isset($_POST["status"])?1:0));
	$isOk=true;
}

if (isset($_POST["alert"])){
	$config=$pdo->prepare("update config set alert_interval=:alert_interval,alert_noti=:alert_noti where configID=1");
	$config->execute(array("alert_interval"=>$_POST["alert_interval"],"alert_noti"=>json_encode(array($_POST["alert_days"],$_POST["alert_noti"]))));
	$isOk=true;
}

	$profile=$pdo->query("select * from profile");
	$post=$profile->fetch(PDO::FETCH_ASSOC);
	$config=$pdo->query("select *,invoice_note as note,invoice_printer as printer from config where configID=1");
	$post=array_merge($post,$config->fetch(PDO::FETCH_ASSOC));