<?php
$isOk=false;
$err='';
if (isset($_POST["store"])) {
	try{
		if(empty($_POST["name"])) throw new Exception(NULL,1);
		if(isset($_GET["p"]) && $_GET["p"]=="edit"){
			$store=$pdo->prepare("update ignore store set name=:name,phone=:phone,email=:email,addr=:addr where storeID=:storeID");
			$store->execute(array("name"=>$_POST["name"],"phone"=>$_POST["phone"],"email"=>$_POST["email"],"addr"=>$_POST["addr"],"storeID"=>intval($_POST["id"])));
		}
		else{
			$store=$pdo->prepare("insert into store set name=:name,email=:email,phone=:phone,addr=:addr,date=unix_timestamp()");
			$store->execute(array("name"=>$_POST["name"],"email"=>$_POST["email"],"phone"=>$_POST["phone"],"addr"=>$_POST["addr"]));
		}
		$isOk=true;
	}
	catch (Exception $e) {
	//	echo $e->getMessage();
		$err=$e->getCode();
	}
}