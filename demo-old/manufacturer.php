<?php
session_start();
$_SESSION=json_decode($_COOKIE["cred"],1);
$usrID=intval($_SESSION['usrID']);
include_once 'layout.php';
include_once 'models/__manufacturer.php';
$path=pathinfo(__FILE__);
$layout=new Layout($path['filename']);
$layout->title="{$layout->controllers()["profile"]["name"]} | Manufacturers";
switch (@$_GET["p"]) {
	case "add":
	case "edit":
		if(!in_array($_SESSION["roleID"],array(1,2))){
			header("Location: dashboard.php?auth=Access denied");
			exit(0);
		}
		$layout->content(NULL,"views/manufacturer/_edit.php");
		break;
	default:
		$layout->content("views/manufacturer/_toolbar.php","views/manufacturer/_body.php");
		break;
}
