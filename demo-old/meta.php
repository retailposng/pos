<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">  
<meta name="keywords" content="pos software, ipad pos, retail management software, web based pos, online pos, point-of-sale software, point-of-sale">
<meta name="description" content="A simple & flexible web-based point of sale for retail stores, Our web based POS will manage your sales, stocks and real-time reporting of daily sales. Depending on your needs and the type of business you have, we have systems specifically designed for retails: electronics, phones, restaurant, bar, grocery, salon and more, a web based application. The system is made to provide the most efficient and productive POS experience for you and your customers.">
<meta name="author" content="Adepoju Aliu" >  