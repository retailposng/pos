<?php 
ini_set("display_errors",1);
$filter="s.date=date_format(now(),'%Y%m%d')";
$date=date("Y-m-d");
$saleType="sold";
$bool=false;
$syncID=@intval($_SESSION["syncID"]);
if(isset($_POST["date"])){
	session_start();
	session_regenerate_id();
	$syncID=intval($_SESSION["syncID"]);
	include_once '../config.php';
	include_once '../lib/Time.php';
	include_once '../store/data.php';
	$date=$_POST["date"];
	$filter="s.date=".str_replace("-","",$_POST["date"]);
	list($y,$m,$d)=explode("-",$_POST["date"]);
	$saleType=$_POST["salesType"];
	$bool=true;
	$valuation=$pdo->query("select * from syncValuation");
	$valuation=$valuation->fetch(PDO::FETCH_ASSOC);
}

$time=new Time();
$sale=$pdo->query("select *,s.product as pname,s.customer as cname,s.qty,((s.sprice*s.qty)-s.deduct) as netsale,s.qty,s.discount,s.status from syncSale s where $filter and s.syncID=$syncID order by s.date desc");
$sale=$sale->fetchAll(PDO::FETCH_ASSOC);
function arrayRowSum($stack,$key){
	$arr=array();
	foreach ($stack as $i=>$data){
		if($data["status"]=="sold")
			$arr[]=$data[$key];
	}
	return array_sum($arr);
}

function arrayRowQuery($stack,$key,$query){
	$arr=array();
	if(is_null($query)){
		foreach ($stack as $i=>$data){
			$arr[]=$data[$key];
		}
		return $arr;
	}
	foreach ($stack as $i=>$data){
		if($data["status"]==$query)
			$arr[]=$key=="*"?$data:$data[$key];

	}
	return $arr;
}

ob_start();
?>
<div class="row">
<div class="col-md-9">
<h3 class="lead">Net Sales <span class="label bg-color-green pull-right no-radius" id="_income">Net Income: ₦<?=number_format(array_sum($profits),2);?></span><span class="label label-warning no-radius pull-right" id="_revenue">Revenue: ₦<?=number_format(array_sum($sales),2);?></span></h3>
<div id="sales_chart_1">
<canvas height="200" width="600"></canvas>
</div>
</div>
<div class="col-md-3">
<h3 class="lead">Profit Margin</h3>
<div id="profit_chart_1">
<canvas></canvas>
</div>

<h3 class="lead">Gross Stock Valuation</h3>
<div class="panel panel-primary">
  <div class="panel-body">
  <h4>₦<?=number_format($valuation["gcp"],2);?><span class="label label-default pull-right">Cost price</span></h4>
  <h4>₦<?=number_format($valuation["gsp"],2);?><span class="label label-success pull-right">Sale price</span></h4>
  </div>
</div>
</div>
</div>

<div class="row mt10 adjMt20" style="padding:0px 10px">
<div class="col-sm-4 no-padding">
<div class="row">
<div class="col-md-6 bg-color-success fg-color-white padding10">
Net Sales<h3 class="no-margin"><strong>₦<?=number_format(arrayRowSum($sale, "netsale"),2);?></strong></h3>
</div>
<div class="col-md-6 bg-color-primary fg-color-white padding10">
Products<h3 class="no-margin"><strong><?=arrayRowSum($sale, "qty");?></strong></h3>
</div>
</div>
</div>
<div class="col-sm-4 no-padding">
<div class="row">
<div class="col-md-6 bg-color-info fg-color-white padding10">
Discount/Allowance<h3 class="no-margin"><strong>₦<?=number_format(arrayRowSum($sale, "deduct"),2);?></strong></h3>
</div>
<div class="col-md-6 bg-color-orange fg-color-white padding10">
Refund/Cancelled<h3 class="no-margin"><strong><?=array_sum(arrayRowQuery($sale,"qty","refund"));?></strong></h3>
</div>
</div>
</div>
<div class="col-sm-4 no-padding">
<div class="row">
<div class="col-md-6 bg-color-warning fg-color-white padding10">
Back Order<h3 class="no-margin"><strong><?=array_sum(arrayRowQuery($sale,"qty","backorder"));?></strong></h3>
</div>
<div class="col-md-6 bg-color-black fg-color-white padding10">
Customers<h3 class="no-margin"><strong><?=count(array_unique(arrayRowQuery($sale,"customerID",NULL)));?></strong></h3>
</div>
</div>
</div>
</div>


<div class="row mt10 ae-well padding10">
<div class="col-md-3">
<div class="input-group" >


<span class="input-group-btn" onmousedown="openCal()" id="dp_sale" data-date="<?=date("d-m-Y");?>" data-date-format="dd-mm-yyyy">
<button class="btn btn-default" type="button"><span class="glyphicon glyphicon-calendar"></span></button>
</span>


                <input type="text" class="form-control" id="tb_data_search" onmouseover="this.focus()" name="x" placeholder="Search" onkeyup="xSearchList.filter(this.value)">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
</div>
<div class=" col-sm-8">
<div class="btn-toolbar">
<div class="btn-group">
<button type="button" onclick="Sales.download('<?=$date;?>',$('#sales_type').val())" class="btn btn-default"><i class="glyphicon glyphicon-download"></i></button>
<button type="button" onclick="Sales.print('<?=$date;?>',$('#sales_type').val())" class="btn btn-default"><i class="glyphicon glyphicon-print"></i></button>
</div>

<div class="btn-group">
<select class="form-control" id="sales_type" onchange="getSalesData('<?=$date;?>',this.value)">
<?php 
                    foreach (array("sold"=>"Sold Items","refund"=>"Refund/Cancelled","backorder"=>"Back Order") as $k=>$v){
						$active=(@$_POST["salesType"]==$k)?("selected=selected"):(NULL);
                    	?>
                    	<option <?=$active;?> value="<?=$k;?>"><?=$v;?></option>
                    	 <?php 
                    }
                    ?>
</select>
</div>

<div class="btn-group">
<small style="line-height:30px">&nbsp;&nbsp;&nbsp;<?=(isset($_POST["date"]) && date("Ymd") > str_replace("-","",$_POST["date"])*1)?(date("D, j M Y",mktime(0, 0, 0,$m,$d,$y))." Sales"):("Today's Sales");?></small>
<small>&nbsp;&nbsp;<i>Last update: <?=$time->format($valuation["lastUpdate"]);?></i></small>
</div>

</div>
</div>

<div class="col-md-1 no-padding">
<div class="btn-group pull-right">
<a href="#table" role="tab" data-toggle="tab" class="btn btn-default active" onclick="toggleBButton(this)"><i class="glyphicon glyphicon-th-list"></i></a>
<a href="#summary" role="tab" data-toggle="tab" class="btn btn-default" onclick="toggleBButton(this)"><i class="glyphicon glyphicon-th-large"></i></a>
</div>

</div>
</div>

<div class="row">
<div class="col-md-12">
<div class="tab-content">
  <div role="tabpanel" class="tab-pane active" id="table">
  <div class="clearfix">&nbsp;</div>
  <table class="table table-striped table-bordered" id="sales_data_table">
<thead>
<tr><th>#</th><th>Invoice</th><th>Items</th><th>Price</th><th>Discount</th><th>Qty.</th><th>Line Total</th><th>Customer</th><th>Seller</th><th>Store</th></tr>
</thead>
<tbody>
<?php 
$discountList=array();
$group=array();
foreach (arrayRowQuery($sale,"*",$saleType) as $i=>$fetch){
	$group[$fetch["invoice"]][]=$fetch;
	$each=json_decode($fetch["discount"],1)["each"];
	$global=json_decode($fetch["discount"],1)["global"];
	//$discountList[]=(((($global*$fetch["sprice"])/100)*$fetch["qty"])+($each*$fetch["qty"]));
	$discountList[]=(($global*$fetch["sprice"])/100)+$each;
	?>
	<tr><td><?=$i+1;?></td>
	<td><?=$fetch["invoice"];?></td><td><?="{$fetch["pname"]}";?></td><td><?=$fetch["sprice"];?></td><td><?=number_format($discountList[$i],2);?></td><td><?=$fetch["qty"];?></td><td><?=number_format(($fetch["sprice"]*$fetch["qty"])-($discountList[$i]*$fetch["qty"]),2);?></td><td><?=$fetch["cname"];?></td><td><?=$fetch["seller"];?></td><td>store<?=$fetch["storeID"];?></td></tr>
	<?php 
}
?>
</tbody>
</table>
  </div>
  <div role="tabpanel" class="tab-pane" id="summary">
  <div class='clearfix'>&nbsp;</div>
  <div id="item_box">
<?php 
$products=array();
$prices=array();
$totalDiscount=array();
$totalLineTotal=array();
foreach($group as $invoice=>$fetch){
?>
<div data-search="1" class="row ae-well padding10" style="margin-bottom:5px">
<div class="col-xs-12 no-padding">
<div class="padding5">
Invoice # - <?=$invoice;?>
<small class="pull-right text-muted"><?=$time->format($fetch[0]["date"]);?></small>
<div class="clear"></div>
<?php 
$linetotal=array();
$discount=array();
foreach ($fetch as $item){
	$each=json_decode($item["discount"],1)["each"];
	$global=json_decode($item["discount"],1)["global"];
	$discount[]=($item["sprice"]*$item["qty"])-(((($global*$item["sprice"])/100)*$item["qty"])+($each*$item["qty"]));
	$linetotal[]=$item["sprice"]*$item["qty"];
	$cname=$item["cname"];
	$products["{$item["pname"]}"][]=$item["qty"];
	$instock["{$item["pname"]}"]=$item["instock"];
?>
<span class="search-data hide"><?="{$invoice} {$item["pname"]} {$time->format($fetch[0]["date"])} {$item["qty"]} {$item["cname"]} {$item["sprice"]}";?></span>
<div class="item-tag" title="<?="Discounts: ₦{$each} + {$global}%";?>">
<?="{$item["pname"]}";?>
&nbsp;&nbsp;<div class="badge bg-color-primary"><?=$item["qty"];?></div>
</div>
<?php 
}
$totalDiscount[]=array_sum($discount);
$totalLineTotal[]=array_sum($linetotal);
?>
</div>
<div class="clear"></div>
<div class="padding5 no-margin">
<span class="label label-warning">₦<?=number_format(array_sum($discount),2);?></span>
<?php
if($linetotal > $discount){
?>
&nbsp;<del>₦<?=number_format(array_sum($linetotal),2);?></del>
<?php 
}
?>
<a href=""><?=$item["cname"];?></a>
</div>
<div class="clear"></div>
</div>

</div>
<?php 
}
?>
</div>
<div class="clearfix">&nbsp;</div>
<table class="table table-striped table-bordered tb_data">
<thead>
<tr><th>#</th><th>Items</th><th>Qty</th><th>Closing stock</th></tr>
</thead>
<tbody>
<?php 
$i=1;
$data=array();
foreach ($products as $product=>$qty){
	$data["product"][]=$product;
	$data["sold"][]=intval(array_sum($qty));
	$data["instock"][]=intval($instock[$product]);
	?>
	<tr><td style="width:30px"><?=$i;?></td>
	<td><?=$product;?></td><td><?=array_sum($qty);?></td><td><?=$instock[$product];?></td></tr>
	<?php 
	$i++;
}
//print_r($data);
?>
</tbody>
</table>
<div class="clearfix">&nbsp;</div>
</div>
</div>

</div>
</div>

<iframe src="javascript:void(0)" id="dl_data" class="hide"></iframe>
<?php 
$contents=ob_get_clean();
echo $bool==true?json_encode(array("contents"=>$contents,"data"=>json_encode(array("label"=>$days,"sales"=>$sales,"profits"=>$profits)),"profitMargin"=>$profitMargin,"revenue"=>array_sum($sales),"income"=>array_sum($profits))):$contents;
?>