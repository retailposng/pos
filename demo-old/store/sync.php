<?php
ini_set("display_errors",1);
session_start();
session_regenerate_id();
if(!isset($_SESSION['usrID'])){
	echo "refresh";
	exit();
}
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: PUT, GET, POST, DELETE, OPTIONS");
header("Access-Control-Allow-Headers: *");
include_once '../config.php';
$usrID=intval($_SESSION['usrID']);
$valuation=json_decode($_POST["valuation"],1)["valuation"];
list($storeID,$syncKey)=explode("@",$valuation['storeCred']);
$date=$_POST["date"];
try{
	$pdo->beginTransaction();
	$verifyKey=$pdo->query("select syncID from syncStore where syncKey='{$syncKey}'");
	if($verifyKey->rowCount() !== 1) Throw new PDOException("Invalid Synchronize key");
	$syncID=$verifyKey->fetch(PDO::FETCH_NUM);
	$sales=$pdo->prepare("replace into syncSale set product=:product,category=:category,qty=:qty,instock=:instock,sprice=:sprice,cprice=:cprice,deduct=:deduct,discount=:discount,invoice=:invoice,serialNo=:serialNo,status=:status,customer=:customer,customerID=:customerID,seller=:seller,usrID=:usrID,saleID=:saleID,storeID=:storeID,syncID=:syncID,date=from_unixtime(:date,'%Y%m%d')");
	foreach (json_decode($_POST["data"],1) as $item){
		$sales->execute(array("product"=>"{$item["mname"]} {$item["pname"]}","category"=>$item["cname"],"qty"=>$item["qty"],"instock"=>$item["qtyR"],"sprice"=>$item["sprice"],"cprice"=>$item["cprice"],"deduct"=>$item["deduct"],"discount"=>$item["discount"],"invoice"=>$item["invoice"],"serialNo"=>$item["serialNo"],"status"=>$item["status"],"customer"=>$item["customer"],"customerID"=>$item["customerID"],"seller"=>$item["seller"],"usrID"=>$item["usrID"],"saleID"=>$item["saleID"],"storeID"=>$storeID,"syncID"=>$syncID[0],"date"=>$item["sdate"]));
	}
	
	$pdo->exec("replace into syncValuation set outofstock={$valuation['os']},reorderlevel={$valuation['rol']},backorder={$valuation['bo']},gsp={$valuation['tsp']},gcp={$valuation['tcp']},lastUpdate=unix_timestamp(),uid=1");
	echo $sales->rowCount()>0?1:0;
	$pdo->commit();
}
catch (PDOException $ex){
	echo $ex->getMessage();
	$pdo->rollBack();
}

print("<pre>");
print_r($valuation);
print("</pre>");
?>