var ImageUpload={formObj:null,path:null,
        		process:function(formObj,url,imgObj,dest){
        			this.formObj=formObj;
        			this.imgObj=imgObj;
        			this.dest=dest;
        		    this.response=$(this.formObj.parentNode).find("small")[0];
        			if(isDefined(this.response)) {
        				$(this.response).text("Uploading...");
        				$(this.response).show();
        			}
        			this.formObj.action=url;
        			this.formObj.target=$(this.formObj.parentNode).find("iframe")[0].name;
        		},
        		track:function(key){
        			this.formObj.reset();
        			this.formObj.action="javascript:void(0)";
        			switch(key){
        			case 1:
        				if(isDefined(this.response)) {
        					$(this.response).text("Image too large");
        					setTimeout(function(o){o.hide();},5000,this.response);
        				}
        				break;
        			case 2:
        				if(isDefined(this.response)) {
        					$(this.response).text("Incorrect file format");
        					setTimeout(function(o){o.hide();},5000,this.response);
        				}
        				break;
        			default:
        				if(isDefined(this.response)) $(this.response).hide();
        				if(typeof this.imgObj=="function"){
        					this.imgObj(key,this.dest);
        				}
        				else{
        					//this.imgObj.src=((isDefined(this.dest))?(this.dest+pathinfo(key)['filename']):(key));
        					this.imgObj.src=key;
        				}
        				break;
        			}
        		}
        };