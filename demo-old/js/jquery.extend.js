$.fn.extend({
  check: function() {
    return this.each(function() {
      this.checked = true;
    });
  },
  uncheck: function() {
    return this.each(function() {
      this.checked = false;
    });
  },getFormData:function(typ){
		 var arr=[],c=0,obj=$(this).find("input").filter(function(index){ return this.type==typ;});
		 for(var i=0;i<obj.length;i++) arr[c++]=obj[i].value;
		 return arr;
	 },disable:function(bg){
		  if(isDefined(getObj(this.id+'_disable'))) return false;
		  var obj=document.createElement("div");
			 obj.className="disable";
			 obj.id=this.id+'_disable';
			 obj.onclick=function(){$(obj).remove();};
			 this.append(obj);
			 if(isDefined(bg)) obj.style.backgroundColor=bg;
			 return true;
	 },enable:function(){
		 if(!isDefined(getObj(this.id+'_disable'))) return false;
		 $('#'+this.id+'_disable').remove();
	 },center:function(x,y){
	    	var top,left;
	    	$(this).css("position","fixed");
	    	if((isDefined(x) && !x.isEmpty()) && (isDefined(y) && !y.isEmpty())){
	    		$(this).css("top",y+"px");
	    		$(this).css("left",x+"px");
	        	return false;
	    	}
	    	
	    	top=Math.round((window.innerHeight-$(this).height())/2);
	    	left=Math.round((window.innerWidth-$(this).width())/2);
	    	$(this).css("top",(top<0)?(0):(top+"px"));
	    	$(this).css("left",(left<0)?(0):(left+"px"));
	    }
});