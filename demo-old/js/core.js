 Object.prototype.prependChild=function(obj){
    	  this.insertBefore(obj,this.firstChild);
      };
      
Object.prototype.getObjN=function(o){return this.getElementsByTagName(o);};
   
   function getObjN2(obj,o,func){
	   var arr=[],c=0,obj=this.getElementsByTagName(o);
	   for(var i=0;i<obj.length;i++){
			if ((isDefined(func) && func(obj[i])) || !isDefined(func)){
			arr[c++]=obj[i];
			}
		}
	   return arr;
	   }
   
   Object.prototype.getObj2=function(id){
	   return this.contentDocument.getElementById(id);
   };
   
   
   //format time
   Date.prototype.toTime=function(){
	   var i=this.getMinutes(),h,hour,min,GMT;
	   if(this.getHours()>12){
	   	h=(this.getHours()-12);
	   	GMT="PM";
	   }
	   else{
	   	h=this.getHours();
	   	GMT="AM";
	   	}
	   hour=(h<10)?("0"+h):(h);
	   min=(i<10)?("0"+i):(i);
	   return (hour+":"+min+" "+GMT).toString();
	   };
 // trim function
    String.prototype.trim=function(){return this.replace(/^[\s\xa0]+|[\s\xa0]+$/g, '');};
    String.prototype.stripHTML=function(){return this.replace(/(?:<[^>]+>)|(?:,)/g,' ');};///<(?:.|\s)*?>/g
    String.prototype.isEmpty=function(){
    	if(this.trim()=='' || this.trim()==null) {
    		return true;
    		}
    		return false;
    };
    
    String.prototype.htmlEscape=function(){
        return String(this).replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/(\n)/g,'<br/>');
    };
    
    
    String.prototype.htmlEncode=function(){
    	   var div=document.createElement('div');
    	   var text=document.createTextNode(this);
    	   div.appendChild(text);
    	   return div.innerHTML;
    	};
    
    Object.prototype.center=function(x,y){
    	var top,left;
    	this.style.position="fixed";
    	if((isDefined(x) && !x.isEmpty()) && (isDefined(y) && !y.isEmpty())){
    		this.style.top=(y+"px");
        	this.style.left=(x+"px");
        	return false;
    	}
    	
    	top=Math.round((window.innerHeight-this.offsetHeight)/2);
    	left=Math.round((window.innerWidth-this.offsetWidth)/2);
    	this.style.top=(top<0)?(0):(top+"px");
    	this.style.left=(left<0)?(0):(left+"px");
    };

   
    Object.prototype.getWidth=function(){
    	return this.offsetWidth;
    };
    
    Object.prototype.getHeight=function(){
    	return this.offsetHeight;
    };
    
    Object.prototype.setWidth=function(w){
    	return this.style.width=w+"px";
    };
    
    Object.prototype.setHeight=function(h){
    	return this.style.height=h+"px";
    };
    
    Object.prototype.hide=function(bool){
    	if(bool)
    		this.style.visibility='hidden';
    	else
    		this.style.display='none';
    };

    Object.prototype.show=function(bool){
    	if(bool)
    		this.style.visibility='visible';
    	else
    	 this.style.display='block';
    };
    
    Object.prototype.getAttrs=function(){
		var arr={};
		for(var i=0;i<this.attributes.length;i++){
			arr[this.attributes[i].name]=this.attributes[i].value;
			}
			return arr;
		};
	
	
	Object.prototype.setAttrs=function(obj){
		for(var x in obj){
				this.setAttribute(x,obj[x]);
				}
		};
	
// fetch table data
		Object.prototype.fetchTableData=function(){
			var bd=this.tBodies[0],arr=[];
			for(var i=0;i<bd.rows.length;i++){
				arr[i]=this.rows[i].outerHTML;
				}
			return arr;
			};
		
			Object.prototype.updateTableData=function(obj,arr){
				this.removeChild(this.firstChild);
				var tb=document.createElement("tbody");
				if(arr.length==0){
					tb.insertRow(0).outerHTML="<tr><td>No match found</td></tr>";
					this.appendChild(tb);
					return false;
				}
				for(var i=0;i<arr.length;i++){
					tb.insertRow(i).outerHTML=obj[arr[i]];
					}
				this.appendChild(tb);
				};
		
				Object.prototype.fetchDataByCol=function(c){
					var bd=this.tBodies[0],arr=[],j=0,chkbox=this.tBodies[0].getElementsByTagName('input');
					if(!this.tagName.toLowerCase()=='table'){
						return false;
					}
					
					for(var i=0;i<bd.rows.length;i++){
					if(chkbox[i].type=="checkbox" && chkbox[i].checked){
						arr[j++]=(isDefined(document.innerText))?(bd.rows[i].cells[c].innerText):(bd.rows[i].cells[c].textContent);
					}
					}
					return arr;
				};
				
				Object.prototype.fetchDataByCols=function(col){
					var bd=this.tBodies[0],arr=[];
					if(!this.tagName.toLowerCase()=='table'){
						return false;
					}
					
					for(var i=0;i<bd.rows.length;i++){
						var arr2=[];
						for(var k=0;k<col.length;k++){
							arr2[k]=(isDefined(document.innerText))?(bd.rows[i].cells[col[k]].innerText):(bd.rows[i].cells[col[k]].textContent);
						}
						arr[i]=arr2;
					  }
					return arr;
				};
				
				Object.prototype.fetchDataByRow=function(r){
					var bd=this.tBodies[0],arr=[],j=0;
					if(!this.tagName.toLowerCase()=='table'){
						return false;
					}
					
					for(var i=1;i<bd.rows[r].cells.length;i++){
					arr[j++]=(isDefined(document.innerText))?(bd.rows[r].cells[i].innerText):(bd.rows[r].cells[i].textContent);
					}
					return arr;
				};
				
				 Object.prototype.getText=function(){
			        	return (isDefined(document.innerText))?(this.innerText):(this.textContent);
			        };
			        
		        Object.prototype.setText=function(data){
		        	this.replaceChild(this.childNodes[0],createTextNode(data));
		        };
			       
		        Object.prototype.getHtml=function(){
		        	return this.innerHTML;
		        };
		        
		        Object.prototype.setHtml=function(data){
		        	this.innerHTML=data;
		        }; 
		        
				 Object.prototype.getRowByTabIndex=function(n){
					 var r=this.tBodies[0].rows;
					 for(var i=0;i<r.length;i++){
						 if(r[i].tabIndex==n){
							return r[i];
					 }
				 }
				 };
				
				 
				 Object.prototype.disable=function(bg){
					  if(isDefined(getObj(this.id+'_disable'))) return false;
					  var obj=document.createElement("div");
						 obj.className="disable";
						 obj.id=this.id+'_disable';
						 this.appendChild(obj);
						 if(isDefined(bg)) obj.style.backgroundColor=bg;
						 return true;
				 };
				 
				 
				 Object.prototype.loading=function(src){
						var img=new Image();
						img.style.position="absolute";
						img.src=src;
						this.appendChild(img);
						img.center();
					};
				 
				 Object.prototype.enable=function(){
					 if(!isDefined(getObj(this.id+'_disable'))) return false;
					 this.removeChild(getObj(this.id+'_disable'));
				 };
				 
				 Object.prototype.clear=function(){
					 this.setHtml('');
					 this.hide();
				 };
				 
				 Object.prototype.destroy=function(){
					 this.parentNode.removeChild(this);
				 };
				 
					Object.prototype.getPosition=function(){
						var posX=0,posY=0,obj=this;
						while (obj != null){
						posX += obj.offsetLeft;
						posY += obj.offsetTop;
						obj=obj.offsetParent;
						}
					return [posX, posY];
					};
					
					
					Object.prototype.hasClass=function(element, cls) {
					    return (' ' + this.className + ' ').indexOf(' ' + cls + ' ') > -1;
					};
					
					
					Object.prototype.addClass=function(classValue){
						var pattern = new RegExp("(^| )" + classValue + "( |$)");
						if (!pattern.test(this.className)){
						if (this.className == "")
							this.className=classValue;
						else
							this.className += " " +classValue;
						}
						return true;
						};


					Object.prototype.removeClass=function(classValue){
						var removedClass=this.className;
						var pattern=new RegExp("(^| )" + classValue+"( |$)");
						removedClass=removedClass.replace(pattern, "$1");
						removedClass=removedClass.replace(/ $/, "");
						this.className=removedClass;
						return true;
						};
						
						
						 Object.prototype.isFilled=function(){
							 this.style.backgroundColor='transparent'; 
						 };
						
						 Object.prototype.isNotFilled=function(){
							 this.style.backgroundColor='#F4F3BE';
							 return false;
						 };
						 
						 Object.prototype.any=function(){
								var obj=this.getElementsByTagName("input"),bool=false;
								for(var i=0;i<obj.length;i++){
									if (obj[i].type=="checkbox"  && !obj[i].value.isEmpty() && obj[i].checked){
										bool=true;
									}
								}
								return bool;
							};
				           
				           Object.prototype.getCheckBox=function(func){
								 var obj=this.getObjN('input'),arr=[],c=0;
								 for(var i=0;i<obj.length;i++){
										if (obj[i].type=="checkbox"  && !obj[i].value.isEmpty() && obj[i].checked){
											arr[c++]=(typeof func=="function")?(func(obj[i].value)):(obj[i].value);
										}
									}
								 return arr;
							 };
							 
							 Object.prototype.getFormData=function(typ,func){
								 //takes 2 arguments, the type and function to check the operation of the object
								 var arr=[],c=0;//obj=(this.nodeName=='FORM')?(this.elements):(this.tBodies[0].getElementsByTagName('input'))
								 if(this.nodeName=='FORM')
									 obj=this.elements;
								 else if(this.nodeName=='TABLE')
									 obj=this.tBodies[0].getObjN('input');
								 else
									 obj=this.getObjN('input');
										 
								 for(var i=0;i<obj.length;i++){
									 if(typ=="select"){
										 if((obj[i].nodeName=="SELECT" && (typeof func=="function" && func(obj[i]))) || (obj[i].nodeName=="SELECT" && typeof func=="undefined")){
												arr[c++]=obj[i].value;
											}
									 }
									 else{
									if((obj[i].type==typ && (typeof func=="function" && func(obj[i]))) || (obj[i].type==typ && typeof func=="undefined")){
										arr[c++]=obj[i].value;
									}
								 }
								 }
								 return arr;
							 };
					
							