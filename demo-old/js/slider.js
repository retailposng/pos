function slider(evt,o,func,rangeCoor){
	var obj=o,rc=rangeCoor || [0,0,0,0];
	e=evtype(evt);
	mouseover=true;
	pleft=parseInt(obj.style.left);
	  xcoor=e.clientX;
	 document.onmousemove=function(e){
		 var x=e.clientX;
		 if((pleft+x-xcoor) >= rc[0] && ((pleft+x-xcoor+obj.offsetWidth+rc[1]) <= obj.parentNode.offsetWidth)){
			  obj.style.left=pleft+x-xcoor+"px";
			  calc(pleft+x-xcoor,obj,func);
			  }
		 
	     return false;
	 	};
	 return false;
	}
	document.onmouseup=function(e){document.onmousemove=null;};
	
function skipSash(e,func){
	var srcObj=evtObj(e),obj=$(srcObj.parentNode).find('div')[1],pos=parseInt(cursorPosition(e)[0])-parseInt(getPosition(srcObj)[0]);
	pos=((pos-(obj.offsetWidth/2))>(obj.parentNode.offsetWidth-obj.offsetWidth))?((obj.parentNode.offsetWidth-obj.offsetWidth)):(((pos-(obj.offsetWidth/2))<0)?(0):(pos-(obj.offsetWidth/2)));
	 obj.style.left=pos+'px';
	calc(pos,obj,func);
	}

function setSliderValue(srcObj,v,func){//setSliderValue(getObj('min_discount'),99,_cent);
	var obj=srcObj.parentNode.getObjN('div')[1],pos=(v*(obj.parentNode.offsetWidth-obj.offsetWidth))/100;
	obj.style.left=pos+'px';
	calc(pos,obj,func);
}


function calc(n,obj,func){
	var maxlen=(obj.parentNode.offsetWidth-obj.offsetWidth),value=Math.round(((n*100)/maxlen));
	$('#discount_value').text(value+'%');
	Cart.Discount.setGlobal(value);//global discount
	//$(obj.parentNode).find('input')[0].value=value;
	if(isDefined(func)) func(value,obj);
}

