<?php
ini_set("display_errors",0);

date_default_timezone_set("Africa/Lagos");

//print_r(json_decode($_COOKIE["cred"],1));

if(@$isCached===false) goto NOTCACHE;
//header("Cache-Control: max-age=3600, must-revalidate"); 
header("Accept-Encoding: gzip,deflate");
header("Last-Modified:".date("F d Y H:i:s.",getlastmod()));
substr_count($_SERVER['HTTP_ACCEPT_ENCODING'],'gzip')?ob_start('ob_gzhandler'):ob_start();
NOTCACHE:

include 'config.php';

class Layout{
	var $path="",$host,$title="",$description="";
	public function __construct($fn=NULL){
		global $pdo;
		$this->fn=$fn;
		$this->host=$_SERVER["HTTP_HOST"];
		$this->pdo=$pdo;
		$this->data=$this->controllers();
	}
	
	public function controllers(){
		$usrID=intval($_SESSION['usrID']);
		$patchQuery=(!in_array(@$_SESSION["roleID"],array(1,2)))?(" and usrID=$usrID"):(NULL);
		$valuation=$this->pdo->query("select sum(tcp) as tcp,sum(tsp) as tsp,(select count(*) from outofstock) as os,(select count(*) from reorderlevel) as rol,(select sum(qty) from backorder) as bo,(select concat(storeID,'@',syncKey) from profile limit 1) as storeCred,(select concat(sum(if(s.price='retail',x.rsprice,x.wsprice)*s.qty),':',sum(s.qty)) from (sale s join stock x on x.stockID) where s.status='sold' and x.stockID=s.stockID and from_unixtime(s.date,'%Y%m%d')=date_format(now(),'%Y%m%d') $patchQuery) as prosold from valuation");
		$profile=$this->pdo->query("select * from profile");
		return array("valuation"=>$valuation->fetch(PDO::FETCH_ASSOC),"profile"=>$profile->fetch(PDO::FETCH_ASSOC));
	}
	
	public function content($toolbar=NULL,$body){
		$this->body=$body;
		$this->toolbar=$toolbar;
		?>
<!DOCTYPE html>
<html>
<head>
   <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta name="author" content="Aliu Adepoju">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?=$this->title;?></title>
     <?php 
    include_once 'meta.php';
     ?>
    <link rel="shortcut icon" href="<?=$this->path;?>img/favicon.png" />
    <link href="<?=$this->path;?>css/styles.css" rel="stylesheet"/>
    <link href="<?=$this->path;?>css/jquery.dataTables.css" rel="stylesheet"/>
    <link href="<?=$this->path;?>css/dialog.css" rel="stylesheet"/>
    <link href="<?=$this->path;?>css/adjust.css" rel="stylesheet"/>
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-57857218-1', 'auto');
  ga('send', 'pageview');

</script>
  </head>
<?=flush();?>
  <body>
 
  <div class="row">
              <div class="col-md-12 no-padding"> 
              <section>
             <?php 
             if($this->fn=="barcode"){
             	?>
             	<div class="row padding5 bg-color-primary"> 
              <div class="col-xs-12">
              <a href="dashboard.php" class="fg-color-white logo"><img src="img/logo.png"/>&nbsp;<strong>Retail</strong>POS</a>
              </div>
              </div>
              
             	<?php
             }else{
             	?>
             	 <div class="row padding5 bg-color-primary"> 
              <div class="col-sm-3">
              <a href="dashboard.php" class="fg-color-white logo"><img src="img/logo.png"/>&nbsp;<strong>Retail</strong>POS</a>
              </div>
              <div class="col-sm-1">
               <span class="hide2" id="noti_box"></span>
              </div>
              <div class="col-sm-8 no-padding text-right">
             <?php 
             if(!in_array($this->fn,array("pos","rstore"))){
             ?>
              <a href="pos.php" class="btn btn-warning">Point Of Sale</a>
              <?php 
              }
              ?>
             <span class="fg-color-white"><?=date("D, j M Y")?></span>
              <span class="fg-color-white" id="_clock"></span>&nbsp;
              <div class="btn-group pull-right">
    <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
      <i class="glyphicon glyphicon-user"></i>&nbsp;<?=$_SESSION["name"];?>
      <span class="caret"></span>
    </button>
    <ul class="dropdown-menu">
      <li><a href="password.php">Change password</a></li>
      <li><a href="logout.php">Log-out</a></li>
    </ul>
  </div>
              </div>
              
            		
              
              <!--
              <div class="col-md-12  ae-tab no-padding">
   <ul class="no-margin no-padding">
   <?php
   $tabs=array("pos"=>"In stock","sale"=>"Sales","store"=>"Store","customer"=>"Customers","supplier"=>"Suppliers","seller"=>"Sellers","statistics"=>"Statistics","supply"=>"Supply");
   //if($this->fn=="add") $tabs["add"]="Add stock";
   foreach ($tabs as $url=>$label){
   	$active=($this->fn==$url)?("class='active'"):(NULL);	
	echo "<li {$active}><a href='{$url}.php'>{$label}</a></li>";
   }
   ?>
  <li class="pull-right"><img alt="loading" src="iconx/loading.gif">&nbsp;</li>
</ul>
 </div>
   -->
 
 </div>
             	<?php 
             }
           
             ?>
 <div class="row">
 <?php if(!is_null($this->toolbar)) {
            			echo "<div class='col-md-12 padding5 ae-well toolbar'>";
            			include_once $this->toolbar;
            			echo "</div>";
            		}
            		?>
 </div>
 <?php include_once $this->body;?>
 </section> 
 </div>
 </div>
  
	  <script src="<?=$this->path;?>js/jquery-2.1.1.min.js" type="text/javascript"></script>
      <script src="<?=$this->path;?>js/bootstrap.min.js" type="text/javascript"></script>
      <script src="<?=$this->path;?>js/jquery.dataTables.js" type="text/javascript"></script>
      <script src="<?=$this->path;?>js/bootstrap-datepicker.js" type="text/javascript"></script>
      <script src="<?=$this->path;?>js/jquery.extend.js" type="text/javascript"></script>
      <script src="<?=$this->path;?>js/date.format.js" type="text/javascript"></script>
       <script src="<?=$this->path;?>js/Chart.js" type="text/javascript"></script>
      <script src="<?=$this->path;?>js/ds_a.js" type="text/javascript"></script>
     <script src="<?=$this->path;?>js/pi.js" type="text/javascript"></script>
      <script src="<?=$this->path;?>js/imageUpload.js" type="text/javascript"></script>
      <script src="<?=$this->path;?>js/validator.js" type="text/javascript"></script>
     <script src="<?=$this->path;?>js/pab.js" type="text/javascript"></script>
     <script src="<?=$this->path;?>js/calendar.js" type="text/javascript"></script>
     <script src="<?=$this->path;?>js/slider.js" type="text/javascript"></script>
     <script src="<?=$this->path;?>js/calc.js" type="text/javascript"></script>
     <script src="<?=$this->path;?>js/base64.js" type="text/javascript"></script>
      <script src="<?=$this->path;?>js/php.js" type="text/javascript"></script>
      
     <?php 
     include_once "{$this->path}js.php";
     ?>
     <script type="text/javascript">
     $(document).ready(function() {
    	 $(function(){
    		    
    			// Checking for CSS 3D transformation support
    			$.support.css3d = supportsCSS3D();
    			
    			var formContainer = $('#formContainer');
    			
    			// Listening for clicks on the ribbon links
    			$('.flipLink').click(function(e){
    				
    				// Flipping the forms
    				formContainer.toggleClass('flipped');
    				
    				// If there is no CSS3 3D support, simply
    				// hide the login form (exposing the recover one)
    				if(!$.support.css3d){
    					$('#login').toggle();
    				}
    				e.preventDefault();
    			});
    			
    			formContainer.find('form').submit(function(e){
    				// Preventing form submissions. If you implement
    				// a backend, you might want to remove this code
    				e.preventDefault();
    			});
    			
    			
    			// A helper function that checks for the 
    			// support of the 3D CSS3 transformations.
    			function supportsCSS3D() {
    				var props = [
    					'perspectiveProperty', 'WebkitPerspective', 'MozPerspective'
    				], testDom = document.createElement('a');
    				  
    				for(var i=0; i<props.length; i++){
    					if(props[i] in testDom.style){
    						return true;
    					}
    				}
    				
    				return false;
    			}
    		});
     
         
    	 thread=ajaxThread();
    	 var dtable=$('#tb_data,.tb_data').dataTable();

   	  /* sales page */
   	  $('#sales_data_table').dataTable( {
        "footerCallback": function ( row, data, start, end, display ) {
            var api = this.api(), data;
 
            // Remove the formatting to get integer data for summation
            var intVal = function ( i ) {
                return typeof i === 'string' ?
                    i.replace(/[\$,]/g, '')*1 :
                    typeof i === 'number' ?
                        i : 0;
            };
 
            // Total over all pages
            data = api.column( 6 ).data();
            total = data.length ?
                data.reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                } ) :
                0;
 
            // Total over this page
            data = api.column( 6, { page: 'current'} ).data();
            pageTotal = data.length ?
                data.reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                } ) :
                0;
 
            // Update footer
            
            $( api.column( 6 ).footer() ).html(
                '$'+pageTotal +' ( $'+ total +' total)'
            );
        }
    } );
   		 
    	 $('#dp3').datepicker().on('changeDate', function(ev){
			thread(["store/content.php",$.param({date:new Date(ev.date).format('Y-m-d')})],function(xhr){},function(xhr){
				$("#_content").html(JSON.parse(xhr.responseText)["contents"]);
				},function(xhr){});
     		
        	 });
    	
    	 clock(getObj('_clock'));
    	 $('#suspend_count').text(Cart.Suspend.count());
    	 Stock.Multiple.add();
		
    	} );
 	
     </script>
    
  </body>
</html>
<?php
}
}
?>