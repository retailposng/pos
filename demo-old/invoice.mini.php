<?php 
/* print("<pre>");
print_r($_POST);
print("</pre>"); */
$hasDiscount=(array_sum(array_map(function($v){ return str_replace("#","",$v);},$discount["each"])) > 0 || $discount["global"] > 0);
$hasSerialNo=count($data["serialNo"]) > 0;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title><?=$config["name"];?> - Receipt</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <style>
    *{
	    padding: 1px !important;
	    margin: 0px !important;
		font-size: 10px !important;
    }
      body, h1, h2, h3, h4, h5, h6{
      font-family: tahoma, sans-serif;
      }
	 
	  .storename{
		  font-weight: bold;
	  }
	
    </style>
  </head>
  <?php 
  flush();
  ?>
  <body onload="window.print();window.parent.Cart.Payment.success('<?=$invoice;?>',<?=$reload;?>);">
    <div class="container">
      <div class="row">
        <div class="col-xs-12">
          <h1><small>Receipt #: <?=$invoice;?>
          </small>
           <small class="pull-right">Date: <?=date("d/m/Y");?></small>
          </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <div class="panel panel-default pad">
            <div class="panel-heading">
              <h4 class="storename"><?=$config["name"];?></h4>
              <strong>Seller:</strong><span class="addr"> <?=$_SESSION["name"];?></span> <br>
            </div>
            <div class="panel-body">
              <p>
                <strong>Address:</strong><span class="addr"> <?=$config["addr"];?></span> <br>
                <strong>Phone number: </strong><span class="addr"> <?=$config["phone"];?></span>
              </p>
            </div>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="panel panel-default">
            <div class="panel-body">
            <strong>Customer:</strong>  <span class="addr"><?=$customer_data["name"];?></span><br>
            <!-- <strong>Address:</strong> <span class="addr"> <?=$customer_data["addr"];?></span> -->
            
            </div>
          </div>
        </div>
      </div>
      <!-- 
       <strong>Email:</strong> <span class="addr"><?=$config["email"];?></span> <br/>
       end client details section -->
      <table class="table table-bordered table-responsive">
        <thead>
	     <tr><th>#</th><th>Items</th><?=($hasSerialNo)?("<th>Serial #</th>"):(NULL);?><th>Price</th><?=($hasDiscount)?("<th>Discount</th>"):(NULL);?><th>Total</th></tr>
   	 </thead>
        <tbody>
          <?php 
          $i=1;
          $linetotal=array();
          $subtotal=array();
          foreach ($data["items"] as $stockID=>$arr){
			$dis=number_format((intval(str_replace("#","",$discount["each"][$stockID]))+(($arr[2]*$discount["global"])/100)),2);
			if(array_key_exists($stockID,$data["serialNo"])){
				for($j=0;$j<$arr[3];$j++){
				$subtotal[$i]=$arr[2];
				
				$linetotal[$i]=$subtotal[$i]-($discount["each"][$stockID]+(($arr[2]*$discount["global"])/100));
					?>
		          	<tr><td><?=$i;?></td><?=($j==0)?("<td rowspan='{$arr[3]}'>".(strlen($arr[1]) > 35)?(substr($arr[1],0,35)."..."):($arr[1])."</td>"):(NULL);?><td><?=@$data["serialNo"][$stockID][$j];?></td><td><small>x<?=$arr[3];?> </small><?=$arr[2];?></td><?=($hasDiscount)?("<td>{$dis}</td>"):(NULL);?><td><?=number_format($linetotal[$i],2);?></td></tr>
		          	<?php
		          	$i++;
				}
			}
			else{
				
				$subtotal[$i]=$arr[2]*$arr[3];
				$linetotal[$i]=($subtotal[$i]-((((strpos($discount["each"][$stockID],"#")===0)?(intval(substr($discount["each"][$stockID],1))):($discount["each"][$stockID]*$arr[3]))+((($arr[2]*$discount["global"])/100)*$arr[3]))));
          	?>
          	<tr><td><?=$i;?></td><td><?=(strlen($arr[1]) > 35)?(substr($arr[1],0,35)."..."):($arr[1]);?></td><?=($hasSerialNo)?("<td></td>"):(NULL);?><td><small>x<?=$arr[3];?></small> <?=$arr[2];?></td><?=($hasDiscount)?("<td>{$dis}</td>"):(NULL);?><td><?=number_format($linetotal[$i],2);?></td></tr>
          	<?php
          	$i++;
          	}
          	}
          ?>
          <tr><td colspan="<?=(3+intval($hasDiscount)+intval($hasSerialNo));?>"></td><td>₦<?=number_format(array_sum($linetotal),2);?></td></tr>
        </tbody>
      </table>
      <div class="row">
      <div class="col-xs-12">
      <table class="table" style="cell-padding:0px;cell-spacing:0px">
      <tbody>
      <tr><td>Paid:</td><td>₦<?=number_format($data["paid"],2);?></td></tr>
        <tr><td>Sub-total:</td><td><?=number_format(array_sum($subtotal),2);?></td></tr>
         <?=($hasDiscount)?("<tr><td>Discounts</td><td>₦".number_format(array_sum($subtotal)-array_sum($linetotal),2)."</td></tr>"):(NULL);?>
          <tr><td>VAT:</td><td><?=number_format($data["vat"],2);?></td></tr>
          <tr><td>Total:</td><td>₦<?=number_format(array_sum($linetotal)+$data["vat"],2);?></td></tr>
           <tr><td>Change:</td><td>₦<?=number_format($data["paid"]-(array_sum($linetotal)+$data["vat"]),2);?></td></tr>
      </tbody>
      </table>
      </div>
      </div>
      <div class="row">
      <div class="col-xs-12">
      <h3 class="lead">
      <?php 
      if (isset($_POST["onShowNote"])) {
      	echo $_POST["note"];
      }
      ?>
      </h3>
      </div>
      
      </div>
    </div>
  </body>
</html>