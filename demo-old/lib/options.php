<?php
function options($tb,$flag=NULL,$default=0){
	global $pdo;
	$str='';
	$fetch=$pdo->query("select {$tb}ID,name from {$tb} {$flag} order by {$tb}.name asc");
	while($res=$fetch->fetch(PDO::FETCH_NUM)){
		$selected=($res[0]==$default || strtolower($res[1])==strtolower($default))?("selected=true"):(NULL);
		$str .="<option value='{$res[0]}' {$selected}>{$res[1]}</option>";
	}
	return $str;
}
?>
