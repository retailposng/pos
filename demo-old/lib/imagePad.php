<?php
function imagePad($filename,$w,$h,$save_as){
	include_once 'smartResizeImage.php';
		$img=smart_resize_image($filename,$w,$h,true,'return');
		list($width,$height,$imageType)=getimagesize($img);
		$dst=imagecreatetruecolor($w,$h);
		imagefilledrectangle($dst,0,0,$w,$h,imagecolortransparent($dst,imagecolorallocate($dst,255,255,255)));
		$img = imagecreatefrompng($img);
		imagecopy($dst,$img,($w-$width)/2,($h-$height)/2,0,0,$width,$height);
		imagepng($dst,$save_as);
		chmod($save_as,0777);
} 

