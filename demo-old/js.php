<script type="text/javascript">
	 addEvent(window,'load',function(){
		 loading=new Image();
  	     loading.src="images/loading2.gif";
		 var date=new Date();
		// Synchronize.start(date.format('Ymd'),'<?=json_encode($this->data);?>');
		
	 		});
	</script>
<?php
if($this->fn=="pos"){
	?>
	<script src="<?=$this->path;?>js/barcode.js" type="text/javascript"></script>
	<script type="text/javascript">
	function adjust(){
		if(window.innerWidth > 980){
			$("#item_box").height(window.innerHeight-107);
			$("#cartScroll").height(window.innerHeight-336);
			$('body').addClass("hide-scroll");
		}
		}

	 addEvent(window,'load',function(){
		 all_stocks=<?=$all_data;?>;
		 //document.write(all_stocks.slice(0,100));
		 loading=new Image();
  	     loading.src="images/loading2.gif";
		 adjust();
		 setInterval(function(){Barcode.pos();},500);
	});
	
	 addEvent(window,'resize',function(){
		 adjust();
	});
		</script>
<?php 
}
if($this->fn=="dashboard"){
	?>
	<script type="text/javascript">
	var doughnutData = [
	    				{
		    					value: <?=$grossMarginProfit;?>,
		    					color: "#46BFBD",
		    					highlight: "#5AD3D1",
		    					label: "Gross Profit Margin (%)"
		    				},
		    				{
		    					value: 100-<?=$grossMarginProfit;?>,
		    					color: "#FDB45C",
		    					highlight: "#FFC870",
		    					label: "Gross Sale (%)"
		    				}

	    			];

	    			window.onload = function(){
	    				var ctx = $("#profit_chart").find("canvas").attr({"height":155}).get(0).getContext("2d");
	    				window.myDoughnut = new Chart(ctx).Doughnut(doughnutData, {responsive : true});
	    			};
	
	</script>
	<?php 
}

if($this->fn=="rstore"){
	?>
	<script type="text/javascript">
	 function getSalesData(date,salesType){
    	 thread(["store/content.php",$.param({'date':(new Date(date)).format('Y-m-d'),'salesType':salesType})],function(xhr){},function(xhr){
    		 //alert(xhr.responseText);
				//delete xSearchList.cache[534536];
				$("#_content").html(JSON.parse(xhr.responseText)["contents"]);
				 $('#tb_data,.tb_data,#sales_data_table').dataTable();
				 data=JSON.parse(JSON.parse(xhr.responseText)["data"]);
				 $("#_income").text("Net Income: ₦"+money(JSON.parse(xhr.responseText)["income"]));
				 $("#_revenue").text("Revenue: ₦"+money(JSON.parse(xhr.responseText)["revenue"]));
				 chart([data["label"],data["sales"],data["profits"]],JSON.parse(xhr.responseText)["profitMargin"]);
				},function(xhr){});
		}

		function openCal(){
		 $('#dp_sale').datepicker().on('changeDate', function(ev){
	     	getSalesData(ev.date,$("#sales_type").val());
	     	 });
		}
 	 
	function chart(cData,profitMargin){
	//	alert(cData[0]);
		var data = {
			    labels:cData[0],
			    datasets: [
			        {
			        	fillColor : "rgba(220,220,220,0.5)",
						strokeColor : "rgba(220,220,220,0.8)",
						highlightFill: "rgba(220,220,220,0.75)",
						highlightStroke: "rgba(220,220,220,1)",
			            data:cData[1]
			        },
			        {

						fillColor : "rgba(151,187,205,0.5)",
						strokeColor : "rgba(151,187,205,0.8)",
						highlightFill : "rgba(151,187,205,0.75)",
						highlightStroke : "rgba(151,187,205,1)",
			            data: cData[2]
			        } 
			    ]
			};

		var doughnutData = [
		    				
		    				{
		    					value: profitMargin,
		    					color: "#46BFBD",
		    					highlight: "#5AD3D1",
		    					label: "Net Profit Margin (%)"
		    				},
		    				{
		    					value: 100-profitMargin,
		    					color: "#FDB45C",
		    					highlight: "#FFC870",
		    					label: "Net Sales (%)"
		    				}

		    			];

		
		$("#sales_chart_1").children().remove();
		$("#sales_chart_1").get(0).appendChild(document.createElement('canvas'));
		
		var ctx = $("#sales_chart_1").find("canvas").attr({"height":250,"width":600}).get(0).getContext("2d");
	
	 	new Chart(ctx).Line(data, {responsive : true});
		
		$("#profit_chart_1").children().remove();
		$("#profit_chart_1").get(0).appendChild(document.createElement('canvas'));
		var ctx = $("#profit_chart_1").find("canvas").attr({"height":180}).get(0).getContext("2d");
			new Chart(ctx).Doughnut(doughnutData, {responsive : true});
		}


	var Sales={
			download:function(date,salesType){
				$("#dl_data").attr("src","store/download.php?date="+date+"&salesType="+salesType);
			},print:function(date,salesType){
				$("#dl_data").attr("src","store/print.php?date="+date+"&salesType="+salesType);
			}
	};	
	
	window.onload = function(){
		 chart([<?=@json_encode($_data["label"]);?>,<?=@json_encode($_data["sales"]);?>,<?=@json_encode($_data["profits"]);?>],<?=@$profitMargin;?>);
		};
	</script>
	<?php
}

if($this->fn=="report"){
	?>
	<script type="text/javascript">
	function analyzeProduct(sku){
			thread(["views/report/__analyze.php",$.param({"sku":sku})],function(xhr){
				},function(xhr){
					$("#_analysis").html(xhr.responseText);
				},function(xhr){});
		}
	
	function chart(cData,profitMargin){
		
		var data = {
			    labels:cData[0],
			    datasets: [
			        {
			        	fillColor :"rgba(220,220,220,0.5)",
						strokeColor : "rgba(220,220,220,0.8)",
						highlightFill: "rgba(220,220,220,0.75)",
						highlightStroke: "rgba(220,220,220,1)",
			            data:cData[1]
			        },
			        {

						fillColor : "rgba(151,187,205,0.5)",
						strokeColor : "rgba(151,187,205,0.8)",
						highlightFill : "rgba(151,187,205,0.75)",
						highlightStroke : "rgba(151,187,205,1)",
			            data: cData[2]
			        } 
			    ]
			};

		var doughnutData = [
		    				
		    				{
		    					value: profitMargin,
		    					color: "#46BFBD",
		    					highlight: "#5AD3D1",
		    					label: "Net Profit Margin (%)"
		    				},
		    				{
		    					value: 100-profitMargin,
		    					color: "#FDB45C",
		    					highlight: "#FFC870",
		    					label: "Net Sales (%)"
		    				}

		    			];

		
		$("#sales_chart").children().remove();
		$("#sales_chart").get(0).appendChild(document.createElement('canvas'));
		var ctx = $("#sales_chart").find("canvas").attr({"height":200,"width":600}).get(0).getContext("2d");
		 	new Chart(ctx).Line(data, {responsive : true});

		$("#profit_chart").children().remove();
		$("#profit_chart").get(0).appendChild(document.createElement('canvas'));
		var ctx = $("#profit_chart").find("canvas").attr({"height":250}).get(0).getContext("2d");
			new Chart(ctx).Doughnut(doughnutData, {responsive : true});
		}
	    	
	
	window.onload = function(){
		 chart([<?=@json_encode($_data["label"]);?>,<?=@json_encode($_data["sales"]);?>,<?=@json_encode($_data["profits"]);?>],<?=@$profitMargin;?>);
	};


	var nowTemp = new Date();
	var now = new Date(nowTemp.getFullYear(), nowTemp.getMonth(), nowTemp.getDate(), 0, 0, 0, 0);
	 
	var checkin = $('#dpd1').datepicker().on('changeDate', function(ev) {
	  if (ev.date.valueOf() > checkout.date.valueOf()) {
	    var newDate = new Date(ev.date);
	    newDate.setDate(newDate.getDate() + 1);
	    checkout.setValue(newDate);
	    thread(["views/report/_data.php",$.param({'from':(new Date(checkin.date)).format('Y-m-d'),'to':(new Date(checkout.date)).format('Y-m-d')})],function(xhr){},function(xhr){
			 data=JSON.parse(JSON.parse(xhr.responseText)["_data"]);
			 setAcqui(JSON.parse(xhr.responseText)["acquisitions"],JSON.parse(xhr.responseText)["revenue"],JSON.parse(xhr.responseText)["income"]);
			  chart([data["label"],data["sales"],data["profits"]],JSON.parse(xhr.responseText)["profitMargin"]);
			 },function(xhr){});
	  }
	  checkin.hide();
	  $('#dpd2')[0].focus();
	}).data('datepicker');
	
	var checkout=$('#dpd2').datepicker().on('changeDate', function(ev) {
		 thread(["views/report/_data.php",$.param({'from':(new Date(checkin.date)).format('Y-m-d'),'to':(new Date(checkout.date)).format('Y-m-d')})],function(xhr){},function(xhr){
				 data=JSON.parse(JSON.parse(xhr.responseText)["_data"]);
				 setAcqui(JSON.parse(xhr.responseText)["acquisitions"],JSON.parse(xhr.responseText)["revenue"],JSON.parse(xhr.responseText)["income"]);
				  chart([data["label"],data["sales"],data["profits"]],JSON.parse(xhr.responseText)["profitMargin"]);
				 },function(xhr){});
		 
	  checkout.hide();
	}).data('datepicker');

	function setAcqui(data,revenue,income){
		var arr=[];
		for(k in data){
			if(data.hasOwnProperty(k)){
				arr.push(template("<a href='%uri%' class='list-group-item'><span class='badge'>%fig%</span><p class='list-group-item-text'>%label%</p></a>",{"uri":k,"fig":data[k][1],"label":data[k][0]}));
		}
		}
		$("#_income").text("Net Income: ₦"+money(income));
		$("#_revenue").text("Revenue: ₦"+money(revenue));
		$("#acqui_list").html(arr.join(""));
	}
	</script>
	<?php
}
if($this->fn=="sale"){
	?>
	<script type="text/javascript">
	 function getSalesData(date,sellerID,salesType,payment){
    	 thread(["views/sale/content.php",$.param({'date':(new Date(date)).format('Y-m-d'),'seller':sellerID,'salesType':salesType,'payment':payment,'storeCred':'<?=json_encode($this->data);?>'})],function(xhr){},function(xhr){
				delete xSearchList.cache[534536];
				$("#_content").html(JSON.parse(xhr.responseText)["contents"]);
				 $('#tb_data,.tb_data,#sales_data_table').dataTable();
				 data=JSON.parse(JSON.parse(xhr.responseText)["data"]);
				},function(xhr){
					});
		}
		
	 $('#dp_sale').datepicker().on('changeDate', function(ev){
     	getSalesData(ev.date,null,$("#sales_type").val(),$("#payment").val());
     	 });

	 var Sales={
				download:function(date,salesType,seller){
					$("#dl_data").attr("src","views/sale/download.php?date="+date+"&salesType="+salesType+"&seller="+seller);
				},print:function(date,salesType,seller){
					$("#dl_data").attr("src","views/sale/print.php?date="+date+"&salesType="+salesType+"&seller="+seller);
				},invoice:function(invoiceID){
					$("#reprintFrame").attr("src","models/__reprint.php?invoice="+invoiceID);
				}
		};
	</script>
	<?php 
	}
	

	if($this->fn=="stock"){
		?>
		<script src="<?=$this->path;?>js/barcode.js" type="text/javascript"></script>
		<script type="text/javascript">
		 addEvent(window,'load',function(){
			 loading=new Image();
	  	     loading.src="images/loading2.gif";
			 setInterval(function(){Barcode.addStock();},500);
		 		});
		</script>
		<?php 
		}
		?>