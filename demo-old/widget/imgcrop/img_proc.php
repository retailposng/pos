<?php 
session_start();
ini_set("display_errors",1);
/* print("<pre>");
print_r($_POST);
print_r($_FILES);
print("</pre>"); */
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8" />
<title></title>
<style>
	*{
	padding:0px;
	margin:0px;
	}
	body{
	text-align:left;
	overflow-y:hidden;
	}
	
.draggable{
	position:fixed;
	left:0px;
	top:0px;
	width:450px;
	height:500px;
	/*border:1px dashed #606060;*/
	background:#fff;
	opacity: 0.5;
	filter: alpha(opacity=50);
	cursor:move;
}
.tr{
	position: absolute;
	top:-5px;
	right:-5px;
	}
	
	.tl{
	position: absolute;
	top:-5px;
	left:-5px;
	}
	
	.bl{
	position: absolute;
	bottom:-5px;
	left:-5px;
	}
	
	.br{
	position: absolute;
	bottom:-5px;
	right:-5px;
	}
	
	.box{
			padding: 5px;
			background:#222;
			}
</style>
<script type="text/javascript" src="js/ds_a.js"></script>
<script type="text/javascript" src="js/core.js"></script>
<script type="text/javascript" src="js/pi.js"></script>
<script type="text/javascript" src="js/imgResizer2.js"></script>
<script type="text/javascript">
	addEvent(window,'load',function(){
					if (window.document.readyState=="complete"){
						getObj('draggable').center();
						window.parent.getObj('default_w').value=window.parent.getObj('dg_w').value=getObj('draggable').offsetWidth;
						window.parent.getObj('default_h').value=window.parent.getObj('dg_h').value=getObj('draggable').offsetHeight;
						getCoord();
						}
					});

</script>
</head>
<body>
<?php
function __autoload($class_name){include_once "lib/classes/{$class_name}.php";} 
$filename=@$_GET['img'];
$path="upload";
if (!empty($filename) && file_exists("{$path}/{$filename}")){
	unlink("{$path}/{$filename}");
	unlink("{$path}/thumbnail/{$filename}");
}
if(!empty($_GET['coor'])){
	function clean($v){ return -intval($v);}//clear coordinate
	$coor=array_map("clean",explode(",",$_GET['coor']));
	$path2arr=pathinfo($_GET['img']);
	$imgCrop=new ImageCrop("{$path}/{$path2arr['basename']}","{$path}/thumbnail/{$path2arr['basename']}");
	$path2arr=pathinfo($imgCrop->coor($coor));
	$filename=$path2arr['basename'];
}


try{
if((empty($_POST) && empty($_FILES)) && !isset($_POST) && !isset($_GET['coor'])) Throw new PDOException("Error: Image size could be too large, <a href='javascript:void(0)' onclick=\"window.parent.getObj('uploadImgDiv').getObjN('img')[0].click()\">re-upload</a>");
if(isset($_FILES)){
if(!empty($_FILES['file']['name'])){
	if($_FILES['file']['error'] > 0) Throw new PDOException("Error: Image size could be too large, <a href='javascript:void(0)' onclick=\"window.parent.getObj('uploadImgDiv').getObjN('img')[0].click()\">re-upload</a>");
	list($width,$height,$imageType)=getimagesize($_FILES['file']['tmp_name']);
	$imageType=image_type_to_mime_type($imageType);
	if(!in_array($imageType,array("image/gif","image/pjpeg","image/jpeg","image/jpg","image/png","image/x-png"))) Throw new PDOException("incorrect file format, <a href='javascript:void(0)' onclick=\"window.parent.getObj('uploadImgDiv').getObjN('img')[0].click()\">re-upload</a>");
	
	$rand=new RandNumGen;
	$filename=$rand->init(10).".png";
	
	if(is_uploaded_file($_FILES['file']['tmp_name'])){
		$imgResizer=new ImageResizer();
		$imgResizer->load($_FILES['file']['tmp_name']);
		$imgResizer->resizeToHeight(($imgResizer->getHeight() > 565)?(565):($imgResizer->getHeight()));
		$imgResizer->save("{$path}/{$filename}",IMAGETYPE_PNG,NULL,0777);
		}
}
}
}
catch (PDOException $e){
	echo $e->getMessage();
}


if (file_exists("{$path}/{$filename}")){
    include_once 'lib/functions/imagePad.php';
	if(file_exists("{$path}/thumbnail/{$filename}")){
        imagePad("{$path}/thumbnail/{$filename}",205,205,"{$path}/thumbnail/{$filename}");
	}
	else{
		imagePad("{$path}/{$filename}",205,205,"{$path}/thumbnail/{$filename}");
	}
	echo "<img src='".((empty($filename))?($path.'/loading.gif'):($path.'/'.$filename))."'  id='srcimg'/>";
	echo "<script>window.parent.getObj('uploadImgDiv').getObjN('img')[0].src='{$path}/thumbnail/{$filename}?r='+Math.random();window.parent.getObj('img_name').value='{$filename}';</script>";
}
?>
<div id="draggable" class='draggable' style='position:fixed;left:0px;top:0px' onmousedown='dragObj(event)'>
<div class='tr box'></div>
<div class='tl box'></div>
<div class='bl box'></div>
<div class='br box'></div>
</div>
</body>
</html>
