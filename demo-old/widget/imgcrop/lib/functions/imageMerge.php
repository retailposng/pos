<?php
function imageMerge($bg,$filename,$w,$h,$save_as){
		$bg_im=imagecreatefrompng($bg);
		$imgRes=new ImageResizer;
		$imgRes->load($filename);
		$imgRes->resizeToWidth(($imgRes->getWidth()>$w)?($w):($imgRes->getWidth()));
		list($width,$height)=array($imgRes->getWidth(),$imgRes->getHeight());
		$dst=imagecreatetruecolor($w,$h);
		imagefilledrectangle($dst,0,0,$w,$h,imagecolorallocate($dst,255,255,255));
		imagecopy($dst,$imgRes->image,($w-$width)/2,($h-$height)/2,0,0,$width,$height);
		$pad=(imagesx($bg_im)-imagesx($dst))/2;
		imagecopymerge($bg_im,$dst,$pad,$pad,0,0,imagesx($dst),imagesy($dst),100);
		imagepng($bg_im,$save_as);
}
//imageMerge('g3972.png','qf8hyi00Fr.jpg',45,45,"qf8hyi00Fr.png");
?> 
