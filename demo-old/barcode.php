<?php
/* header("Pragma: no-cache");
header("Cache: no-cache");
header("Cache-Control: no-cache, must-revalidate");
header("Expires: Mon, 9 Jul 1995 05:00:00 GMT"); */
session_start();
$_SESSION=json_decode($_COOKIE["cred"],1);
$usrID=intval($_SESSION['usrID']);
$isCached=false;
include_once 'config.php';
if(isset($_GET["data"])){
	$item=[];
	$barcode=$pdo->quote($_GET["data"]);
	$stock=$pdo->query("select s.stockID,s.barcode,m.name as mname,p.name as pname,c.name as cname,s.cprice,s.rsprice,s.wsprice,s.qty,s.vat,s.description,i.name as iname,(select sum(x.qty) from stock x where x.stockID=s.stockID) as instock from (stock s join manufacturer m on m.manufacturerID join product p on p.productID join category c on c.categoryID join img i on i.imgID) where m.manufacturerID=p.manufacturerID and p.productID=s.productID and c.categoryID=p.categoryID and i.imgID=s.imgID and s.barcode=$barcode limit 1");
	$item=$stock->fetch(PDO::FETCH_ASSOC);
	/*
	if($stock->rowCount()==0 && isset($_GET['page']) && $_GET['page']=="stock"){
		$stock=$pdo->query("select s.stockID,s.barcode,m.name as mname,p.name as pname,c.name as cname,s.cprice,s.rsprice,s.wsprice,s.qty,s.vat,s.description,i.name as iname,(select sum(x.qty) from stock x where x.stockID=s.stockID) as instock from (stok s join manufacturer m on m.manufacturerID join product p on p.productID join category c on c.categoryID join img i on i.imgID) where m.manufacturerID=p.manufacturerID and p.productID=s.productID and c.categoryID=p.categoryID and i.imgID=s.imgID and s.barcode=$barcode limit 1");
	   $item=$stock->fetch(PDO::FETCH_ASSOC);
	}*/
	if(!is_null($item["vat"])){
		$item["vat"]=json_decode($item["vat"],1);
		//$vat=json_decode($item["vat"],1);
		/* if(in_array($vat["type"],["#inclusive","#exclusive"])){
			$item['vat_val']=$vat["val"];
			$item['vat_type']=$vat["type"];
		}
		$item["vat"]=NULL; */
	}
	$item['time']=time();
	$data=json_encode(($stock->rowCount() !=0 )?($item):(array("query"=>$_GET["data"],"time"=>$item['time'])));
	$pdo->exec("truncate table barcode;insert into barcode set data='{$data}',date={$item['time']}");
	include_once 'layout.php';
	$path=pathinfo(__FILE__);
	$layout=new Layout($path['filename']);
	if($stock->rowCount() !=0){
		$layout->title="Decoding barcode data...";
		$layout->content(NULL,"views/barcode/_preview.php");
	}
	else{
		$layout->title="Item not found in store inventory";
		$layout->content(NULL,"views/barcode/_notfound.php");
	}
	exit();
}

$barcode=$pdo->query("select data from barcode");
echo $barcode->fetch(PDO::FETCH_ASSOC)["data"];