<?php
session_start(); 
$_SESSION=json_decode($_COOKIE["cred"],1);
$usrID=intval($_SESSION['usrID']);
include_once 'layout.php';
include_once 'models/__user.php';
$path=pathinfo(__FILE__);
$layout=new Layout($path['filename']);
$layout->title="{$layout->controllers()["profile"]["name"]} | Users";
if(!in_array($_SESSION["roleID"],array(1,2))){
	header("Location: dashboard.php?auth=Access denied");
	exit(0);
}
switch (@$_GET["p"]) {
	case "add":
	case "edit":
		$layout->content(NULL,"views/user/_edit.php");
	break;
	default:
		$layout->content("views/user/_toolbar.php","views/user/_body.php");
	break;
} 
