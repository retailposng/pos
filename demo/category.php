<?php
session_start();
$_SESSION=json_decode($_COOKIE["cred"],1);
$usrID=intval($_SESSION['usrID']);
include_once 'layout.php';
include_once 'models/__category.php';
$path=pathinfo(__FILE__);
$layout=new Layout($path['filename']);
$layout->title="{$layout->controllers()["profile"]["name"]} | Stock categories";
switch (@$_GET["p"]) {
	case "add":
	case "edit":
		if(!in_array($_SESSION["roleID"],array(1,2))){
			header("Location: dashboard.php?auth=Access denied");
			exit(0);
		}
		$layout->content(NULL,"views/category/_edit.php");
		break;
	default:
		$layout->content("views/category/_toolbar.php","views/category/_body.php");
		break;
}
