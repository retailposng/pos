<?php
class SignInController{
	var $isOk=0,$err;
	function __construct($pdo,$post,$fields=array()){
		$this->pdo=$pdo;
		$this->post=$post;
		$this->fields=$fields;
		$this->profile=array();
		$this->validate();
	} 
	
	function filter(){
		$data=array();
		foreach($this->post as $k=>$v){
			if(in_array($k,$this->fields)){
				$data[$k]=$v;
			}
		}
		return $data;
	}
	
	function validate(){
		$data=$this->filter();
		try{
		foreach ($data as $k=>$v){
			if(empty($v)){
				Throw new PDOException("Incorrect Email or Password");
			}
			
		}
		   $email=$data['email'];
		   if(!filter_var(filter_var($email,FILTER_SANITIZE_EMAIL),FILTER_VALIDATE_EMAIL) && !preg_match("/^((234(?!0))|(0))(?!0)[0-9]{10}$/",$email,$matches)){
				Throw new PDOException("Incorrect Email or Password");
			}
			if(isset($matches)){
				$email=(strlen($matches[0])==11)?("234".substr($matches[0],1)):($matches[0]);
			}
			
			    $email=$this->pdo->quote($email);
				$pwd=$this->pdo->quote(sha1($data['pwd']));
				$res=$this->pdo->query("select u.usrID, u.name, u.roleID from usr u where (u.email=$email or u.phone=$email) and u.pwd=$pwd");
				$this->profile=$res->fetch(PDO::FETCH_NUM);
				if($res->rowCount()==0){
					Throw new PDOException("Incorrect Email or Password");
				}  
				$this->isOk=1;
		}
		catch (PDOException $ex){
			$this->isOk=0;
			$this->err=$ex->getMessage();
		}
	}
	}