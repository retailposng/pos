<?php
session_start();
$_SESSION=json_decode($_COOKIE["cred"],1);
$usrID=intval($_SESSION['usrID']);
include_once 'layout.php';
include_once 'models/__sale.php';
include_once 'lib/Time.php';
$path=pathinfo(__FILE__);
$layout=new Layout($path['filename']);
$layout->title="{$layout->controllers()["profile"]["name"]} | Sales";
$layout->content("views/sale/_toolbar.php","views/sale/_body.php");
?>  
