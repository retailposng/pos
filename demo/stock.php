<?php
session_start();
$_SESSION=json_decode($_COOKIE["cred"],1);
$usrID=intval($_SESSION['usrID']);
include_once 'layout.php';

$path=pathinfo(__FILE__);
$layout=new Layout($path['filename']);
$layout->title="{$layout->controllers()["profile"]["name"]} | Stocks";

if(!in_array($_SESSION["roleID"],array(1,2))){
	header("Location: dashboard.php?auth=Access denied");
	exit(0);
}

$pdo->exec("truncate barcode");

$config=$pdo->query("select * from config where configID=1");
$config=$config->fetch(PDO::FETCH_ASSOC);
if(isset($_GET["edit"]) && !empty($_GET["edit"])){
	$stockID=intval($_GET["edit"]);
	$stock=$pdo->query("select s.*,s.date as sdate,m.name as mname,p.name as pname,s.description,c.name as cname,i.name as iname,(select k.name from color k where k.colorID=s.colorID) as color,(select l.name from location l where l.locationID=s.locationID) as location from (stock s join manufacturer m on m.manufacturerID join product p on p.productID join category c on c.categoryID join img i on i.imgID) where m.manufacturerID=p.manufacturerID and p.productID=s.productID and c.categoryID=p.categoryID and i.imgID=s.imgID and s.stockID=$stockID");
	$stock=$stock->fetch(PDO::FETCH_ASSOC);
	$date=$pdo->quote($stock['date']);
	$supplier=$pdo->query("select * from supplier where supplierID=(select supplierID from purchase where date={$date} limit 1)");
	$supplier=$supplier->fetch(PDO::FETCH_ASSOC);
	if(!in_array($_SESSION["roleID"],array(1,2))){
		header("Location: dashboard.php?auth=Access denied");
		exit(0);
	}
	
	$layout->content(NULL,"views/stock/_add.php");
	exit();
}

$isDeleted=false;
if(isset($_POST["del"]) && !empty($_POST["del"])  && !empty($_POST["status"])){
	$stockID=intval($_POST["del"]);
	$status=$pdo->quote($_POST["status"]);
	$note=$pdo->quote($_POST["note"]);
	$pdo->exec("update stock set status=$status,note=$note where stockID=$stockID");
	$isDeleted=true;
	$layout->content("views/stock/_toolbar.php","views/stock/_body.php");
	exit();
}

switch (@$_GET["p"]) {
	case "add":
		if(!in_array($_SESSION["roleID"],array(1,2))){
			header("Location: dashboard.php?auth=Access denied");
			exit(0);
		}
		$layout->content(NULL,"views/stock/_add.php");
	break;
	case "multiple":
		if(!in_array($_SESSION["roleID"],array(1,2))){
			header("Location: dashboard.php?auth=Access denied");
			exit(0);
		}
		$layout->content(NULL,"views/stock/_addMultiple.php");
	break;
	default:
		$layout->content("views/stock/_toolbar.php","views/stock/_body.php");
	break;
} 
