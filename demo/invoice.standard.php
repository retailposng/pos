<?php 
/* print("<pre>");
print_r($_POST);
print("</pre>"); */
$hasDiscount=(array_sum($discount["each"]) > 0 || $discount["global"] > 0);
$hasSerialNo=count($data["serialNo"]) > 0;
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title><?=$config["name"];?> - Invoice</title>
    <link rel="stylesheet" href="../css/bootstrap.css">
    <style>
      body, h1, h2, h3, h4, h5, h6{
      font-family: tahoma, sans-serif;
      }
    </style>
  </head>
  <?php 
  flush();
  ?>
  <body onload="window.print();window.parent.Cart.Payment.success('<?=$invoice;?>',<?=$reload;?>);">
    <div class="container">
      <div class="row">
        <div class="col-xs-6">
          <h3>
           <img src="../images/logo/<?=$config["logo"];?>" alt="<?=$config["name"];?> Logo"/>
          </h3>
        </div>
        <div class="col-xs-6 text-right">
          <h1>INVOICE</h1>
          <h1><small>Invoice #: <?=$invoice;?>
          <br/>
          <?=date("d/m/Y");?>
          </small>
          
          </h1>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-6">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4><?=$config["name"];?></h4>
            </div>
            <div class="panel-body">
              <p>
                <strong>Address:</strong> <?=$config["addr"];?> <br>
                <strong>Email:</strong> <?=$config["email"];?><br/>
                <strong>Phone number: </strong> <?=$config["phone"];?>
              </p>
            </div>
          </div>
        </div>
        <div class="col-xs-5 col-xs-offset-1">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h4>Customer</h4>
            </div>
            <div class="panel-body">
            <strong>Name:</strong> <?=$customer_data["name"];?><br>
            <strong>Address:</strong> <?=$customer_data["addr"];?>
            </div>
          </div>
        </div>
      </div>
      <!-- / end client details section -->
      <table class="table table-bordered">
        <thead>
	     <tr><th>#</th><th>Item</th><?=($hasSerialNo)?("<th>Serial #</th>"):(NULL);?><th>Qty</th><th>Price</th><?=($hasDiscount)?("<th>Discount</th>"):(NULL);?><th>Line Total</th></tr>
   	 </thead>
        <tbody>
          <?php 
          $i=1;
          $linetotal=array();
          $subtotal=array();
          foreach ($data["items"] as $stockID=>$arr){
			$dis=number_format(($discount["each"][$stockID]+(($arr[2]*$discount["global"])/100)),2);
			//$desc=($config["item_desc"]==1)?("<br/><small>{$arr[5]}</small>"):(NULL);
			if(array_key_exists($stockID,$data["serialNo"])){
				for($j=0;$j<$arr[3];$j++){
				$subtotal[$i]=$arr[2];
				$linetotal[$i]=$subtotal[$i]-($discount["each"][$stockID]+(($arr[2]*$discount["global"])/100));
				
					?>
		          	<tr><td><?=$i;?></td><?=($j==0)?("<td rowspan='{$arr[3]}'>{$arr[1]}</td>"):(NULL);?><td><?=@$data["serialNo"][$stockID][$j];?></td><td>1</td><td><?=number_format($arr[2],2);?></td><?=($hasDiscount)?("<td>{$dis}</td>"):(NULL);?><td><?=number_format($linetotal[$i],2);?></td></tr>
		          	<?php
		          	$i++;
				}
			}
			else{
				
				$subtotal[$i]=$arr[2]*$arr[3];
				$linetotal[$i]=($subtotal[$i]-((($discount["each"][$stockID]*$arr[3])+((($arr[2]*$discount["global"])/100)*$arr[3]))));
          	?>
          	<tr><td><?=$i;?></td><td><?=$arr[1];?></td><?=($hasSerialNo)?("<td></td>"):(NULL);?><td><?=$arr[3];?></td><td><?=number_format($arr[2],2);?></td><?=($hasDiscount)?("<td>{$dis}</td>"):(NULL);?><td><?=number_format($linetotal[$i],2);?></td></tr>
          	<?php
          	$i++;
          	}
          	}
          ?>
          <tr><td colspan="<?=(4+intval($hasDiscount)+intval($hasSerialNo));?>"></td><td>₦<?=number_format(array_sum($linetotal),2);?></td></tr>
        </tbody>
      </table>
      <div class="row">
      <div class="col-xs-4 col-xs-offset-5 text-right">
       <h3>
         Paid:<br>
         Sub-total:<br>
         <?=($hasDiscount)?("Discounts:<br>"):(NULL);?>
         VAT:<br>
         Total:<br>
         Change:<br>
          </h3>
      </div>
        <div class="col-xs-3 text-left">
          <h3>
         ₦<?=number_format($data["paid"],2);?> <br>
         ₦<?=number_format(array_sum($subtotal),2);?> <br>
         <?=($hasDiscount)?("₦".number_format(array_sum($subtotal)-array_sum($linetotal),2)."<br>"):(NULL);?>
          ₦<?=number_format($data["vat"],2);?> <br>
         ₦<?=number_format(array_sum($linetotal)+$data["vat"],2);?> <br>
         ₦<?=number_format($data["paid"]-(array_sum($linetotal)+$data["vat"]),2);?> <br>
          </h3>
        </div>
      </div>
      <div class="row">
      <div class="col-md-12">
      <h3 class="lead">
      <?php 
      if (isset($_POST["onShowNote"])) {
      	echo $_POST["note"];
      }
      ?>
      </h3>
      </div>
      
      </div>
    </div>
  </body>
</html>