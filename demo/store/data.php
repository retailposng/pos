<?php
//ini_set("display_errors",1);
$m=date("m");
$y=date("Y");
$from=new DateTime(date("Y-m-d",mktime(0, 0, 0, $m,1,$y)));
$to=new DateTime(date("Y-m-d",mktime(0, 0, 0, $m+1, 0,$y)));

$bool=false;
if(isset($_POST["date"])){
	$bool=true;
	$date=new DateTime($_POST["date"]);
	$m=$date->format("m");
	$y=$date->format("Y");
	$from=new DateTime(date("Y-m-d",mktime(0, 0, 0, $m,1,$y)));
	$to=new DateTime(date("Y-m-d",mktime(0, 0, 0, $m+1, 0,$y)));
}

function getSalesRange($start,$end){
	global $pdo;
	$sales=$pdo->query("select sum((s.sprice*s.qty)-s.deduct) as netsale,sum(((s.sprice*s.qty)-s.deduct)- (s.cprice*s.qty)) as netprofit from (syncSale s) where s.status='sold' and (s.date >= $start and s.date < $end)");
	return $sales->fetch(PDO::FETCH_ASSOC);
}


$interval=$from->diff($to);
$diff=$interval->format('%a');

if($diff > 7 && $diff <= 31){
	$range[$from->format('M jS')]=array($from->format('Ymd'),date("Ymd",mktime(0, 0, 0, $from->format('m'),$from->format('d')+1,$from->format('Y'))));
	for($i=0;$i<$diff;$i++){
		$label=$from->format('M jS');
		$begin=$from->format('Ymd');
		$nxt=$from->add(new DateInterval("P1D"));
		$end=$nxt->format('Ymd');
		$range[$label]=array($begin, $end);
	}
	
	$range[$to->format('M jS')]=array($end,$to->format('Ymd')+1);
}


list($days,$sales,$profits)=array(array(),array(),array());
foreach ($range as $k=>$interval){
	$getData=getSalesRange($interval[0],$interval[1]);
	$days[]=$k/* ." ".$interval[0]." ".$interval[1] */;
	$sales[]=intval($getData["netsale"]);
	$profits[]=intval($getData["netprofit"]);
}

/* print("<pre>");
print_r($days);
print_r($sales);
print_r($profits);
print("</pre>"); */

$profitMargin=@round((array_sum($profits)/array_sum($sales))*100);

$_data=array("label"=>$days,"sales"=>$sales,"profits"=>$profits);
//print_r($data);
if($bool==true){
	//echo json_encode(array("_data"=>json_encode(array("label"=>$days,"sales"=>$sales,"profits"=>$profits)),"profitMargin"=>$profitMargin,"acquisitions"=>$acquisitions,"revenue"=>array_sum($sales),"income"=>array_sum($profits)));
}