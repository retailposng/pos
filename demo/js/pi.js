var g_index=[],memory=[];
sprintf=function(f){
	alert(arguments[0].split('%')+' '+arguments[1]);
};
var url_pattern=/^(http[s]?:\/\/){0,1}(www\.){0,1}[a-zA-Z0-9\.\-]+\.[a-zA-Z]{2,5}[\.]{0,1}$/i;
var email_pattern=/^.{3,}\@[0-9a-z\-]+\.[\. a-z]+$/i;
var phone_pattern=/^\(?([0-9]{3})\)?[-.]?([0-9]{3})[-.]?([0-9]{4,})$/;
var alpha=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
zerofill=function(arg){return (arg<10)?('0'+arg):(arg);};
// bind the event handlers
      function addEvent(elem, evtType, func) {
         if (elem.addEventListener) {
            elem.addEventListener(evtType, func, false);
         } else if (elem.attachEvent) {
            elem.attachEvent("on"+evtType, func);
         } else {
            elem["on"+evtType]=func;
         }
      }
      
      
     
      var Connection={img:new Image(),count:0,isRunning:false,time:new Date().getTime(),
    	status:"Connecting...",ping:function(){
    		this.img.src="http://www.google.com/favicon.ico?q="+Math.random();
    		this.img.onerror=function(){
    			Connection.status=false;
    			Connection.count++;
    			Notification.set("offline");
    		};
			 this.img.onload=function(){
				 Connection.status=true;
				 Notification.set("online");
				  };
				 return this.status; 
    	},
		run:function(func){
			Notification.set("init");
			this.isConnected=false;
			if(this.isRunning !== false) clearInterval(this.isRunning);
			this.isRunning=setInterval(function(){
				if(Connection.ping() === true){
					if(Connection.count > 2 || ((new Date().getTime()-Connection.time) > (1000*60*1))) {
						Connection.isConnected=false;
						Connection.count=0;
					}
					
					if(Connection.isConnected === false) {
						Connection.time=new Date().getTime();
						func();
					}
					
					 Connection.isConnected=true;
					 return false;
					}
					
					if(Connection.ping() === false){
					// alert("Offline "+Connection.count);
					}
			},2000);
		}	  
      };
      
      var Notification={
    		  set:function(arg){
    			  $('#noti_box').html({
    				  "init":"<span class='label label-info'><i class='glyphicon glyphicon-globe'></i>&nbsp;Checking Internet...</span>",
    				  "offline":"<span class='label label-danger'><i class='glyphicon glyphicon-remove'></i>&nbsp;No Internet Connection</span>",
    				  "online":"<span class='label label-success'><i class='glyphicon glyphicon-ok'></i>&nbsp;Connected To Internet</span>",
    				  "fetchSales":"<span class='label label-default'>Fetching sales...</span>",
    				  "refresh":"<span class='label label-warning'>Refresh page</span>",
    				  "synSales":"<span class='label label-default'>Synchronizing sales...</span>",
    				  "synchronized":"<span class='label label-success'><i class='glyphicon glyphicon-ok'></i>&nbsp;Synchronized</span>"
    					  }[arg]).show();
    		  },hide:function(){
    			  $('#noti_box').hide();
    		  },show:function(){
    			  $('#noti_box').show();
    		  }
      };
      
      
      function pathinfo(path){
    		var fp,fn;
    		if(!isDefined(path)) return {"filename":""};
    		fp=path.split("/");
    		fn=fp[fp.length-1];
    		return {"filename":fn};
    	}
      
   var getObj=function(id){return document.getElementById(id);};
   
   
    function evtObj(evt){return (isDefined(evt))?(evt.target):(window.event.srcElement);}
    function evtype(e){return (isDefined(e))?(e):(window.event);}
    
          //is defined
   var isDefined=function(obj){
    	if(obj==null || typeof obj=="undefined"){
    		return false;
    	}
    	return true;
    };

 // trim function
    String.prototype.trim=function(){return this.replace(/^[\s\xa0]+|[\s\xa0]+$/g, '');};
    String.prototype.stripHTML=function(){return this.replace(/(?:<[^>]+>)|(?:,)/g,' ');};///<(?:.|\s)*?>/g
    String.prototype.isEmpty=function(){
    	if(this.trim()=='' || this.trim()==null) {
    		return true;
    		}
    		return false;
    };
    
	
    function cache(uid,data){
   	 if(isDefined(memory[uid])){
   		 return memory[uid];
   	 }
   	 else{
   		 return memory[uid]=data;
   		 }
       }
    
    
function loadJS(src,func) {
	var scripts=window.document.head.getObjN('script');
	for (var i=0;i<scripts.length-1;i++) {
		if (pathinfo(scripts[i].src)['filename']==pathinfo(src)['filename']) return false;
	}
	var script=document.createElement("script");
	script.type="text/javascript";
	script.src=src;
	window.document.head.appendChild(script);
	addEvent(script,"load",func);
}    
    
		function ajaxThread(){//thread([url,args],function(e){},function(e){},function(e){});
			var i=0,thr=[],request,method;
			function f(args,processing,completed,error,dest){
				request=args[1] || null;
				method=(args[1])?("POST"):("GET");
				thr[i]=(window.XMLHttpRequest)?(new XMLHttpRequest()):((window.ActiveXObject)?(new ActiveXObject("Msxml2.XMLHTTP")):(new ActiveXObject("Microsoft.XMLHTTP")));
				thr[i].open(method,(args[0].indexOf('?')==-1)?(args[0]+"?"):(args[0])+'&rand='+Math.random(),true);
				if(args[2])
					thr[i].setRequestHeader("Content-Type","text/xml");
				 else if(args[1])
					 thr[i].setRequestHeader("Content-type", "application/x-www-form-urlencoded");
				thr[i].send(request);
				thr[i].onreadystatechange=function(){
				if (this.readyState < 4)
					processing(this);
				else if (this.readyState==4 && this.status==200)
					completed(this,dest || null);
				else
					error();
				};
				i++;
				return this;
			}
			return f;
		}
	
		function abort(e){
		    if(e){
		    	e.onreadystatechange=function(){};
		    	e.abort();
		    	e=null;
		    }
		  }
		
		
		function load(url,dest,func,srcObj){
			var unlock=trigger(function(args,func){
				abort(args[0]);
				func('timeout',args[1],args[2]);
				}
			,5,func);
			func('running',dest,srcObj);
			thread([url],function(e){
				unlock(e,dest,srcObj);
			},function(e){
				func('finished',dest,srcObj);
				getObj(dest).setHtml(e.responseText);
				unlock(e,dest,srcObj);
				},function(e){});
		}
//check form
function check(param,status,func){
var x=param,s=(status==1)?(true):(false);
if(typeof x == "object"){
	if(x.tagName=='FORM'){
for (var i=0;i<x.elements.length;i++){
	if (x.elements[i].type=="checkbox" && !x.elements[i].value.isEmpty()){
//x.elements[i].checked=s;
x.elements[i].click();
if (isDefined(func)) func(obj[i].value);
}
}
}
	else{
		obj=x.getElementsByTagName('input');
		for(var i=0;i<obj.length;i++){
			if (obj[i].type=="checkbox" && !obj[i].value.isEmpty()){
			//obj[i].checked=s;
				obj[i].click();
			if (isDefined(func)) func(obj[i].value);
			}
		}
	}
}
else{
	if(getObj(x).checked){
		//getObj(x).checked=false;
		getObj(x).click();
		if (isDefined(func)) func(getObj(x).value);
	}
	else{
	//	getObj(x).checked=true;
		getObj(x).click();
		if (isDefined(func)) func(getObj(x).value);
	}
	}
}

function toggle_check(obj,func){
	    // toggle checkbox
	   toggle(obj.id,function(){
		   check(obj,1,func);
	   },function(){
		   check(obj,0,func);  
	   });
}

function trigger(func,t,data){
	var i=0,proc=[],timer=[];
	function f(){
		var c=0,arr=[],n=0;
		for(var m=0;m<arguments.length;m++) arr[n++]=arguments[m];
		proc[i]=counter=function(){
			if(c==t) func(arr,data);
			c++;
		timer[i]=setTimeout(counter,500);
		};
		clearTimeout(timer[i]);
		proc[i++]();
		for(var j=0;j<proc.length;j++){
			proc[j]=null;
			delete proc[j];
			}
		return this;
		}
		return f;
		}
 

function Queue(t){//t=time interval,queue series of task to run
	var load=[];
    this.run=function(){
	timer=setInterval(function(){
	if(load.length==0) return false;
	load.shift()(load.length);
	},t);
	};
	this.add=function(task){
	load.push(task);
	};
	this.run();
}

function RQueue(arr,t){//recursive queue
	var load=[];
    this.run=function(){
	timer=setInterval(function(){
	if(load.length==0) load=load.concat(arr);
	load.shift()(load.length);
	},t);
	};
	this.run();
}
	var togVar=[];
	function toggle(key,func1,func2,src){
		if (!isDefined(togVar[key]) || togVar[key]==0){
			togVar[key]=1;
			func1(src);
			}
		else{
			togVar[key]=0;
			func2(src);
			}
	}
	
	function toggleOn(key){
		togVar[key]=1;
	}
	
	function toggleOff(key){
		togVar[key]=0;
	}
	
	function toggleReset(key){
		togVar[key]=0;
	}
	
	var n=0,regId=[],UID=function(obj){
		 for(var c in obj.parentNode.childNodes){
			 if (isDefined(obj.parentNode.childNodes[c]) && obj.parentNode.childNodes[c].tagName=="INPUT" && obj.parentNode.childNodes[c].type=="hidden"){
				 return obj.parentNode.childNodes[c].value; 
			 }
		 }
	with(Math){var i=round(random()*1000);}
	if(!regId.in_array(i)){
		regId[n++]=i;
	    var HI=document.createElement("input");
	    HI.type='hidden';
	   // HI.name='regId'; && obj.parentNode.childNodes[c].name=="regId"
	    HI.value=i;
	    obj.parentNode.appendChild(HI);
		return i;
	}
	else{
		return UID();
	}
	 };	

	var onMenu=function(evt,obj,func){//assign id to event source and div object
		var arr=[],c=0,iObj=evtObj(evt);
		function iter(obj){
			for(var i=0;i<obj.childNodes.length;i++){
				if(obj.childNodes[i].hasChildNodes()){
					arr[c++]=obj.childNodes[i];
					iter(obj.childNodes[i]);
				}
				else{
					arr[c++]=obj.childNodes[i];
				}
				
			}
		return arr;
		}
	
		window.onmousedown=function(evt){
			var nObj=evtObj(evt);
			
			if(nObj.id==obj.id) return false;//if main_div: ignore
			if(!iter(obj).in_array(nObj)){
				$(obj).hide();
				 //alert(iObj.id+" : "+nObj.id);
				if (nObj.id != iObj.id) {
					toggleReset(obj.id);
				}
				if(isDefined(func)) func();
			}
			};
	};

	cloneSelect=function(obj){
		var str='';
		for (var i=0;i<obj.options.length;i++){
			str+="<option value='"+obj.options[i].value+"'>"+obj.options[i].innerHTML+"<\/option>";
		}
		return str;
	};

					
		// accordion function
		accordion=function(param){
		for (var i=0;i<param[1].length;i++){
			(param[1][i]==param[0])?(getObj(param[0]).show()):(getObj(param[1][i]).hide());
			}
			};
			
				
				
			function fetchTableData(obj,f){
				var bd=obj.tBodies[0],attrs=[],arr=[],format=f || 'text';
				for(var i=0;i<bd.rows.length;i++){
					var arr1=[],cellAttrs=[bd.rows[i].getAttrs()];
					for(var j=0;j<bd.rows[i].cells.length;j++){
						cellAttrs[j+1]=bd.rows[i].cells[j].getAttrs();
						arr1[j]=(format=='text')?(bd.rows[i].cells[j].innerText):(bd.rows[i].cells[j].innerHTML);
						}
					    attrs[i]=cellAttrs;
						arr[i]=arr1;
					}
				return [arr,attrs];
				}
			
			
			
			function updateTableData(obj,data){
				var tb=obj.tBodies[0],arr=data[0],rw,ce0;
				for(var i=0;i<arr.length;i++){
				rw=tb.deleteRow(i);
				rw=tb.insertRow(i);
				rw.setAttrs(data[1][i][0]);
				ce0=rw.insertCell(0);
				ce0.innerHTML=(isNaN(Number(arr[i][0])))?(arr[i][0]):(i+1);
				ce0.setAttrs(data[1][i][1]);
				var ce1;
				for(var j=1;j<arr[i].length;j++){
					ce1=rw.insertCell(j);
					ce1.innerHTML=arr[i][j];
					ce1.setAttrs(data[1][i][j+1]);
				}
				}
				}
		
			function lock(flag){
				var div=document.createElement("div");
				    div.className="lock";
				    div.id="lock";
				if(flag){
					if(isDefined(getObj('lock'))) return false;
					window.document.body.style.overflowY='hidden';
					window.document.body.appendChild(div);
					return false;
				}
				if(isDefined(getObj('lock'))) {
					window.document.body.style.overflowY='auto';
					window.document.body.removeChild(getObj('lock'));
				}
			}
			

					function search_data(obj,uid,col){
						 //store search data in memory
						 if(isDefined(g_index[uid])){
							 return g_index[uid];
						 }
						 else{
							return g_index[uid]=[obj.fetchTableData(),obj.fetchDataByCols(col)];
						 }
						}
					
					
					var SearchList={data:[],delay:2,cache:[],founds:[],inCart:false,
						config:function(obj){
						this.delay=(isDefined(obj["delay"]))?(obj["delay"]):(this.delay);
					},filter:trigger(function(data){
						var strongKeyword=$('#search_strong_keyword').val();
						SearchList.model((!strongKeyword.isEmpty())?(strongKeyword+" "+data[0]):(data[0]),data[1],data[2]);
					},1),
							query:function(uid){
								if(isDefined(this.cache[uid])){
									 return this.cache[uid];
								 }
								 else{
									 var data=[];
									 this.box=$('#item_box').find("div").filter(function(){ return this.hasAttribute('data-search');});
									 for(var i=0;i<this.box.length;i++){
										 data[i]=[this.box[i].getAttribute("data-search")];
										 this.data[i]=this.box[i].outerHTML;
									 }
									 
									return this.cache[uid]=data;
								 }
							},more:function(keyword){
							    $('#item_box').html("<div class='clear'>&nbsp;</div><div class='alert alert-info margin10'>Please wait... searching store</div>");
								thread(["views/pos/more.php",$.param({"keyword": keyword,"founds": JSON.stringify(this.founds)})],function(xhr){},function(xhr){
								//alert(xhr.responseText);
									if(xhr.responseText.isEmpty()){
										if(SearchList.founds.length > 0){
										   SearchList.filter(keyword,Cart.check,2348034309999);
										}
										else{
											$('#item_box').html("<div class='clear'>&nbsp;</div><div class='alert alert-warning margin10'>No match found</div>");
										}
										return false;
									}
									//alert(JSON.parse(xhr.responseText));SearchList.data.join('')+
									$('#item_box').html(xhr.responseText);
									//SearchList.cache=[];
									//SearchList.filter(keyword,Cart.check,2348034309999);
									//alert(xhr.responseText);
								},function(xhr){});
							},model:function(keyword,func,key){
								if(keyword.isEmpty()){
									$('#item_box').html("<div class='row no-margin'>"+this.data.join("")+"</div>");
									if(isDefined(func)) func(keyword);
									return false;
								}
								var data=this.query(key).array_search(keyword).load(this.data);
								if(data.length==0) {
									this.more(keyword);
									this.founds=[];
									return false;
								}
								//if(keyword.isEmpty()) dataIndex=range(0,dataIndex.length);
								var str=[],founds=[];
								for(var i=0;i<data.length;i++) {
									//if(str.in_array(data[i])) continue;
									str.push(data[i]);
								}
								
								$('#item_box').html("<div class='row no-margin'>"+str.join("")+"</div>");
								if(!keyword.isEmpty()) {
									var itemBox=$("#item_box").find("div").filter(function(index){return this.hasAttribute("data-stockID");});
									for(var i=0;i<itemBox.length;i++) founds.push(itemBox[i].getAttribute("data-stockID"));
									this.founds=founds;
									//alert(JSON.stringify(stocks));
									$('#item_box').append("<div class='no-padding margin10'><button class='btn btn-block btn-info' onclick=\"SearchList.more('"+keyword+"')\">Click to load more similar items</button></div>");
								}
								if(isDefined(func)) func(keyword);
							}
					};
					
					
					

					var xSearchList={data:[],cache:[],
						filter:trigger(function(data){
						xSearchList.model(data[0],data[1]);
					},0),
							query:function(uid){
								if(isDefined(this.cache[uid])){
									 return this.cache[uid];
								 }
								 else{
									 var data=[];
									 this.box=$('#item_box').find("div").filter(function(){ return this.hasAttribute('data-search');});
									 for(var i=0;i<this.box.length;i++){
										 data[i]=[$(this.box[i]).find("span.search-data").text()];
										 this.data[i]=this.box[i].outerHTML;
									 }
									
									return this.cache[uid]=data;
								 }
							},model:function(keyword,func){
								var dataIndex=this.query("534536").array_filter(keyword,"");
								if(dataIndex.length==0){
									$('#item_box').html("<div class='clear'>&nbsp;</div><div class='alert alert-warning'>No match found</div>");
									return false;
								}
								if(keyword.isEmpty()) dataIndex=range(0,dataIndex.length);
								var str=[],c=4,k=0;
								for(var i=0;i<dataIndex.length;i++){
									str.push(this.data[dataIndex[i]]+"<div class='clear'>&nbsp;</div>");
								}
								$('#item_box').html(str.join(""));
								if(isDefined(func)) func(keyword);
							}
					};					 
					 
				
					//column sorter
					 function columnSorter(obj,order){
						 var arr,tbObj=getElementsByClassName(document,"table","sortable")[0],data=tbObj.fetchTableData(),tb=tbObj.tBodies[0];
                       	 arr=tbObj.fetchDataByCols([obj.cellIndex]);
						 keys=arr.array_map_keys(arr.array_sort(order));
						 tbObj.removeChild(tb);
						 tb=document.createElement("tbody");
						 for(var i=0;i<keys.length;i++) tb.insertRow(i).outerHTML=data[keys[i]];
						 tbObj.appendChild(tb);
						 for(var i=0;i<tb.rows.length;i++) tb.rows[i].cells[0].innerHTML=i+1;
						 	}
					 
						
						
						function clock(obj){
							function f(){
							 var d=new Date(),digits,readout='';
							  digits=d.getHours();
							  readout+=(digits > 9 ? '':'0')+(digits%12)+':';
							  digits=d.getMinutes();
							  readout+=(digits > 9 ? '':'0')+digits+':';
							  digits=d.getSeconds();
							  readout+=(digits > 9 ? '':'0')+digits;
							  obj.innerHTML=readout;
							}
							setInterval(function(){f();},1000);
							}
						
						function money(base){
							var moni=formatTo(base,2).split('.'),res='',v='';
							for(var i=moni[0].length-1,j=1;i>=0;i--,j++){
								res +=moni[0][i];
								if(j%3==0 && i!=0) res +=',';
							}
							for(var k=res.length-1;k>=0;k--) v +=res[k];
							return v+"."+moni[1];
						}
						
						function formatTo(base,precision){
						  var a,s;
						  a=roundTo(base,precision);
						  s=a.toString();
						  var decimalIndex=s.indexOf(".");
						  if (precision > 0 && decimalIndex < 0){
						    decimalIndex=s.length;
						    s+='.';
						  }
						  while (decimalIndex+precision+1 > s.length){
						    s+='0';
						  }
						  return s;
						}

						function roundTo(base,precision){
						  var m,a;
						  m=Math.pow(10,precision);
						  a=Math.round(base*m)/m;
						  return a;
						}	
						
						function getElementsByClassName(oElm,strTagName,oClassNames){
							var arrReturnElements = new Array();
							if ( typeof( oElm.getElementsByClassName ) == "function" ) {
								/* Use a native implementation where possible FF3, Saf3.2, Opera 9.5 */
								var arrNativeReturn = oElm.getElementsByClassName( oClassNames );
								if ( strTagName == "*" )
									return arrNativeReturn;
								for ( var h=0; h < arrNativeReturn.length; h++ ) {
									if( arrNativeReturn[h].tagName.toLowerCase() == strTagName.toLowerCase() )
										arrReturnElements[arrReturnElements.length] = arrNativeReturn[h];
								}
								return arrReturnElements;
							}
							var arrElements = (strTagName == "*" && oElm.all)? oElm.all : oElm.getElementsByTagName(strTagName);
							var arrRegExpClassNames = new Array();
							if(typeof oClassNames == "object"){
								for(var i=0; i<oClassNames.length; i++){
									arrRegExpClassNames[arrRegExpClassNames.length]=new RegExp("(^|\\s)" + oClassNames[i].replace(/\-/g, "\\-") + "(\\s|$)");
								}
							}
							else{
								arrRegExpClassNames[arrRegExpClassNames.length]=new RegExp("(^|\\s)" + oClassNames.replace(/\-/g, "\\-") + "(\\s|$)");
							}
							var oElement;
							var bMatchesAll;
							for(var j=0; j<arrElements.length; j++){
								oElement = arrElements[j];
								bMatchesAll = true;
								for(var k=0; k<arrRegExpClassNames.length; k++){
									if(!arrRegExpClassNames[k].test(oElement.className)){
										bMatchesAll = false;
										break;
									}
								}
								if(bMatchesAll){
									arrReturnElements[arrReturnElements.length] = oElement;
								}
							}
							return (arrReturnElements);
						}
						
						
						
						
						function scrollPosition(){
							var pos=[0,0];
							if (typeof window.pageYOffset != 'undefined'){
							pos=[window.pageXOffset,window.pageYOffset];
							}
							else if (typeof document.documentElement.scrollTop != 'undefined' && document.documentElement.scrollTop > 0){
							pos=[document.documentElement.scrollLeft,document.documentElement.scrollTop];
							}
							else if (typeof document.body.scrollTop != 'undefined'){
							pos=[document.body.scrollLeft,document.body.scrollTop];
							}
							return pos;
							}

						function cursorPosition(e){
							var evt=evtype(e),scrollpos=scrollPosition(),cursorpos=[0,0];
							if (typeof evt.pageX != "undefined" && typeof evt.x != "undefined"){
							cursorpos[0]=evt.pageX;
							cursorpos[1]=evt.pageY;
							}
							else{
							cursorpos[0]=evt.clientX+scrollpos[0];
							cursorpos[1]=evt.clientY+scrollpos[1];
							}
							return cursorpos;
							}
						
					
						
						function setCookie(sName,sValue,oExpires,sPath,sDomain,bSecure){
							var sCookie=sName+"="+encodeURIComponent(sValue);
							if (oExpires){
							sCookie +=";expires="+oExpires.toGMTString();
							}
							if (sPath){
							sCookie +=";path="+sPath;
							}
							if (sDomain){
							sCookie +=";domain="+sDomain;
							}
							if (bSecure){
							sCookie += ";secure";
							}
							document.cookie=sCookie;
							}
						
						
						function getCookie(sName){
							var sRE="(?:; )?"+sName+"=([^;]*);?";
							var oRE=new RegExp(sRE);
							if (oRE.test(document.cookie)){
							return decodeURIComponent(RegExp["$1"]);
							} 
							else {
							return null;
							}
							}

						function deleteCookie(sName,sPath,sDomain) {
							setCookie(sName,"",new Date(0),sPath,sDomain);
							}
						
function ScrollableTabs(o){
var obj=getObj(o),buttons=obj.getObjN2('button',function(o){ return o.className=='st000';}),ulObj=obj.getObjN2('ul',function(o){ return o.className=='st000';})[0],liObj=ulObj.getObjN('li');

buttons[0].onclick=function(){
	if(liObj.length <= 4) return false;
	li=document.createElement("li"),attrs=liObj[0].getAttrs();
	li.innerHTML=liObj[0].innerHTML;
	li.setAttrs(attrs);
	ulObj.removeChild(liObj[0]);
	ulObj.appendChild(li);
	};
	
buttons[1].onclick=function(){
	if(liObj.length <= 4) return false;
    li=document.createElement("li"),attrs=liObj[liObj.length-1].getAttrs();
	li.innerHTML=liObj[liObj.length-1].innerHTML;
	li.setAttrs(attrs);
	ulObj.removeChild(liObj[liObj.length-1]);
	ulObj.insertBefore(li,ulObj.firstChild);
	};
	
	this.add=function(func,box){//callback function & minimized box
		var li=document.createElement("li"),bt=(getObj(box).title.length > 13)?(getObj(box).title.substring(0,10)+"..."):(getObj(box).title);
		li.innerHTML=bt+"<a href='javascript:void(0)' title='Remove' style='float:right'><i class='icon-remove'></i></a>";
		li.title=getObj(box).title;//getObj(box).getAttribute("rec")
		li.id=box+"_tab";
		addEvent(li,'click',function(){
			func();
			sat.remove(li);
			}
		);
		ulObj.insertBefore(li,ulObj.firstChild);
		if(liObj.length > 0) getObj(o).show();
		addEvent(li.getObjN("a")[0],'click',function(){
			sat.remove(li);
			getObj(box).parentNode.removeChild(getObj(box));
		});
	};
	
	this.remove=function(li){
		ulObj.removeChild(li);
		if(liObj.length==0) getObj(o).hide();
	};
	
}

function getRel(arr,obj){//get relatives
		var rels=[],c=0;
		for(var i=0;i<arr.length;i++){
			if(!isDefined(document.elementFromPoint(arr[i][0],arr[i][1]))) return false;
			var targetObj=document.elementFromPoint(arr[i][0],arr[i][1]).parentNode;
			if(targetObj.nodeName==obj.nodeName && targetObj.getAttribute("data-draggable")==obj.getAttribute("data-draggable")) rels[c++]=targetObj;
			}
			return rels;//return relatives
		}
	
	var activeBox=function(x,y,obj){//put active box to front
		var targetObj=getRel([[x-1,y-1],[x-1+obj.offsetWidth,y-1],[x-1,y-1+obj.offsetWidth],[x+2+obj.offsetWidth,y+2+obj.offsetWidth]],obj);
        if(targetObj.length > 0){
			obj.style.zIndex="20";
			for(var i=0;i<targetObj.length;i++) targetObj[i].style.zIndex="10";
	     }
        };

     
        function toggleBButton(obj,box){
        	var box_wrap=box || $(obj.parentNode),butObj=box_wrap.find(obj.tagName);
        	for(var i=0;i<butObj.length;i++){
        		if(obj.tagName.toLowerCase()=="a"){
        			(butObj[i].href==obj.href)?($(butObj[i]).addClass("active")):($(butObj[i]).removeClass("active"));
        		}
        		else{
        			(butObj[i].value==obj.value)?($(butObj[i]).addClass("active")):($(butObj[i]).removeClass("active"));
        		}
        	}
        	if(isDefined(box_wrap.find("input"))) box_wrap.find("input").val(obj.hash);
        	
        }
        
       
        function template(tmpl,data){
            return tmpl.replace(/%(\w*)%/g,function(m,key){return data.hasOwnProperty(key)?data[key]:"";});
        }
        
       
 		function prependChild(Obj,obj){
 	    	 Obj.insertBefore(obj,Obj.firstChild);
 	      }
 	      
 	      
 	    function getObjN(Obj,o){
 	    	return Obj.getElementsByTagName(o);
 		}
 	   
 	   function getObjN2(Obj,o,func){
 		   var arr=[],c=0,obj=Obj.getElementsByTagName(o);
 		   for(var i=0;i<obj.length;i++){
 				if ((isDefined(func) && func(obj[i])) || !isDefined(func)){
 				arr[c++]=obj[i];
 				}
 			}
 		   return arr;
 		   }
 	   
 	 
 	   
 	   //format time
 	   Date.prototype.toTime=function(){
 		   var i=this.getMinutes(),h,hour,min,GMT;
 		   if(this.getHours()>12){
 		   	h=(this.getHours()-12);
 		   	GMT="PM";
 		   }
 		   else{
 		   	h=this.getHours();
 		   	GMT="AM";
 		   	}
 		   hour=(h<10)?("0"+h):(h);
 		   min=(i<10)?("0"+i):(i);
 		   return (hour+":"+min+" "+GMT).toString();
 		   };
 	 // trim function
 	    String.prototype.trim=function(){return this.replace(/^[\s\xa0]+|[\s\xa0]+$/g, '');};
 	    String.prototype.stripHTML=function(){return this.replace(/(?:<[^>]+>)|(?:,)/g,' ');};///<(?:.|\s)*?>/g
 	    String.prototype.isEmpty=function(){
 	    	if(this.trim()=='' || this.trim()==null) {
 	    		return true;
 	    		}
 	    		return false;
 	    };
 	    
 	    String.prototype.htmlEscape=function(){
 	        return String(this).replace(/&/g, '&amp;').replace(/"/g, '&quot;').replace(/'/g, '&#39;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/(\n)/g,'<br/>');
 	    };
 	    
 	    
 	    String.prototype.htmlEncode=function(){
 	    	   var div=document.createElement('div');
 	    	   var text=document.createTextNode(this);
 	    	   div.appendChild(text);
 	    	   return div.innerHTML;
 	    	};
 	    
 	    center=function(Obj,x,y){
 	    	var top,left;
 	    	Obj.style.position="fixed";
 	    	if((isDefined(x) && !x.isEmpty()) && (isDefined(y) && !y.isEmpty())){
 	    		Obj.style.top=(y+"px");
 	        	Obj.style.left=(x+"px");
 	        	return false;
 	    	}
 	    	
 	    	top=Math.round((window.innerHeight-Obj.offsetHeight)/2);
 	    	left=Math.round((window.innerWidth-Obj.offsetWidth)/2);
 	    	Obj.style.top=(top<0)?(0):(top+"px");
 	    	Obj.style.left=(left<0)?(0):(left+"px");
 	    }

 	    
 	    		
 					 
 					disable=function(Obj,bg){
 						  if(isDefined(getObj(Obj.id+'_disable'))) return false;
 						  var obj=document.createElement("div");
 							 obj.className="disable";
 							 obj.id=Obj.id+'_disable';
 							 Obj.appendChild(obj);
 							 if(isDefined(bg)) obj.style.backgroundColor=bg;
 							 return true;
 					 }
 					 
 					 
 					 loading=function(Obj,src){
 							var img=new Image();
 							img.style.position="absolute";
 							img.src=src;
 							Obj.appendChild(img);
 							img.center();
 						}
 					 
 					 enable=function(Obj){
 						 if(!isDefined(getObj(Obj.id+'_disable'))) return false;
 						 Obj.removeChild(getObj(Obj.id+'_disable'));
 					 }
 					 
 					getPosition=function(Obj){
						var posX=0,posY=0,obj=Obj;
						while (obj != null){
						posX += obj.offsetLeft;
						posY += obj.offsetTop;
						obj=obj.offsetParent;
						}
					return [posX, posY];
					};