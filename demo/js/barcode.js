var Barcode={data:null,time:0,form:null,ready:function(formObj){
	this.form=formObj;
	$(this.form["activate"]).html("<i class='glyphicon glyphicon-ok'></i> Ready for Barcode Reader");
	$(this.form["activate"]).removeClass("btn-danger");
	$(this.form["activate"]).addClass("btn-success");
},notReady:function(formObj){
	this.form=formObj;
	$(this.form["activate"]).html("<i class='glyphicon glyphicon-remove'></i> Not Ready for Barcode Reader");
	$(this.form["activate"]).removeClass("btn-success");
	$(this.form["activate"]).addClass("btn-danger");
},reader:function(formObj){
	//formObj['data'].value=formObj['data'].value.replace(/[a-z]+/g,'');
	formObj['data'].value=formObj['data'].value;
	formObj.target=$('#barcodeFrame').get(0).name;
	setTimeout(function(obj){
		obj.reset();
	},100,this.form);
},pos:function(){
	//$("#barcode_control").focus();
	thread(["barcode.php"],function(xhr){},function(xhr,obj){
		var stock=JSON.parse(xhr.responseText);
		if(!isDefined(stock["barcode"]) || (obj.data==stock["barcode"] && obj.time==stock["time"])) return false;
		obj.data=stock["barcode"];
		obj.time=stock["time"];
		Cart.add([stock["stockID"],stock["mname"]+" "+stock["pname"],stock["rsprice"],1,'btn btn-xs fg-color-lBlack',stock["description"],((stock["vat"]["type"]=="#exclusive")?(stock["vat"]["val"]):(0)),stock["qty"],stock["wsprice"],'retail']);
	},function(xhr){},this);
},
							addStock:function(){
								thread(["barcode.php?page=stock"],function(xhr){},function(xhr,obj){
									//alert(xhr.responseText);
									var stock=$.parseJSON(xhr.responseText),barcode=stock["barcode"] || stock["query"];
									if(obj.data==barcode && obj.time==stock["time"]) return false;
									obj.data=barcode;
									obj.time=stock["time"];
									if(isDefined(stock["query"])) {
										$("form.stock-form")[0].reset();
										$("#product_img").attr("src","img/burger1.png");
										$("form.stock-form")[0]["barcode"].value=barcode;
										thread(["views/barcode/upc-lookup.php",$.param({"barcode":barcode})],function(xhr){},function(xhr,obj){
										//$("form.stock-form")[0]["description"].value=xhr.responseText;
										var stock=JSON.parse(xhr.responseText);
										if(stock["results_count"]==0) return false;
										obj.fill({"barcode":barcode,"product":stock["results"][0]["name"],"manufacturer":stock["results"][0]["manufacturer"],"category":stock["results"][0]["category"],"color":stock["results"][0]["color"]});
									  
										},function(xhr){
										
										},obj);
										
										return false;
									}
									obj.fill({"barcode":barcode,"product":stock["pname"],"manufacturer":stock["mname"],"category":stock["cname"],"cprice":stock["cprice"],"rsprice":stock["rsprice"],"wsprice":stock["wsprice"],"description":stock["description"],"vat":stock["vat"],"icon":"views/upload/thumbnail/"+stock["iname"]});

									
								},function(xhr){
									
								},this);
							},fill:function(stock){
								var formObj=$("form.stock-form")[0];
								for(var k in stock){
									if(formObj.hasOwnProperty(k)){
									  if(!isDefined(formObj[k].value)) formObj[k].innerHTML=stock[k];
									  formObj[k].value=stock[k];
									}
								}
								$("#product_img").attr("src",stock["icon"]);
								
								if(isDefined(stock["vat"])){
									$("#tax_box").find("a").filter(function(){
										formObj["vat["+stock["vat"]["type"]+"]"].value=stock["vat"]["val"];
										return this.hash==stock["vat"]["type"];
									}).click();
								}
							}
					};
