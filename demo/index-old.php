<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Retail store point of sale</title>    
    <link href="css/styles.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  
  <div class="clearfix">&nbsp;</div>
  <div class="clearfix">&nbsp;</div>
  <div class="clearfix">&nbsp;</div>
  <div class="clearfix">&nbsp;</div>
 <div class="row">
 <div class="col-lg-12">
  <?php 
            if(isset($_GET["auth"])){
            ?>
            <div class="alert alert-warning"><?=$_GET["auth"];?></div>
            <?php 
            }
            ?>
 </div>
 </div>
  <div class="row">
			<div class="col-md-4"></div>
			<div class="col-md-4 well">
			 <form action="models/__login.php" method="post" id="login" role="form">
            <h3 class="form-signin-heading">Login</h3>
            <input type="text" class="form-control" name="email" id="loginEmail" placeholder="Email address" required autofocus>
            <div class="clear">&nbsp;</div>
            <input type="password" class="form-control" name="pwd" id="loginPass" placeholder="Password" required>
           <div class="clear">&nbsp;</div>
            <button  class="btn btn-lg btn-primary btn-block" type="submit" name="login">Login</button>
          </form>
			</div>
			<div class="col-md-4"></div>
			</div>
  
    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
    <script type="text/javascript">
   
    	    
    </script>
  </body>
</html>
