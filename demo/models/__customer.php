<?php
$isOk=false;
$err='';
if (isset($_POST["customer"])) {
	try{
		if(empty($_POST["name"])) throw new Exception(NULL,1);
		if(isset($_GET["p"]) && $_GET["p"]=="edit"){
			$customer=$pdo->prepare("update ignore customer set name=:name,phone=:phone,email=:email,addr=:addr where customerID=:customerID");
			$customer->execute(array("name"=>$_POST["name"],"phone"=>$_POST["phone"],"email"=>$_POST["email"],"addr"=>$_POST["addr"],"customerID"=>intval($_POST["id"])));
		}
		else{
			$customer=$pdo->prepare("insert into customer set name=:name,email=:email,phone=:phone,addr=:addr,date=unix_timestamp()");
			$customer->execute(array("name"=>$_POST["name"],"email"=>$_POST["email"],"phone"=>$_POST["phone"],"addr"=>$_POST["addr"]));
		}
		$isOk=true;
	}
	catch (Exception $e) {
		$err=$e->getCode();
	}
}