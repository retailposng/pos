<?php
$isOk=false;
$err=NULL;
if (isset($_POST['reset']) && $_POST['reset'] !=@$_SESSION['reset']){
	$_SESSION['reset']=$_POST['reset'];
	try {
		if(empty($_POST['oldpwd']) || empty($_POST['pwd1']) || empty($_POST['pwd2'])){
			Throw new Exception("All fields are required.");
		}

		if(strlen($_POST['pwd1']) < 4 || strlen($_POST['pwd2']) < 4){
			Throw new Exception("Password length too short, <small>minimum of 4 characters required</small>");
		}

		if(strlen($_POST['pwd1']) != strlen($_POST['pwd2'])){
			Throw new Exception("Password does not match.");
		}


		$pwd=$pdo->quote(sha1($_POST['oldpwd']));
		$check_pwd=$pdo->query("select * from usr where pwd=$pwd and usrID=$usrID");
		if($check_pwd->rowCount()==0) Throw new Exception("Invalid old password.");
		$newpwd=$pdo->quote(sha1($_POST['pwd1']));
		if($pdo->exec("update usr set pwd=$newpwd where usrID=$usrID")){
			$err="<div class='alert alert-success'>Password successfully changed</div>";
		}
		$isOk=true;
	} catch (Exception $ex) {
		$err="<div class='alert alert-danger'>".$ex->getMessage()."</div>";
	}
}