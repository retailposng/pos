<?php
ini_set("display_errors",0);
include '../config.php';

session_start();
$_SESSION=json_decode($_COOKIE["cred"],1);
$usrID=intval($_SESSION['usrID']);
print("<pre>");
print_r($_POST);
print("</pre>");
//exit(0);
function getID($tb,$data){
	global $pdo;
	$str=array();
	foreach (array_keys($data) as $col) $str[]="{$col}=:{$col}";
	$cols=join(",",$str);
	$queryID=$pdo->prepare("insert into {$tb} set {$cols} on duplicate key update {$tb}ID=LAST_INSERT_ID({$tb}ID),{$cols}");
	$queryID->execute(array_map(function($v){ return str_replace("'","",$v);},$data));
	return $pdo->lastInsertId();
}
try{
	$pdo->beginTransaction();
	$email=NULL;
	$phone=NULL;
	$product=$_POST["product"];
	$qty=$_POST["qty"];
	if(!empty($_POST['email'])){
		if(!filter_var($_POST['email'],FILTER_VALIDATE_EMAIL))
			Throw new PDOException("Invalid Email Address");
		$email=filter_var($_POST['email'],FILTER_SANITIZE_EMAIL);
	}
	
	foreach (array("manufacturer","category","product","supplier","icon") as $v){
		if(isset($_POST[$v]) && empty($_POST[$v])) {
			Throw new PDOException("{$v} is required");
		}
	}
	
	$codes=NULL;
	if(isset($_POST["codes"])){
		$codes=$_POST["codes"];
		if (count($codes) < $_POST["qty"]) Throw new PDOException("Product codes are less than it quantity");
		if(count(array_unique($codes)) < count($codes)) Throw new PDOException("Duplicate product codes found");
		$codes=json_encode($codes);
	}
	if (!empty($_POST["phone"])) $phone=$_POST["phone"];
	$imgURL=(filter_var($_POST['icon'],FILTER_VALIDATE_URL))?($_POST['icon']):("../{$_POST['icon']}");
	$img_path=pathinfo($imgURL);
	if(!file_exists("../views/upload/large/{$img_path["basename"]}")) {
		include_once '../lib/smartResizeImage.php';
		include_once '../lib/RandNumGen.php';
		$rand=new RandNumGen();
		$img_path["basename"]=$rand->init(12).".png";
		copy($imgURL,"../views/upload/large/{$img_path["basename"]}");
		smart_resize_image("../views/upload/large/{$img_path["basename"]}",64,64,true,"../views/upload/thumbnail/{$img_path["basename"]}");
	}
	$time=time();
	$imgID=getID("img",array("name"=>$img_path["basename"]));
	$manufacturerID=getID("manufacturer",array("name"=>$_POST['manufacturer']));
	$categoryID=getID("category",array("name"=>$_POST['category']));
	$productID=getID("product",array("name"=>$_POST['product'],"manufacturerID"=>$manufacturerID,"categoryID"=>$categoryID));
	$supplierID=getID("supplier",array("name"=>$_POST['supplier'],"phone"=>$phone,"email"=>$email,"addr"=>$_POST["addr"],"usrID"=>$usrID));
	
	if(!empty($_POST['location'])) $locationID=getID("location",array("name"=>$_POST['location']));
	if(!empty($_POST['color'])) $colorID=getID("color",array("name"=>@$_POST['color']));
	$size=@$_POST['size'];
	$description=@$_POST["description"];
	$barcode=@$_POST["barcode"];
	$vat=json_encode(array("type"=>@$_POST['vat']["type"],"val"=>@$_POST['vat'][@$_POST['vat']["type"]]));
	
	$getSKU=$pdo->query("select * from stock where productID=$productID");
	$SKU=sprintf("%s%03d%s",substr($_POST['manufacturer'],0,1),$getSKU->rowCount()+1,substr($_POST['product'],0,2));
	$stockID=intval(@$_POST["stockID"]);
	$stock=$pdo->prepare("insert into stock set stockID=:stockID,productID=:productID,SKU=:SKU,barcode=:barcode,qty=:qty,imgID=:imgID,cprice=:cprice,rsprice=:rsprice,wsprice=:wsprice,size=:size,locationID=:locationID,colorID=:colorID,description=:description,vat=:vat,date=:date on duplicate key update stockID=if(:stockID=0,stockID,:stockID),barcode=:barcode,productID=:productID,qty=if(:stockID > 0,:qty,qty+:qty),imgID=:imgID,cprice=:cprice,rsprice=:rsprice,wsprice=:wsprice,size=:size,locationID=:locationID,colorID=:colorID,description=:description,vat=:vat,date=:date");
	$stock->execute(array("stockID"=>$stockID,"productID"=>$productID,"SKU"=>$SKU,"barcode"=>$barcode,"qty"=>$qty,"imgID"=>$imgID,"cprice"=>$_POST["cprice"],"rsprice"=>$_POST["rsprice"],"wsprice"=>$_POST["wsprice"],"size"=>$_POST["size"],"locationID"=>@$locationID,"colorID"=>@$colorID,"description"=>$description,"vat"=>$vat,"date"=>$time));
	$purchaseID=getID("purchase",array("stockID"=>$pdo->lastInsertId(),"codes"=>$codes,"qty"=>$qty,"supplierID"=>$supplierID,"receiverID"=>$usrID,"expiry"=>@$_POST["expiry"],"note"=>$_POST["note"],"date"=>$time));
	echo "<script>window.parent.Stock.success($purchaseID);</script>";
	$pdo->commit();
}
catch (PDOException $ex) {
	$pdo->rollBack();
	echo $err=$ex->getMessage();
	echo "<script>window.parent.Stock.error('$err');</script>";
}