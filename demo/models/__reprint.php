<?php
ini_set("display_errors",1);
session_start();
session_regenerate_id();
include_once '../config.php';
$invoice=$_GET["invoice"];
$data=array();
$data["print"]=1;
$usrID=intval($_SESSION['usrID']);
$reload=0;
function rmJsNull($arr){
	return array_map(function($v){ return (trim($v)=='null' || empty($v))?(NULL):(trim($v));},$arr);
}



$data["serialNo"]=array();
foreach ($pdo->query("select *,p.name as pname,c.name as cname,c.phone as cphone,c.email as cemail,c.addr as caddr,s.qty as sqty,i.qty as qtyR,s.discount,m.name as mname from (sale s join stock i on i.stockID join product p on p.productID join customer c on c.customerID join manufacturer m on m.manufacturerID) where i.stockID=s.stockID and p.productID=i.productID and c.customerID=s.customerID and m.manufacturerID=p.manufacturerID and s.invoice='{$invoice}' order by s.date desc") as $i=>$fetch){
	$data["items"][$fetch["stockID"]]=array($fetch["stockID"],$fetch["mname"]." ".$fetch["pname"],$fetch["sprice"],$fetch["sqty"],$i);
	$data["customer"]=array($fetch["cname"],$fetch["cphone"],$fetch["cemail"],$fetch["caddr"]);
	$discount=json_decode($fetch["discount"],1);
	$data["discount"]["each"][$fetch["stockID"]]=$discount["each"];
	$data["discount"]["global"]=$discount["global"];
	if(count(json_decode($fetch["serialNo"],1)) > 0) $data["serialNo"][$fetch["stockID"]]=json_decode($fetch["serialNo"],1);
	$data["paid"]=$fetch["paid"];
}
$discount=$data["discount"];
$customer_data=array_combine(array("name","email","phone","addr"), $data["customer"]);
$customer_data=rmJsNull($customer_data);

$hasDiscount=(array_sum($discount["each"]) > 0 || $discount["global"] > 0);
$hasSerialNo=count($data["serialNo"]) > 0;

$profile=$pdo->query("select * from profile where profileID=1");
$config=$profile->fetch(PDO::FETCH_ASSOC);

ob_start();
include "../invoice.php";
echo ob_get_clean();
/* print("<pre>");
print_r($data);
print("</pre>"); */
?>