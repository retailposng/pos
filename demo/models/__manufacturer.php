<?php
$isOk=false;
if (isset($_POST["manufacturer"])) {
	try{
		if(empty($_POST["name"])) throw new Exception(NULL,1);
		if(isset($_GET["p"]) && $_GET["p"]=="edit"){
			$manufacturer=$pdo->prepare("update manufacturer set name=:name where manufacturerID=:manufacturerID");
			$manufacturer->execute(array("name"=>$_POST["name"],"manufacturerID"=>intval($_POST["id"])));
		}
		else{
			$manufacturer=$pdo->prepare("insert into manufacturer set name=:name");
			$manufacturer->execute(array("name"=>$_POST["name"]));
		}
		$isOk=true;
	}
	catch (PDOException $e) {
		$err=$e->getMessage();
		
	}
}