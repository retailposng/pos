<?php
$isOk=false;
if (isset($_POST["category"])) {
	try{
		if(empty($_POST["name"])) throw new Exception(NULL,1);
		if(isset($_GET["p"]) && $_GET["p"]=="edit"){
			$category=$pdo->prepare("update category set name=:name where categoryID=:categoryID");
			$category->execute(array("name"=>$_POST["name"],"categoryID"=>intval($_POST["id"])));
		}
		else{
			$category=$pdo->prepare("insert into category set name=:name");
			$category->execute(array("name"=>$_POST["name"]));
		}
		$isOk=true;
	}
	catch (PDOException $e) {
		$err=$e->getMessage();
		
	}
}