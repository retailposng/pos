<div class="image-store hide2" id="image_store">
<h2 style="background:#428BCA" class="no-margin padding10 fg-color-white">Image store<a href="javascript:void(0)" onclick="ImageStore.hide()" title="Close" class="close">&times;</a></h2>

    <div class="row">
    <div class="col-md-6 text-center">
    <div class="row">
            <div class="col-sm-12">
             <h4>Upload image from computer</h4>
             <div class="clear">&nbsp;</div>
             <div class="rel" id="product_icon_box">
			   <a href="javascript:void(0)" class="thumbnail" title="Upload icon" style="width:100px;display:inline-table">
			   <img src="img/burger1.png" class="img-responsive" alt="image-name"/>
			 	</a>
			 	<div class="clearfix"></div>
			 	<button class="btn btn-xs btn-default" onclick="$('#upload_stock_icon').click()">Select Image&nbsp;<i class="glyphicon glyphicon-upload"></i></button>
			  </div>
             <form action='javascript:void(0)' class="hide" method='post' name="logoForm" enctype='multipart/form-data'>
					                   <iframe src="javascript:void(0)" name="logoFrame"></iframe>
					                    <input type="file" name="file" title="Upload" onchange="this.form['submit'].click()" id="upload_stock_icon" class="upload"/>
					                  <input type='submit' name='submit' style='display:none' onclick="ImageUpload.process(this.form,'views/upload.php',setProductIcon)" value='submit'/>
					                   </form>
            </div>
            
            </div>
            <div class="clear">&nbsp;</div>
            <div class="clear">&nbsp;</div>
            <h1>OR</h1>
            <div class="row">
            <div class="col-md-12">
                <h4>Select from related Images</h4>
                
            </div>
            <div class="clear"></div>
            <div data-image="thumbnails" id="related_thumbnails" style="max-height:300px;overflow:auto">
           Loading related images...
            </div>
            </div>
    
    </div>
    <div class="col-md-6 rel">
    <div class="clear">&nbsp;</div>
    <ul class="nav nav-tabs">
    <li class="active"><a href="#tab1" data-toggle="tab">Google Image Search</a></li>
    <li><a href="#tab2" data-toggle="tab" onclick="ImageStore.Synchronize.start();">Synchronize Images</a></li>
  </ul>
  <div class="tab-content">
    <div class="tab-pane active" id="tab1">
      <div class="row" id="image_store_web">
<div class="clear">&nbsp;</div>
<div class="col-sm-12">



<form action="javascript:void(0)" method="post">
               <div class="input-group">
               <input type="text" class="form-control" name="keyword" placeholder="Search Image">
               <div class="input-group-btn">
               <button type="submit" onclick="ImageStore.Web.search(this.form['keyword'].value)" class="btn btn-default"><i class="glyphicon glyphicon-search"></i></button>
               </div>
               </div>
               </form>
            </div>
            <div class="clear">&nbsp;</div>
             <div data-image="thumbnails" id="search_thumbnails"></div>
            
            <div class="col-lg-12">
            <ul class="pagination hide2" id="image_search_pagination">
            <?php 
            for ($i=0,$j=1;$i<5;$i++,$j++){
				$active=($i==0)?("class='active'"):(NULL);
            	echo "<li $active><a href='javascript:void(0)' onclick='ImageStore.Web.pagination($i)'>$j</a></li>";
            }
            ?>
			</ul>
			
            </div>
            
        </div>    
    </div>
    <div class="tab-pane" id="tab2">
     <!--  <h1>Synchronize</h1> -->
     <div class="clear">&nbsp;</div>
      <div class="row" data-image="thumbnails" id="syn_thumbnails">
      </div>
    </div>
  </div>
    
    
    </div>
    <div class="col-md-12">
			<button type="button" onclick="ImageStore.hide()" class="btn btn-default pull-right">Close</button>
            </div>
    </div>
</div>