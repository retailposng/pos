<div class="row calculator bg-color-white border" id="calc_div">
<div class="col-md-12 padding5 text-left" style="border-bottom:1px solid #ccc;height:auto;background:#f5f5f5;cursor:move">
<h4 class="no-padding no-margin">
<i class="icon-calculate"></i>&nbsp;Calculator
</h4>
</div>
<div class="col-md-12 padding5">
<div class="form-control screen text-right border digit" id="screen">0</div>
<div class="btn-toolbar">
<button class="btn btn-func" value="&#40;" onclick="openBracketBtn(this)">&#40;</button><button class="btn btn-func" value=")" onclick="closeBracketBtn(this)">&#41;</button><button class="btn btn-func" value="%" onclick="signBtn(this)">%</button><button class="btn btn-func" id="ce_btn" value="ce" onclick="ClearEntry(this)">CE</button>
</div>

<div class="btn-toolbar">
<button class="btn" value="7" onclick="numBtn(this)">7</button><button class="btn" value="8" onclick="numBtn(this)">8</button><button class="btn" value="9" onclick="numBtn(this)">9</button><button class="btn btn-func" value="÷" onclick="signBtn(this)">÷</button>
</div>

<div class="btn-toolbar">
<button class="btn" value="4" onclick="numBtn(this)">4</button><button class="btn" value="5" onclick="numBtn(this)">5</button><button class="btn" value="6" onclick="numBtn(this)">6</button><button class="btn btn-func" value="x" onclick="signBtn(this)">x</button>
</div>

<div class="btn-toolbar">
<button class="btn" value="1" onclick="numBtn(this)">1</button><button class="btn" value="2" onclick="numBtn(this)">2</button><button class="btn" value="3" onclick="numBtn(this)">3</button><button class="btn btn-func" value="-" onclick="signBtn(this)">-</button>
</div>

<div class="btn-toolbar" style="margin-bottom:0px">
<button class="btn" value="0" onclick="numBtn(this)">0</button><button class="btn" value="." onclick="numBtn(this)">.</button><button class="btn btn-green" onclick="equalBtn()">&#61;</button><button class="btn btn-func" value="+" onclick="signBtn(this)">+</button>
</div>

</div>
</div>