<div class="clear">&nbsp;</div>
<div class="col-md-12">
<table class="table table-striped table-bordered" id="tb_data">
<thead>
<tr><th>#</th><th></th><th>Product</th><th>Category</th><th>Qty.</th><th>In stock</th><th>Price</th><th>Supplier</th><th>Date</th></tr>
</thead>
<tbody>
<?php
$i=1;
foreach ($this->pdo->query("select c.name as cname,concat(m.name,' ',p.name) as pname,d.qty as dqty,s.qty as sqty,s.cprice,x.name as xname,i.name as iname,d.supplierID,from_unixtime(s.date,'%d-%m-%Y') as sdate from (purchase d join stock s on s.stockID join category c on c.categoryID join product p on p.productID join manufacturer m on m.manufacturerID join supplier x on x.supplierID join img i on i.imgID) where c.categoryID=p.categoryID and p.productID=s.productID and m.manufacturerID=p.manufacturerID and d.stockID=s.stockID and x.supplierID=d.supplierID and i.imgID=s.imgID order by d.date desc") as $fetch){
?>
<tr><td><?=$i;?></td>
<td class="mini-thumbnail">
<a href="javascript:void(0)" class="thumbnail no-margin mini-thumbnail">
<img src="views/upload/thumbnail/<?=$fetch["iname"];?>"/>
</a>
</td>
<td><a href=""><?=$fetch["pname"];?></a></td><td><?=$fetch["cname"];?></td><td><?=$fetch["dqty"];?></td><td><?=$fetch["sqty"];?></td><td><?=$fetch["cprice"];?></td><td><a href=""><?=$fetch["xname"];?></a></td><td><?=$fetch["sdate"];?></td></tr>
<?php 
$i++;
}
?>
</tbody>
</table>
</div>