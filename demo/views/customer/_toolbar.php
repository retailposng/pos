<div class="row">
<div class="col-md-2 no-padding"><h3 class="no-margin lh40">Customers</h3></div>
<div class="col-md-6 no-padding adjMt10">
<div class="input-group">
                <div class="input-group-btn search-panel">
                    <a href="customer.php?p=add" class="btn btn-primary">Add customer&nbsp;<i class="glyphicon glyphicon-plus"></i></a>
             	</div>
                <input type="text" class="form-control" id="tb_data_search" name="x" placeholder="Filter" onmouseover="this.focus()"/>
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
            </div>
            <div class="col-md-4 adjMt10">
            <?php 
            include_once 'views/stock/alert.php';
            ?>
            </div>
            </div>