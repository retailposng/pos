<div class="row">
<div class="col-md-2 no-padding"><h3 class="no-margin lh40">Point Of Sale</h3></div>
<div class="col-md-6 no-padding adjMt10">
<div class="input-group">
                <div class="input-group-btn search-panel dropdown">
                    
                    <button type="button" class="btn btn-default" data-toggle="dropdown">
                    	<span id="search_label">Category</span> <span class="caret"></span>
                    </button>
                    <input type="hidden" id="search_strong_keyword" value=""/>
                    <ul class="dropdown-menu">
                    <!-- 
                     <li>  
                	<div class="col-xs-12">                    	
                        <form class="control-group">
                        	<label>Search »</label>
                        	<input type="text" class="form-control" onkeyup="xsearch(this.value,getObj('dropdown-menu'))" placeholder="Search">
                        </form>
                    </div>
                    
                </li> -->
                <li class='w100'><a href='javascript:void(0)' class='fill' onclick="searchDropdownCategory('')">All</a></li>
                    <li class='w100'><a href='javascript:void(0)' class='fill' onclick="inCart()">Items In Cart</a></li>
                    <li class='w100 divider'></li>
                  
                <li>
                <ul class="pos-dropdown">
                  
                    <?php 
			        foreach ($this->pdo->query("select * from category order by name") as $fetch){
			        	echo "<li class='w100'><a href='javascript:void(0)' class='fill' onclick=\"searchDropdownCategory('{$fetch['name']}',Cart.check)\">{$fetch['name']}</a></li>";
			        }
			        ?></ul>
			        </li>
                    </ul>
                    
                </div>
                
                <input type="text" class="form-control" onmouseover="this.focus()" name="x" placeholder="Search item descriptions or SKU" onkeyup="SearchList.filter(this.value,Cart.check)">
                <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><span class="glyphicon glyphicon-search"></span></button>
                </span>
            </div>
            <span id="_index"></span>
            </div>
            <div class="col-md-4 no-padding adjMt10">
            <?php 
            include_once 'views/stock/alert.php';
            ?>
            </div>
            </div>