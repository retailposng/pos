<div class="clear">&nbsp;</div>
<div class="col-md-12">
<table class="table table-striped table-bordered" id="tb_data">
<thead>
<tr><th>#</th><th>Name</th><th>Email</th><th>Phone</th><th>Address</th><th>Supplied</th><th></th></tr>
</thead>
<tbody>
<?php
$i=1; 
foreach ($this->pdo->query("select s.*,(select sum(p.qty) from purchase p where p.supplierID=s.supplierID) as qty from supplier s") as $fetch){
?>
<tr><td><?=$i;?></td><td><?=$fetch["name"];?></td><td><?=$fetch["email"];?></td><td><?=$fetch["phone"];?></td><td><?=$fetch["addr"];?></td><td><a href="#"><?=intval($fetch["qty"]);?></a></td><td><a href="supplier.php?p=edit&name=<?=$fetch["name"];?>&email=<?=$fetch["email"];?>&phone=<?=$fetch["phone"];?>&addr=<?=$fetch["addr"];?>&id=<?=$fetch["supplierID"];?>" class="btn btn-sm btn-default"><i class="glyphicon glyphicon-edit"></i></a></td></tr>
<?php 
$i++;
}
?>
</tbody>
</table>
</div>