<?php 
global $isOk,$post,$err;
if(isset($_GET)) $_POST=$_GET;
else if(isset($post)) $_POST=$post;
ob_start();
?>
<div class="row bg-color-white padding10">
<div class="col-md-7">
<?php
if ($isOk) {
	echo "<div class='alert alert-success padding5'>Successful</div>";
}
$err_arr=array(1=>"Enter supplier name",23000=>"Duplicate data found either email/phone number already exist");
if(!empty($err)) echo "<div class='alert alert-danger padding5'>{$err_arr[$err]}</div>";
?>
<form action="<?=$_SERVER["PHP_SELF"];?>?p=<?=@$_POST["p"];?>" method="post">
<div class="control-group">
    <label class="control-label">Supplier Name</label>
    <input type="hidden"name="id" value="<?=@$_POST["id"];?>"/>
    <input type="text" name="name" class="form-control" value="<?=@$_POST["name"];?>"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Email Address</label>
    <input type="text" name="email" class="form-control" value="<?=@$_POST["email"];?>"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Phone Number</label>
    <input type="text" name="phone" class="form-control" value="<?=@$_POST["phone"];?>"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Address</label>
    <textarea class="form-control" rows="5" name="addr"><?=@$_POST["addr"];?></textarea>
    </div>
  <div class="clear">&nbsp;</div>
   <div class="control-group">
  <button type="submit" name="supplier" class="btn btn-primary">Submit</button>
    </div>
  </form>
</div>
<div class="col-md-5">

</div>
</div>
<?php 
$contents=ob_get_clean();
include_once 'views/setting/_template.php';
?>