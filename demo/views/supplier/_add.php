<?php 
global $isOk,$post;
if(isset($post)) $_POST=$post;
?>
<div class="row bg-color-white padding10">
<div class="col-md-7">
<?php
if ($isOk) {
	echo "<div class='alert alert-success padding5'>Successfully added</div>";
}
?>
<form action="<?=$_SERVER["PHP_SELF"];?>?p=add" method="post">
<div class="control-group">
    <label class="control-label">Full Name</label>
    <input type="text" name="name" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Email Address</label>
    <input type="text" name="email" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Phone Number</label>
    <input type="text" name="phone" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group">
    <label class="control-label">Home/Office Address</label>
    <textarea class="form-control" rows="5" name="addr">
    </textarea>
    </div>
  <div class="clear">&nbsp;</div>
   <div class="control-group">
  <button type="submit" name="supplier" class="btn btn-primary">Add</button>
    </div>
  </form>
</div>
<div class="col-md-5">

</div>
</div>