<?php 
//include 'lib/options.php';
$list=array("sale"=>"Sales","stock"=>"Stock","purchase"=>"Purchase","setting"=>"Configuration");
$list2=array("customer"=>"Customer","supplier"=>"Supplier","user"=>"User");
$list3=array("manufacturer"=>"Manufacturer","category"=>"Category","product"=>"Product");
if(isset($post)) $_POST=$post;
?>
<div class="ae-well">
<div class="col-md-3">
<h3><span class="label label-default padding5"><?=((isset($_GET['p']))?(ucfirst($_GET['p'])." "):(NULL)).(array_merge($list,$list2,$list3)[$this->fn]);?></span>&nbsp;Setting</h3>
<div class="list-group">
<?php 
foreach ($list as $url=>$listItem){
	$active=($this->fn==$url)?("active"):(NULL);
	echo "<a href='$url.php' class='list-group-item $active'>$listItem</a>";
}
?>
</div>
<div class="clear">&nbsp;</div>
<div class="list-group">
<?php 
foreach ($list2 as $url=>$listItem){
	$active=($this->fn==$url)?("active"):(NULL);
	echo "<a href='$url.php' class='list-group-item $active'>{$listItem}</a>";
}
?>
</div>
<div class="clear">&nbsp;</div>
<div class="list-group">
<?php 
foreach ($list3 as $url=>$listItem){
	$active=($this->fn==$url)?("active"):(NULL);
	echo "<a href='$url.php' class='list-group-item $active'>{$listItem}</a>";
}
?>
</div>
</div>

<div class="col-md-9">
<div class="clear">&nbsp;</div>
<?=$contents;?>
 </div>
 <div class="clear"></div>
 </div>