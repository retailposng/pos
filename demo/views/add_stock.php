<?php 
$usrID=1;
include 'lib/options.php';
?>
 
 <form action="javascript:void(0)" method="post" enctype='multipart/form-data'>
 <div class="hide2" id="add_stock_box">
	<div class="row">
    <div class="col-md-12 no-padding add-stock-box" id="add_item">
    <h3 class="padding10 no-margin" id='add_title'>Add Product  
    <a href="javascript:void(0)" class="close">&times;</a>
    </h3>
    <div class="padding10" id="control_box" style="height:100%;overflow:auto">
    
    <div class="control-group row" id="store_box">
    <label class="control-label">Store</label>
    <div class="col-xs-11 no-padding">
    <select class="form-control" name="store">
    <option value=""></option>
    <?=options("store");?>
    </select>
    <input type="text" class="form-control hide2" name="_store" placeholder="Type and press enter" onmouseover="this.focus()" onkeyup="addCategory(event)"/>
    </div>
    <div class="col-xs-1 padding5">
      <a href="javascript:void(0)" onclick="addManu(getObj('store_box'))"><i class="glyphicon glyphicon-plus"></i></a>
    </div>
  </div>
  
    <div class="clear">&nbsp;</div>
   <div class="control-group row">
    <div class="col-xs-12">
    <input type="checkbox" name="state" value="stock" id="stock" checked="checked"/>&nbsp;&nbsp;<label for="stock">Add to stock</label>
    </div>
      </div>
   
    <div class="clear">&nbsp;</div>
    <div class="control-group row" id="manu_box">
    <label class="control-label">Manufacturer</label>
    <div class="col-xs-11 no-padding">
    <select class="form-control" name="manu" onchange="getProduct(this.form)">
    <option value=""></option>
    <?=options("manufacturer");?>
    </select>
    <input type="text" class="form-control hide2" name="_manu" placeholder="Type and press enter" onmouseover="this.focus()" onkeyup="addCategory(event)"/>
    </div>
    <div class="col-xs-1 padding5">
      <a href="javascript:void(0)" onclick="addManu(getObj('manu_box'))"><i class="glyphicon glyphicon-plus"></i></a>
    </div>
  </div>

 
 <div class="clear">&nbsp;</div>
   <div class="control-group row" id="cat_box">
    <label class="control-label">Category</label>
    
    <div class="col-xs-11 no-padding">
    <select class="form-control" name="cat" onchange="getProduct(this.form)">
    <option value=""></option>
     <?=options("category");?>
    </select>
     <input type="text" class="form-control hide2" name="_cat" placeholder="Type and press enter" onmouseover="this.focus()" onkeyup="addCategory(event)"/>
    </div>
    <div class="col-xs-1 padding5">
      <a href="javascript:void(0)" onclick="addManu(getObj('cat_box'))"><i class="glyphicon glyphicon-plus"></i></a>
    </div>
  </div>
  
  <div class="clear">&nbsp;</div>
   <div class="control-group row" id="pro_box">
    <label class="control-label">Product Name</label>
    
    <div class="col-xs-11 no-padding">
    <select class="form-control" name="pro">
    <option value=""></option>
     <?=options("product");?>
    </select>
     <input type="text" class="form-control hide2" name="_pro" placeholder="Type and press enter" onmouseover="this.focus()" onkeyup="addCategory(event)"/>
    </div>
    <div class="col-xs-1 padding5">
      <a href="javascript:void(0)" onclick="addManu(getObj('pro_box'))"><i class="glyphicon glyphicon-plus"></i></a>
    </div>
  </div>
  
  <div class="clear">&nbsp;</div>
   <div class="control-group row">
    <div class="col-xs-4 no-padding">
    <label class="control-label" >Cost Price</label>
    <input type="text" class="form-control" name="cprice" value="0.00" onmouseover="this.focus()" style="width:100px;"/>
    </div>
    
    <div class="col-xs-4 no-padding">
    <label class="control-label" >Sale Price</label>
    <input type="text" class="form-control" name="sprice" value="0.00" onmouseover="this.focus()" style="width:100px;"/>
    </div>
    
    <div class="col-xs-4 no-padding" id="example">
   <label class="control-label" >Quantity</label>
    <input type="number" class="form-control" name="qty" min="1" value="1" onmouseover="this.focus()" style="width:100px;"/>
    </div>
    <div class="clear">&nbsp;</div>
    
    <div class="control-group row">
    
    <div class="col-xs-6 no-padding">
    <label class="control-label">Product Icon</label>
    
   
    
    <div class="rel" id="product_icon_box">
   <a href="javascript:void(0)" class="thumbnail">
   <img src="img/burger1.png" class="img-responsive" alt="image-name"/>
  
   </a>
   <input type="hidden" name="icon" value=""/>
  <div class="absBR btn-group" style="right:3px;bottom:3px">
							<button type="button" class="btn btn-default" onclick="rmLogo(getObj('logo_img'))" title="Remove" style="padding:0px 3px!important"><i class="glyphicon glyphicon-remove"></i></button>
							<button type="button" class="btn btn-xs btn-default" onclick="getObj('upload_stock_icon').click()"><i class="glyphicon glyphicon-upload"></i></button>
							
							
							</div>
   </div>
   
  </div>
    
    <div class="col-xs-6">
    <label class="control-label">Product codes | S/N &nbsp;<i class="glyphicon glyphicon-question-sign"></i></label>
    <input type="file" name="file"  class="form-control"/>
   </div>
   
   </div>
    </div>
 
  <div class="clear">&nbsp;</div>
    </div>
    <div class="alert alert-danger padding5 margin10 pull-left hide2" id="err1"></div>
    <div class="btn-group pull-right margin10">
  <button type="reset" class="btn btn-default">Reset</button>
  <button type="button" onclick="Stock.proceed(this.form)" class="btn btn-default">Proceed</button>
  </div>
    </div>
    <div class="clear">&nbsp;</div>
    </div>
    
    <div class="col-md-12 no-padding add-stock-box hide2"  id="add_supplier" style="overflow:auto">
    <div class="row">
	<div class="col-xs-12 no-padding add-stock-box" id="add_item" style="overflow:auto">
    <h3 class="padding10 no-margin" id='add_title'>Add Supplier  
    <a href="javascript:void(0)" class="close">&times;</a>
    </h3>
      <div class="clear">&nbsp;</div>
    
    <div class="control-group row">
    <div class="col-xs-6">
    <input type="radio" name="supplier" value="new" id="new_supplier" onchange="getObj('new_supplier_box').show();getObj('existing_supplier_box').hide()"/>&nbsp;&nbsp;<label for="new_supplier">New supplier</label>
    </div>
    <div class="col-xs-6">
    <input type="radio" name="supplier" value="old" id="old_supplier" checked="checked" onchange="getObj('new_supplier_box').hide();getObj('existing_supplier_box').show()"/>&nbsp;&nbsp;<label for="old_supplier">Existing supplier</label>
    </div>
    </div>
    
    <div class="clear">&nbsp;</div>
    
    <div class="padding10" id="existing_supplier_box">
    <div class="control-group row">
   <label>Supplier</label>
    <div class="col-xs-11 no-padding">
    <select class="form-control" name="supplierID">
   <option value=""></option>
   <?=options("supplier");?>
   </select>
    </div>
    <div class="col-xs-1 padding5">
      <a href="javascript:void(0)" onclick="getObj('new_supplier').click()"><i class="glyphicon glyphicon-plus"></i></a>
    </div>
  </div>
    </div>
    
   <div class="hide2" id="new_supplier_box">
    <div class="control-group col-xs-12">
    <label class="control-label">Company Name</label>
    <input type="text" name="fname" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group col-xs-12">
    <label class="control-label">Email Address</label>
    <input type="text" name="email" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group col-xs-12">
    <label class="control-label">Phone Number</label>
    <input type="text" name="phone" class="form-control"/>
    </div>
    <div class="clear">&nbsp;</div>
    <div class="control-group col-xs-12">
    <label class="control-label">Office Address</label>
    <textarea class="form-control" rows="5" name="addr">
    </textarea>
    </div>
    </div>
    
     <div class="clear">&nbsp;</div>
    <div class="col-xs-12">
    <div class="alert alert-danger padding5 margin10 pull-left hide2" id="err2"></div>
    <div class="btn-group pull-right">
    <button type="button" class="btn btn-default" onclick="getObj('add_supplier').hide();getObj('add_item').show();">Back</button>
    <button type="submit" onclick="Stock.add(this.form)" class="btn btn-primary">Submit</button>
    </div>
    </div>
      <div class="clear">&nbsp;</div>
    </div>
    </div>
    <span id="err"></span>
    <div class="clear"></div>
   </div>
      <div class="clear"></div>
  </div>
  </form>
  <form action='javascript:void(0)' class="hide" method='post' name="logoForm" enctype='multipart/form-data'>
					                   <iframe src="javascript:void(0)" name="logoFrame"></iframe>
					                    <input type="file" name="file" title="Upload" onchange="this.form['submit'].click()" id="upload_stock_icon" class="upload"/>
					                  <input type='submit' name='submit' style='display:none' onclick="upload.process(this.form,'view/upload.php',setProductIcon)" value='submit'/>
					                   </form>
					                    <iframe src="javascript:void(0)" name="addStockFrame" class="hide" style="/* position:fixed;left:300px;bottom:200px */"></iframe>