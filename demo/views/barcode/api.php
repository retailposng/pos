<?php
include '../../config.php';
ini_set("display_errors",1);
/* print("<pre>");
print_r($_POST);
print("</pre>"); */
function getID($tb,$data){
	global $pdo;
	$str=array();
	foreach (array_keys($data) as $col) $str[]="{$col}=:{$col}";
	$cols=join(",",$str);
	$queryID=$pdo->prepare("insert into {$tb} set {$cols} on duplicate key update {$tb}ID=LAST_INSERT_ID({$tb}ID),{$cols}");
	$queryID->execute($data);
	return $pdo->lastInsertId();
}


$manufacturerID=getID("manufacturer",array("name"=>$_POST['manufacturer']));
$categoryID=getID("category",array("name"=>$_POST['category']));
$productID=getID("product",array("name"=>$_POST['product'],"manufacturerID"=>$manufacturerID,"categoryID"=>$categoryID));


$size=intval(@$_POST['size']);
$colorID=intval(getID("color",array("name"=>@$_POST['color'])));


$_12digits=sprintf("234%02d%02d%03d%d%d",$manufacturerID,$categoryID,$productID,$size,$colorID);

$arr=[];
foreach (str_split($_12digits) as $k=>$v){
	$weight=(($k+1)%2==0)?(3):(1);	
	$arr[$k]=intval($v)*$weight;
}

$checkDigit=10-(array_sum($arr)%10);
echo $barcode=$_12digits.$checkDigit;
?>