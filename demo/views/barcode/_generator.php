<div class="row">
<div class="col-md-4"></div>
<div class="col-md-4">
<h1>Barcode Generator</h1>

 <form action="views/barcode/output.php" method="post" onsubmit="this.target=$('#bargen').get(0).name">

  <div class="control-group row" id="manu_box">
    <label>Manufacturer</label>
   <div class="col-xs-12 rel no-padding">
              		<input type="text" class="form-control" placeholder="Manufacturer" name="manufacturer" id="<?=uniqid();?>"  onmouseover="this.focus()" autocomplete="off" onkeyup="DList.search(event,this,getObj('manufacturer_dlist'))" required="required"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="manufacturer_dlist">
				      <ul class="navlist link"></ul>
				      </div>
    </div>
  </div>

 
 <div class="clear">&nbsp;</div>
 
 <div class="control-group row" id="pro_box">
    <label>Product Name</label>
    
  <div class="col-xs-12 rel no-padding">
              		<input type="text" class="form-control" placeholder="Product" name="product" id="<?=uniqid();?>"  onmouseover="this.focus()" autocomplete="off" onkeyup="DList.search(event,this,getObj('product_dlist'))" required="required"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="product_dlist">
				      <ul class="navlist link"></ul>
				      </div>
    </div>
  </div>
  
  <div class="clear">&nbsp;</div>
   
    <div class="row" id="cat_box">
    <label>Category</label>
    
   <div class="col-xs-12 rel no-padding">
              		<input type="text" class="form-control" placeholder="Category" name="category" id="<?=uniqid();?>"   autocomplete="off" onkeyup="DList.search(event,this,getObj('category_dlist'))"  required="required"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="category_dlist">
				      <ul class="navlist link"></ul>
				      </div>
    </div>
  </div>
  
   <div class="clear">&nbsp;</div>
   <div class="row">
 <div class="col-xs-12 no-padding">
    <label onclick="toggle('desc2',function(){$('#_desc2').show();},function(){$('#_desc2').hide();})">Variant <i class="caret"></i></label>
	 <div class="row hide2" id="_desc2">
	 <div class="col-xs-6 no-padding">
	 <select class="form-control" name="size">
	 <?php 
	 $i=0;
	 foreach (array(""=>"--Size--","none"=>"None","xxxs"=>"Smallest","xxs"=>"Extra extra small","xs"=>"Extra small","s"=>"Small","m"=>"Medium","l"=>"Large","xl"=>"Extra large","xxl"=>"Extra extra large","xxxl"=>"Largest") as $k=>$v){
	 	echo "<option value='{$i}'>{$v}</option>";
	 	$i++;
	 }
	 ?>
	 </select>
	 </div>
	 <div class="col-xs-6 pr0">
	  <input type="text" class="form-control" placeholder="Color" name="color" id="<?=uniqid();?>"  onmouseover="this.focus()" autocomplete="off" onkeyup="DList.search(event,this,getObj('color_dlist'))"/>
				      <div class="absTR white-box padding5 hide2 suggest-sch" id="color_dlist">
				      <ul class="navlist link"></ul>
				      </div>
	  
	 </div>
	 
	 </div>
	 <div class="clear">&nbsp;</div>
	 <div class="well well-sm">
	  <div class="control-group">
      <label>Per column</label>
    <input type="number" name="col" class="form-control" value="3"  required="required"/>
      </div>
	 
	 <div class="control-group">
      <label>Quantity</label>
    <input type="number" name="qty" class="form-control" value="15"  required="required"/>
      </div>
	 
	 </div>
	 
	 <div class="clear">&nbsp;</div>
	 <button type="submit" name="generate" class="btn btn-primary btn-block">Generate</button>
	 
	 </div>
  </div>
 </form>
</div>
<div class="col-md-4"></div>
</div>
<iframe src="javascript:void(0)" name="bargen" id="bargen" class="hide"></iframe>