<?php
include '../../config.php';
ini_set("display_errors",1);
/* print("<pre>");
print_r($_POST);
print("</pre>"); */
function getID($tb,$data){
	global $pdo;
	$str=array();
	foreach (array_keys($data) as $col) $str[]="{$col}=:{$col}";
	$cols=join(",",$str);
	$queryID=$pdo->prepare("insert into {$tb} set {$cols} on duplicate key update {$tb}ID=LAST_INSERT_ID({$tb}ID),{$cols}");
	$queryID->execute($data);
	return $pdo->lastInsertId();
}


$manufacturerID=getID("manufacturer",array("name"=>$_POST['manufacturer']));
$categoryID=getID("category",array("name"=>$_POST['category']));
$productID=getID("product",array("name"=>$_POST['product'],"manufacturerID"=>$manufacturerID,"categoryID"=>$categoryID));


$size=intval(@$_POST['size']);
$colorID=intval(getID("color",array("name"=>@$_POST['color'])));


$_12digits=sprintf("234%02d%02d%03d%d%d",$manufacturerID,$categoryID,$productID,$size,$colorID);

$arr=[];
foreach (str_split($_12digits) as $k=>$v){
	$weight=(($k+1)%2==0)?(3):(1);	
	$arr[$k]=intval($v)*$weight;
}

$checkDigit=10-(array_sum($arr)%10);
$barcode=$_12digits.$checkDigit;
?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <title><?=strtoupper("{$_POST['manufacturer']} {$_POST['product']}");?></title>
    <style type="text/css">
   
    table{
    width: 100%;
    }
    table td{
    padding: 10px;
    text-align: center;
    border: 1px solid #222;
    }
    </style>
  </head>
  <body onload="window.print()">
  <?php 
  $sizes=array_values(array("none"=>"None","xxxs"=>"Smallest","xxs"=>"Extra extra small","xs"=>"Extra small","s"=>"Small","m"=>"Medium","l"=>"Large","xl"=>"Extra large","xxl"=>"Extra extra large","xxxl"=>"Largest"));
  for($i=0;$i<$_POST["qty"];$i++){
  	?>
  	<img src="../../lib/barcode/barcode.php?code=<?=$barcode;?>&encoding=EAN&mode=png"/>
  	<?php
  	if(($i+1)%$_POST["col"]==0 && $i < $_POST["qty"]-1) echo "<p style='border-bottom: 1px dashed #000'></p>";
  }
  ?>
  <br/>
  <br/>
  <br/>
  <table>
  <thead>
  <tr><th>Product</th><th>Category</th><th>Size</th><th>Color</th></tr>
  </thead>
  <tbody>
  <tr><td><?="{$_POST["manufacturer"]} {$_POST["product"]}";?></td><td><?="{$_POST["category"]}";?></td><td><?="{$sizes[$_POST["size"]]}";?></td><td><?="{$_POST["color"]}";?></td></tr>
  </tbody>
  </table>
  </body>
  </html>